<!DOCTYPE HTML>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Lato"/>
        <link rel="stylesheet" href="/RWA_project/admin_main_panel.css">
    </head>
    
    <body>
    
    
    
    	<div class="container-fluid">
            <div class="row py-3">
            	<div class="col-2"></div>
                <div class="col-8"></div>
                <div class="col-2">
                    <div class="row">
                    	<div class="col-3"></div>
                        <div class="col-7 get-new-quiz-button d-flex" onclick="getRandomQuizPreviewPair();">
                            <span class="mx-auto my-auto">User panel</span>
                        </div>
                        <div class="col-2"></div>
                    </div>
                </div>
            </div>
        </div>




        <div class="container">
            <div class="row py-5">
                <div class="col-12"></div>
            </div>

            <div class="row">
                <div class="col-3"></div>
                <div class="col-6 px-5 d-flex">
                    <span class="quiz-font1 mx-auto my-auto text-center">Admin quiz panel</span>
                </div>
                <div class="col-3"></div>
            </div>
            
            <div class="row mt-n3">
                <div class="col-3"></div>
                <div class="col-6 px-5 d-flex">
                    <span class="quiz-font2 mx-auto my-auto" onclick="sendCreateQuizMessage(redirectToQuizView, function(){});">Click here to create new quiz</span>
                </div>
                <div class="col-3"></div>
            </div>
            
            <div class="row py-3">
                <div class="col-12"></div>
            </div>
        </div>
        
        
        
        
        
        
        
        
        
        
        
        
        <!-- QUIZ PREVIEW TEMPLATE => START -->
        <div class="row py-4 quiz-preview-box quiz-preview-template hidden">
        	<div class="col-3"></div>
           	<div class="col-6 px-5">
				<!--  FLIP BOX START -->
              	<div class='flip-box'>
                	<!--  FLIP BOX FRONT START -->
                    <div class='front'>
                        <div class="row quiz-preview bg-color-1">
                           	<div class="col-1"></div>
                           	<div class="col-2 d-flex">
                           		<span class="mx-auto my-auto"><img class="quiz-img" src="/RWA_project/img/html_icon.png" alt="Icon"></span>
                       		</div>
                       		<div class="col-6 my-auto">
                           		<div class="row d-flex">
                               		<div class="col-12 quiz-title my-auto">Begginer Javascript</div>
                  	            	<div class="col-12 quiz-short-description my-auto">21 questions - 10min duration</div>
                           		</div>
                            </div>
                            <div class="col-1"></div>
                           	<div class="col-2 d-flex">
                               	<span class="mx-auto my-auto">
                               		<img class="quiz-view-button" src="/RWA_project/img/start_icon.png" alt="Icon" onclick="event.stopPropagation(); startQuiz(this);">
                               	</span>
                           	</div>
                       	</div>
                    </div>
					<!--  FLIP BOX FRONT END -->
						
					<!--  FLIP BOX BACK START -->
                    <div class='back'>
                       	<div class="row quiz-preview bg-color-1 inverse">
                           	<div class="col-2"></div>
                           	<div class="col-8 py-2">
                           		<div class="row d-flex">
                           			<div class="col-12 quiz-info mx-auto my-auto pb-2">
                           				<span class="quiz-info">Created by Admin, maximum score 10</span>
                           			</div>
                           	 		<div class="col-12 quiz-description my-auto">
                           	 			<span class="quiz-description">
                           	 				Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui.
                               	 		</span>
                               	 	</div>
                            	</div>
                            </div>
                            <div class="col-2 d-flex"></div>
                        </div> 
                    </div>
                    <!--  FLIP BOX BACK END -->
                </div>
                <!--  FLIP BOX END -->
            </div>
            <div class="col-3"></div>
         </div>
         <!-- QUIZ PREVIEW TEMPLATE => END -->
        
        
        
     
        



        <div class='container'>
        	<div class="row">
        		<div class="col-12 quiz-preview-container">
       				<!-- PLACE TO ADD QUIZ PREVIEW -->
				</div>
			</div>
        </div>





        
        
    </body>
    
    <!-- MY JAVASCRIPT-->
    <script type="text/javascript" src="/RWA_project/admin_main_panel_communication.js"></script>
    <script type="text/javascript" src="/RWA_project/admin_main_panel_animation.js"></script>
    
 </html>