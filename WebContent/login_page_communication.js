var loginURI = "http://localhost:8080/RWA_project/rest/admin/panel/";



function Login(redirectURIVal) {
	
	var username = document.getElementsByClassName("username")[0];
	var password = document.getElementsByClassName("password")[0];
	var loginButton = document.getElementsByClassName("user-login-button")[0];
	var redirectURI = redirectURIVal;
	
	
	function getUsername() {
		return username.value;
	}
	
	
	function getPassword() {
		return password.value;
	}
	
	
	function restartUsername() {
		username.value = '';
	}
	
	
	function restartPassword() {
		password.value = '';
	}
	
	
	
	this.getUsername = getUsername;
	this.getPassword = getPassword;
	
	
	// CALLBACK ON EVENTS
	
	
	function loginButtonCallbackFunction() {
		
		var username = getUsername();
		var password = getPassword();
		
		console.log("LOGIN pressed, username: ", username, " password: ", password);
		
		if(username == null || password == null)
			return;
		
			
		var dataJSON = JSON.stringify({username: username, password: password});
		var httpRequest = new XMLHttpRequest();
			
		httpRequest.onreadystatechange = function() {
			if (this.readyState == 4)
			{	
				if(this.status == 200)
				{
					window.location.replace(redirectURI);
				}
				else
				{
					restartUsername();
					restartPassword();
				}
			}
		};
			
		httpRequest.open("POST", "/RWA_project/rest/login", true);
		httpRequest.setRequestHeader("Content-type", "application/json");
		httpRequest.send(dataJSON);
	}
	
	
	
	loginButton.addEventListener("click", loginButtonCallbackFunction);

}



var login = new Login(loginURI);



