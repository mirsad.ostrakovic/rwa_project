var userinfoName = null;
var userinfoEmail = null;

var quizSessionList = [];

var newQuizID = null;


// UTIL FUNCTIONS

function fetchJSONFile(path, callback) {
	var httpRequest = new XMLHttpRequest();
	
	httpRequest.onreadystatechange = function() {
		if (httpRequest.readyState === 4) {
			if (httpRequest.status === 200) {
				console.log("fetchJSON: ", httpRequest.responseText);
				var data = JSON.parse(httpRequest.responseText);
				if (callback) callback(data);
			}
		}
	};
	
	httpRequest.open('GET', path);
	httpRequest.send(); 
} 



function isFunction(functionToCheck) {
	return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}







// GUI MODEL




function QuizStatusBar() {
	var quizStatusBar = document.getElementsByClassName("quiz-status-bar")[0];
	var startNewQuizButton = quizStatusBar.getElementsByClassName("start-new-quiz-button")[0];
	var startNewQuizButtonTitle = startNewQuizButton.getElementsByClassName("quiz-title")[0]; 
	var userCountText = quizStatusBar.querySelectorAll(".user-count .user-count-text")[0];
	
	var newQuizID = null;
	
	function initStartNewQuizButton(quizID) {
		
		var URL = null;
		
		if(quizID != undefined && quizID != null)
			URL = "http://localhost:8080/RWA_project/rest/quiz/info/" + quizID;
		else
			URL = "http://localhost:8080/RWA_project/rest/quiz/info/random/1";
		
		 fetchJSONFile(URL, function(data) {
			 startNewQuizButtonTitle.textContent = "Click here to start - " + data.quizTitle;
			 newQuizID = data.quizID;
			 showStartNewQuizButton();
		 });
	}
	
	function setUserCount(userCount) {
		userCountText.textContent = "User count: " + userCount;
	}
	
	
	function hideStartNewQuizButton() {
		quizStatusBar.classList.add("hidden");
	}
	
	
	function showStartNewQuizButton() {
		quizStatusBar.classList.remove("hidden");
	}
	
	function getNewQuizID() {
		return newQuizID;
	}
	
	
	this.initStartNewQuizButton = initStartNewQuizButton;
	this.hideStartNewQuizButton = hideStartNewQuizButton;
	this.showStartNewQuizButton = showStartNewQuizButton;
	this.getNewQuizID = getNewQuizID;
	this.setUserCount = setUserCount;
	
	
	// EVENT HANDLERS => CALLBACKS
	
	var startNewQuizButtonCallback = null;
	
	
	function startNewQuizCallbackFunction() {
		if(isFunction(startNewQuizButtonCallback))
			startNewQuizButtonCallback(newQuizID, hideStartNewQuizButton, function() {});
	}
	
	
	startNewQuizButton.addEventListener("click", startNewQuizCallbackFunction);
	
	

	function setStartNewQuizCallbackFunction(startNewQuizCallbackFunc) {
		startNewQuizButtonCallback = startNewQuizCallbackFunc;
	}
	

	this.setStartNewQuizCallbackFunction = setStartNewQuizCallbackFunction;
	
}

var quizStatusBar = new QuizStatusBar();
quizStatusBar.hideStartNewQuizButton();
quizStatusBar.initStartNewQuizButton(otherQuizID);




function DisplayContainer() {
	var displayContainer = document.getElementById("display-container");
	
	var removeLastElementCallback = null;
	var DOMElementList = [];
	
	function getDOMElement() {
		if(DOMElementList.length == 0) {
			DOMElementList.push(document.createElement("DIV"));
			displayContainer.appendChild(DOMElementList[0]);
			DOMElementList[0].className = 'col-12';
			return DOMElementList[0];
		}
		else if(DOMElementList.length == 1) {
			DOMElementList.push(document.createElement("DIV"));
			displayContainer.appendChild(DOMElementList[1]);
			DOMElementList[0].className = 'col-6 px-0'
			DOMElementList[1].className = 'col-6 px-0'
			return DOMElementList[1];
		}
		else
			return null;
	} 
	
	
	function removeDOMElement(DOMElement) {
		DOMElementList = DOMElementList.filter(function(element) {
				return element !== DOMElement;
			});
		
		displayContainer.removeChild(DOMElement);
		
		if(DOMElementList.length == 1)
			DOMElementList[0].className = 'col-12';
		else if(DOMElementList.length == 0 && isFunction(removeLastElementCallback))
			removeLastElementCallback();
	}
	
	
	this.setRemoveLastElementCallback = function(removeLastElementCallbackFunc) { removeLastElementCallback = removeLastElementCallbackFunc; }
	this.getDOMElement = getDOMElement;
	this.removeDOMElement = removeDOMElement;
}

var displayContainer = new DisplayContainer();
displayContainer.setRemoveLastElementCallback(function() { location.replace("http://localhost:8080/RWA_project/quiz_preview.html"); });







function QuizSessionMainPanel(fillBarDirection) {
	var quizSessionTemplate = document.getElementsByClassName("quiz-session-template")[0];
	var answerTemplate = document.getElementsByClassName("answer-template")[0];
	
	
	var quizSession = quizSessionTemplate.cloneNode(true);
	quizSession.classList.remove("quiz-session-template");
	quizSession.classList.remove("hidden");
	
	
	var questionOrdinalNumber = quizSession.getElementsByClassName("question-ordinal-number")[0];
	var questionText = quizSession.getElementsByClassName("question-text")[0];
	var answersBox = quizSession.getElementsByClassName("answers-box")[0];
	var remainTimeBar = quizSession.getElementsByClassName("remain-time-bar")[0];
	var counter = new Counter(0, 10000, animateFillBar);
	
	// ================================================================================================
	
	//var startNewQuizBar = quizSession.getElementsByClassName("start-new-quiz-bar")[0];
	//var startNewQuizButton = quizSession.getElementsByClassName("start-new-quiz-button")[0];
	var submitAnwerButton = quizSession.getElementsByClassName("submit-answer-button")[0];
	var skipQuestionButton = quizSession.getElementsByClassName("skip-question-button")[0];
	
	//function removeStartNewQuizBar() {
	//	startNewQuizBar.classList.add("hidden");
	//}
	
	
	// ================================================================================================
	
	function animateFillBar(widthInPercentage) {
		var red = 	Math.round((widthInPercentage * 255  + (100 - widthInPercentage) * 0  ) / 100);
		var green = Math.round((widthInPercentage * 0 	 + (100 - widthInPercentage) * 255) / 100);
		var blue = 	Math.round((widthInPercentage * 0    + (100 - widthInPercentage) * 0  ) / 100);
		
		remainTimeBar.style.width = widthInPercentage + "%";
		remainTimeBar.style.backgroundColor = "rgb(" + red + "," + green + "," + blue + ")";
	}
	
	function setQuestionTiming(remainTime, totalTime)
	{
		counter.stop();
		counter = new Counter(totalTime - remainTime, 
						      totalTime, 
						      animateFillBar);
		counter.start();
	}

	
	function setQuestionOrdinalNumber(questionOrdinalNumberVal) {
		questionOrdinalNumber.textContent = "Question " + questionOrdinalNumberVal;
	}
	
	function setQuestionText(questionTextVal) {
		questionText.textContent = questionTextVal;
	}
	
	
	function addAnswer(answerID, answerText) {
		var answer = answerTemplate.cloneNode(true);
		answer.classList.remove("hidden");
		answer.classList.remove("answer-template");
		answer.setAttribute("answerID", answerID); 
		answer.getElementsByClassName("answer-text")[0].textContent = answerText;
		answersBox.appendChild(answer);
	}
	
	function reset() {
		while (answersBox.firstChild)
			answersBox.removeChild(answersBox.firstChild);
	}
	

	function getAnswerIDs() {
		var answers = answersBox.getElementsByClassName("answer");
		var answersListSize = answers.length;
		var answerIDs = [];
		
		for(var idx = 0; idx < answersListSize; ++idx)
		{
			if(answers[idx].getElementsByClassName("checkbox-value")[0].checked)
				answerIDs.push(answers[idx].getAttribute("answerID"));
		}
		
		return answerIDs;
	}

	function addToParent(parentDOMElement) {
		parentDOMElement.appendChild(quizSession);
	}
	
	function fillBarDirectionToRight() {
		remainTimeBar.classList.remove("right");
	}
	
	function fillBarDirectionToLeft() {
		remainTimeBar.classList.add("right");
	}
	
	
	this.setQuestionTiming = setQuestionTiming;
	this.setQuestionOrdinalNumber = setQuestionOrdinalNumber;
	this.setQuestionText = setQuestionText;
	this.addAnswer = addAnswer;
	this.reset = reset;
	this.getAnswerIDs = getAnswerIDs;
	this.addToParrent = addToParent;
	this.fillBarDirectionToRight = fillBarDirectionToRight;
	this.fillBarDirectionToLeft = fillBarDirectionToLeft;
	
	// EVENT HANDLERS - CALLBACKS
	var submitAnswerButtonCallback = null;
	var skipQuestionButtonCallback = null;
	
	
	function submitAnswerCallbackFunction() {
		if(isFunction(submitAnswerButtonCallback))
			submitAnswerButtonCallback();
	}
	
	
	function skipQuestionCallbackFunction() {
		if(isFunction(skipQuestionButtonCallback))
			skipQuestionButtonCallback();
	}
	

	
	submitAnwerButton.addEventListener("click",  submitAnswerCallbackFunction);
	skipQuestionButton.addEventListener("click", skipQuestionCallbackFunction);
	
	
	function setSubmitAnswerCallbackFunction(submitAnswerCallbackFunction) {
		submitAnswerButtonCallback = submitAnswerCallbackFunction;
	}
	
	
	function setSkipQuestionCallbackFunction(skipQuestionCallbackFunction) {
		skipQuestionButtonCallback = skipQuestionCallbackFunction;
	}
	
	
	this.setSubmitAnswerCallbackFunction = setSubmitAnswerCallbackFunction;
	this.setSkipQuestionCallbackFunction = setSkipQuestionCallbackFunction;

}










function QuizSessionGetUserInfoPanel(onSubmitCallbackFunction, onSkipCallbackFunction) {
	var getUserInfoPanelTemplate = document.getElementsByClassName("get-user-info-panel-template")[0];	
	var onSubmitCallback = onSubmitCallbackFunction;
	var onSkipCallback = onSkipCallbackFunction;
	
	var getUserInfoPanel = getUserInfoPanelTemplate.cloneNode(true);
	getUserInfoPanel.classList.remove("get-user-info-panel-template");
	getUserInfoPanel.classList.remove("hidden");
	
	var nameInputField = getUserInfoPanel.getElementsByClassName("name-input")[0];
	var emailInputField = getUserInfoPanel.getElementsByClassName("email-input")[0];
	
	var submitButton = getUserInfoPanel.getElementsByClassName("submit-button")[0];
	var skipButton = getUserInfoPanel.getElementsByClassName("skip-button")[0];

	function submit() {
		var name = nameInputField.value;
		var email = emailInputField.value;
		
		onSubmitCallback(name, email);
	}
	
	function skip() {
		onSkipCallback();
	}
	
	submitButton.addEventListener("click", function(){ console.log("Submit"); submit(); });
	skipButton.addEventListener("click", function(){ console.log("Skip"); skip(); });
	
	
	function addToParent(parentDOMElement) {
		parentDOMElement.appendChild(getUserInfoPanel);
	}
	
	
	this.addToParrent = addToParent;
}









function QuizSessionFinishMessagePanel(onOpenCallbackFunction, onCloseCallbackFunction) {
	var quizFinishMessageTemplate = document.getElementsByClassName("quiz-finish-msg-template")[0];	
	var onOpenCallback = onOpenCallbackFunction;
	var onCloseCallback = onCloseCallbackFunction;
	
	var quizFinishMessage = quizFinishMessageTemplate.cloneNode(true);
	quizFinishMessage.classList.remove("quiz-finish-msg-template");
	quizFinishMessage.classList.remove("hidden");
	
	
	var openButton = quizFinishMessage.getElementsByClassName("open-button")[0];
	var closeButton = quizFinishMessage.getElementsByClassName("close-button")[0];

	function open() {
		onOpenCallback();
	}
	
	function close() {
		onCloseCallback();
	}
	
	openButton.addEventListener("click", function(){ console.log("Open"); open(); });
	closeButton.addEventListener("click", function(){ console.log("Close"); close(); });
	
	
	function addToParent(parentDOMElement) {
		parentDOMElement.appendChild(quizFinishMessage);
	}
		
	this.addToParrent = addToParent;
}


















function Counter(currentTimeMs, endTimeMs, callback) {
    this.currentValue = currentTimeMs / 20;
    this.endValue = endTimeMs / 20;
    this.interval = 20;
    this.callback = callback;
    this.id = null;
}

Counter.prototype.increment = function () {
    this.currentValue++;
};

Counter.prototype.start = function () {

	var that = this;
  
    var incrementAndCallCallback = function () {
        that.increment();
        that.callback(100 * that.currentValue / that.endValue);
        if (that.currentValue >= that.endValue) {
            that.stop();
        }
    };
    
    // Start call
    incrementAndCallCallback.call(this);
    
    this.id = setInterval(incrementAndCallCallback.bind(this), this.interval);
};

Counter.prototype.stop = function () {
	if(this.id != null)
		clearInterval(this.id);
};









function QuizSession(quizID, fillBarDirection) {
	
	var quizResultID = null;
	
	// INIT CONSTANTS
	const START_QUIZ_MSG = 1;
	const CURRENT_QUESTION_MSG = 2;
	const INVALID_REQUEST_MSG = 3;
	const SEND_ASNWER_MSG = 4;
	const SKIP_QUESITON_MSG = 5;
	const WAIT_TO_USER_DATA_MSG = 6;
	const QUESTION_TIMEOUT_MSG = 7;
	const SUCCESSFULL_END_OF_QUIZ_MSG = 8;
	const SEND_USER_DATA_MSG = 9;
	const SERVER_ERROR_MSG = 10;
	const INVALID_ANSWER_MSG = 11;
	
	
	// THAT
	var that = this;
	
	
	// QUIZ SESSION MAIN PANEL DISPLAY VARIABLES

	var quizSessionPanel = new QuizSessionMainPanel();
	var dispContainer = displayContainer.getDOMElement();
	quizSessionPanel.addToParrent(dispContainer);
	
	if(fillBarDirection === true) 
		quizSessionPanel.fillBarDirectionToLeft();
	else
		quizSessionPanel.fillBarDirectionToRight()
	
	
		
	function setFillBarDirection(fillBarDir) {
		if(fillBarDir === true) 
			quizSessionPanel.fillBarDirectionToLeft();
		else
			quizSessionPanel.fillBarDirectionToRight()
	}
		
	
	function startNewQuizCallback(newQuizID, trueFunc, falseFunc) {
		quizSessionList.push(new QuizSession(newQuizID, true));
		trueFunc();
	}
	
	this.setFillBarDirection = setFillBarDirection;
	
	
	// USER MESSAGES
	
	function getMessageTemplate() {
		return { messageType: null, answerIDs: [], email: null, name: null };
	}

	
	function sendStartMessage() {
		console.log("Send start message");
		var startMessage = getMessageTemplate();
		startMessage.messageType = START_QUIZ_MSG;
		webSocket.send(JSON.stringify(startMessage));
	}
	
	function sendSkipQuestionMessage() {
		var skipQuestionMessage = getMessageTemplate();
		skipQuestionMessage.messageType = SKIP_QUESITON_MSG;
		webSocket.send(JSON.stringify(skipQuestionMessage));
	}
	
	function sendAnswerMessage() {
		var sendAnswerMessage = getMessageTemplate();
		sendAnswerMessage.messageType = SEND_ASNWER_MSG;
		sendAnswerMessage.answerIDs = quizSessionPanel.getAnswerIDs();
		webSocket.send(JSON.stringify(sendAnswerMessage));
	}
	
	function sendUserDataMessage() {
		var sendUserDataMessage = getMessageTemplate();
		sendUserDataMessage.messageType = SEND_ASNWER_MSG;
		sendUserDataMessage.email = userEmail; 
		sendUserDataMessage.name = userName;
		webSocket.send(JSON.stringify(sendUserDataMessage));
	}
	
	this.sendSkipQuestionMessage = sendSkipQuestionMessage;
	this.sendAnswerMessage = sendAnswerMessage;
	
	
	
	
	// REGISTER CALLBACKS FOR BUTTONS
	
	quizSessionPanel.setSubmitAnswerCallbackFunction(sendAnswerMessage);
	quizSessionPanel.setSkipQuestionCallbackFunction(sendSkipQuestionMessage);
	quizStatusBar.setStartNewQuizCallbackFunction(startNewQuizCallback);
	
	
	
	
	// QUIZ SESSION FINISH MESSAGE PANEL
	function openFinishMessagePanelFunction() {
		location.replace("http://localhost:8080/RWA_project/rest/quiz/result/" + quizResultID);
		return;
	}
	
	function closeFinishMessagePanelFunction() {
		displayContainer.removeDOMElement(dispContainer)
		
		quizSessionList = quizSessionList.filter(function(element) {
				return element !== that;
			});
	
		console.log(that);
		console.log(quizSessionList);
		if(quizSessionList.length == 1)
			quizSessionList[0].setFillBarDirection(false);
		//location.replace("http://localhost:8080/RWA_project/quiz_preview.html");
		return;
	}
	
	
	var quizSessionFinishMessagePanel = new QuizSessionFinishMessagePanel(openFinishMessagePanelFunction, closeFinishMessagePanelFunction);
	
	
	function setupFinishMessagePanel() {
		while (dispContainer.firstChild)
			dispContainer.removeChild(dispContainer.firstChild);
		
		quizSessionFinishMessagePanel.addToParrent(dispContainer);
	}
	
	
	
		
	// INIT WEBSOCKET VARIABLES
	var webSocket = new WebSocket("ws://localhost:8080/RWA_project/quiz/" + quizID);
	
	webSocket.onopen 	= function(message)  { processOpen(message);    };
	webSocket.onmessage = function(message)  { processMessage(message); }
	webSocket.onclose 	= function(message)  { processClose(message);   };
	webSocket.onerror 	= function(message)  { processError(message);   };
	 
	// INIT GET USER DATA
	var getUserInfoPanel = null;
	var userName = "unknown";
	var userEmail = "unknown";
	
	if(userinfoName == null || userinfoEmail == null)
	{
		function submitUserData(name, email) {
			if(name != null)
				userName = name;
			if(email != null)
				userEmail = email;
		
			if(name != null && email != null)
			{
				userinfoName = userName;
				userinfoEmail = userEmail;
			}
			
			sendUserDataMessage();
		}
		
		function skip() {
			sendUserDataMessage();
		}
		
		getUserInfoPanel =  new QuizSessionGetUserInfoPanel(submitUserData, skip);
	}
	else
	{
		userName = userinfoName;
		userEmail = userinfoEmail;
	}
	
	
	function setupGetQuizInfoPanel() {
		while (dispContainer.firstChild)
			dispContainer.removeChild(dispContainer.firstChild);
		
		getUserInfoPanel.addToParrent(dispContainer);
	}
	
	
	
	
	// SERVER MESSAGES
	function processOpen(message) {
		console.log("Connected to server");
		console.log("sendStartMessage: ");
		console.log(sendStartMessage);
		sendStartMessage();
	}

	
	function logMessageType(serverMessage) {
		switch(serverMessage.messageType)
		{
			case START_QUIZ_MSG:
				console.log("START_QUIZ_MSG");
				break;
			
			case CURRENT_QUESTION_MSG:
				console.log("CURRENT_QUESTION_MSG")
				break;
				
			case INVALID_REQUEST_MSG:
				console.log("INVALID_REQUEST_MSG")
				break;
				
			case SEND_ASNWER_MSG:
				console.log("SEND_ASNWER_MSG")
				break;
		
			case SKIP_QUESITON_MSG:
				console.log("SKIP_QUESITON_MSG")
				break;
				
			case WAIT_TO_USER_DATA_MSG:
				console.log("WAIT_TO_USER_DATA_MSG")
				break;
				
			case QUESTION_TIMEOUT_MSG:
				console.log("QUESTION_TIMEOUT_MSG");
				break;
				
			case SUCCESSFULL_END_OF_QUIZ_MSG:
				console.log("SUCCESSFULL_END_OF_QUIZ_MSG with quizResultID: ", serverMessage.quizResultID);
				break;
				
			case SEND_USER_DATA_MSG:
				console.log("SEND_USER_DATA_MSG");
				break;
				
			case QUESTION_TIMEOUT_MSG:
				console.log("QUESTION_TIMEOUT_MSG")
				break;
				
			case SERVER_ERROR_MSG:
				console.log("SERVER_ERROR_MSG")
				break;
		
			case INVALID_ANSWER_MSG:
				console.log("INVALID_ANSWER_MSG")
				break;
				
			default:
				console.log("UNKNOWN MESSAGE TYPE");
		}
	}
	
	
	function processMessage(message) {
		console.log("Receive message from server");
		
		var serverMessage = JSON.parse(message.data);
		logMessageType(serverMessage);
		
		if(serverMessage.messageType == WAIT_TO_USER_DATA_MSG)
		{
			if(getUserInfoPanel == null)
				sendUserDataMessage();
			else
				setupGetQuizInfoPanel();
			return;
		}
		else if(serverMessage.messageType == SUCCESSFULL_END_OF_QUIZ_MSG)
		{
			quizResultID = serverMessage.quizResultID;
			setupFinishMessagePanel();
			return;
		}
		
		quizSessionPanel.reset();
		quizSessionPanel.setQuestionOrdinalNumber(serverMessage.question.questionOrdinalNumber);
		quizSessionPanel.setQuestionText(serverMessage.question.questionText);
		quizSessionPanel.setQuestionTiming(serverMessage.question.remainTime, serverMessage.question.totalTime);
	
		
		var answersListSize =  serverMessage.question.answerCollection.length;
		for(var idx = 0; idx < answersListSize; ++idx)
		{
			quizSessionPanel.addAnswer(serverMessage.question.answerCollection[idx].answerID,
									   serverMessage.question.answerCollection[idx].answerText);
		}
	}

	function processClose(message) {
		webSocket.send("Client disconnected");
		console.log("Client disconnected");
	}

	function processError(message) {
		console.log("Session error");
	}
	
}




function UserQuizCountSession(registerAsQuizPlayer) {
	
	const REGISTER_QUIZ_USER = 1;
	const QUIZ_STAT_MSG = 2;

	
	
	function getMessageTemplate() {
		return { messageType: null, userCount: null };
	}
	
	
	function sendRegisterUserMessage() {
		var registerUserMessage = getMessageTemplate();
		registerUserMessage.messageType = REGISTER_QUIZ_USER;
		webSocket.send(JSON.stringify(registerUserMessage));
	}
	
	
	// INIT WEBSOCKET VARIABLES	
	var webSocket = new WebSocket("ws://localhost:8080/RWA_project/user/stat");

	webSocket.onopen 	= function(message)  { processOpen(message);    };
	webSocket.onmessage = function(message)  { processMessage(message); }
	webSocket.onclose 	= function(message)  { processClose(message);   };
	webSocket.onerror 	= function(message)  { processError(message);   };

	function processOpen(message) {
		if(registerAsQuizPlayer)
			sendRegisterUserMessage();
		
		console.log("Connected to server");
	}
	
	function processMessage(message) {
		var serverMessage = JSON.parse(message.data);
		
		if(serverMessage.messageType == QUIZ_STAT_MSG) {
			updateQuizUserCountCallbackFunction(serverMessage.userCount);
			console.log(message.data);
		}
	}
	
	function processClose(message) {
		webSocket.send("Client disconnected");
		console.log("Client disconnected");
	}

	function processError(message) {
		console.log("Session error");
	}
	
	// QUIZ STAT EVENTS => CALLBACK
	var updateQuizUserCountCallback = null;
	
	
	function updateQuizUserCountCallbackFunction(userCount) {
		if(isFunction(updateQuizUserCountCallback))
			updateQuizUserCountCallback(userCount);
	} 
	
	
	function setUpdateQuizUserCountCallback(updateQuizUserCountCallbackFunc) {
		updateQuizUserCountCallback = updateQuizUserCountCallbackFunc;
	}
	
	this.setUpdateQuizUserCountCallback = setUpdateQuizUserCountCallback;
	
	
}



function updateUserCount(userCount) {
	console.log("User count: ", userCount);
	
	if(userCount != null)
		document.querySelectorAll(".user-count .user-count-text")[0].textContent = "User count: " + userCount;
}




var userQuizCountSession = new UserQuizCountSession(true);
userQuizCountSession.setUpdateQuizUserCountCallback(quizStatusBar.setUserCount);




quizSessionList.push(new QuizSession(thisQuizID, false));
//var quizSession2 = new QuizSession(1);
