
function isFunction(functionToCheck) {
	return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}


function QuizInfoBox() {
	
	
	var quizInfoBoxTemplate = document.getElementsByClassName("quiz-info-box-template")[0];
	
	var quizInfoBox = quizInfoBoxTemplate.cloneNode(true);
	quizInfoBox.classList.remove("quiz-info-box-template");
	quizInfoBox.classList.remove("hidden");
	
	
	var quizTitlePermamentValue = null;
	
	var totalQuizTime = null;
	var maximumQuizScore = null;
	var quizID = null;
	// GUI SETUP
	
	var quizTitle = quizInfoBox.getElementsByClassName("quiz-title")[0];
	//var quizTitleText = quizInfoBox.getElementsByClassName("quiz-title-text")[0];
	var quizOwner = quizInfoBox.getElementsByClassName("quiz-owner")[0];
	var numberOfQuestions = quizInfoBox.getElementsByClassName("number-of-questions")[0];
	var maximumScore = quizInfoBox.getElementsByClassName("maximum-score")[0];
	var totalTime = quizInfoBox.getElementsByClassName("total-time")[0];
	var activeInactiveButton = quizInfoBox.getElementsByClassName("active-inactive-button")[0];
	var activeInactiveButtonContent = activeInactiveButton.getElementsByClassName("content")[0];
	var addQuestionButton = quizInfoBox.getElementsByClassName("add-question-button")[0];
	
	var isQuizActive = null;

	
	
	function setQuizTitle(quizTitleVal) {
		quizTitle.value = quizTitleVal;
		quizTitlePermamentValue = quizTitleVal;
	}
	
	function restoreQuizTitleToPermanentValue() {
		quizTitle.value = quizTitlePermamentValue;
	}
	
	function getQuizTitle() {
		return quizTitle.value;
	}
	
	
	
	
	function setQuizOwner(quizOwnerVal) {
		quizOwner.textContent = "By " + quizOwnerVal;
	}
	
	
	function setNumberOfQuestions(numberOfQuestionsVal) {
		numberOfQuestions.textContent = "Number of questions: " + numberOfQuestionsVal;
	}

	
	function setMaximumScore(maximumScoreVal) {
		maximumQuizScore = maximumScoreVal;
		maximumScore.textContent = "Maximum score: " + maximumQuizScore;
	}
	
	function updateMaximumScore(scoreToAdd) {
		maximumQuizScore += scoreToAdd;
		maximumScore.textContent = "Maximum score: " + maximumQuizScore;
	}
	
	
	function setTotalTime(totalTimeVal) {
		totalQuizTime = totalTimeVal;
		totalTime.textContent = "Total time: " + Math.round(totalQuizTime / 60) + "min";
	}
	
	function updateTotalTime(timeToAdd) {
		totalQuizTime += timeToAdd;
		totalTime.textContent = "Total time: " + Math.round(totalQuizTime / 60) + "min";
	}
	
	
	function getTotalTime() {
		return totalQuizTime;
	}
	
	
	function getQuizTitle() {
		return quizTitle.value;
	}
	
	
	function setActiveState() {
		activeInactiveButton.classList.remove("inactive");
		activeInactiveButtonContent.textContent = "A";
		isQuizActive = true;
	}
	
	
	function setInactiveState() {
		activeInactiveButton.classList.add("inactive");
		activeInactiveButtonContent.textContent = "I";
		isQuizActive = false;
	}
	
	
	function setQuizID(quizIDVal) {
		if(quizIDVal != null)
		{
			quizID = quizIDVal;
			quizInfoBox.setAttribute("quizID", quizIDVal);
		}
	}
	
	
	function getQuizID() {
		return quizID;
	}
	
	
	
	function getDOMElement() {
		return quizInfoBox;
	}
	
	
	this.setQuizTitle = setQuizTitle;
	this.setQuizOwner = setQuizOwner;
	this.setNumberOfQuestions = setNumberOfQuestions;
	this.setMaximumScore = setMaximumScore;
	this.updateMaximumScore = updateMaximumScore;
	this.setTotalTime = setTotalTime;
	this.getTotalTime = getTotalTime;
	this.updateTotalTime = updateTotalTime;
	this.setActiveState = setActiveState;
	this.setInactiveState = setInactiveState;
	this.setQuizID = setQuizID;
	this.getQuizID = getQuizID;
	this.getDOMElement = getDOMElement;
	
	
	
	// CALLBACK ON EVENTS
	var activeInactiveButtonCallback = null;
	var addQuestionCallback = null;
	var setQuizTitleCallback = null;
	
	
	
	function activeInactiveButtonCallbackFunction() {
		console.log("quizActiveStatusButton clicked");

		
		if(isQuizActive == true)
		{	
			if(isFunction(activeInactiveButtonCallback))
				activeInactiveButtonCallback(quizID, false, setInactiveState, function(){});
		}
		else
		{
			if(isFunction(activeInactiveButtonCallback))
				activeInactiveButtonCallback(quizID, true, setActiveState, function(){});
		}
	}
	
	
	function addQuestionCallbackFunction() {
		console.log("add question clicked");
		if(isFunction(addQuestionCallback))
			addQuestionCallback(quizID, function(){}, function(){});
	}
	
	
	function setQuizTitleCallbackFunction() {
		var title = getQuizTitle();
		
		if(isFunction(setQuizTitleCallback)) 
			setQuizTitleCallback(getQuizID(), title, function(){ setQuizTitle(title); }, restoreQuizTitleToPermanentValue);
	}
	
	
	activeInactiveButton.addEventListener("click", activeInactiveButtonCallbackFunction);
	addQuestionButton.addEventListener("click", addQuestionCallbackFunction);
	quizTitle.addEventListener("focusout", setQuizTitleCallbackFunction);
	
	
	function setActiveInactiveButtonCallback(activeInactiveBtnCallback) {
		activeInactiveButtonCallback = activeInactiveBtnCallback;
	}
	
	
	function setAddQuestionCallback(addQuestionCallbackFunc) {
		addQuestionCallback = addQuestionCallbackFunc;
	}
	
	function setSetQuizTitleCallback(setQuizTitleCallbackFunc) {
		setQuizTitleCallback = setQuizTitleCallbackFunc;
	}
	
	
	
	this.setActiveInactiveButtonCallback = setActiveInactiveButtonCallback;
	this.setAddQuestionCallback = setAddQuestionCallback;
	this.setSetQuizTitleCallback = setSetQuizTitleCallback;
	
}











function QuestionsBox() {
	var questionsBoxTemplate = document.getElementsByClassName("questions-box-template")[0];
	
	var questionsList = [];
	
	var questionsBoxParent = questionsBoxTemplate.cloneNode(true);
	var questionsBox = questionsBoxParent.getElementsByClassName("questions-box")[0];
	questionsBoxParent.classList.remove("questions-box-template");
	questionsBoxParent.classList.remove("hidden");
	
	
	function getQuestionList() {
		return questionsList;
	}
	
	
	function addQuestion(question) {
		var questionDOM = question.getDOMElement();
		
		if(questionDOM.classList.contains("admin-panel-question"))
		{
			questionsBox.appendChild(questionDOM);
			questionsList.push(question);
		}
	}

	
	function removeQuestion(questionID) {
		questionsList = questionsList.filter(function(value, index, arr){ return value.getQuestionID() !== questionID });

		for(var child = questionsBox.firstElementChild; child !== null; child = child.nextElementSibling) {
		    
			if(child.getAttribute("questionID") == questionID)
			{
				questionsBox.removeChild(child);
				break;
			}
		}
	}
	
	
	function getQuestion(questionID) {
		var question = questionsList.filter(function(value, index, arr){ return value.getQuestionID() === questionID });
		
		if(question == null)
			return null;
		else
			return question[0];
	}
	
	
	function getNumberOfQuestions() {
		return questionsList.length;
	}
	
	
	function getDOMElement() {
		return questionsBoxParent;
	}

	
	this.addQuestion = addQuestion;
	this.getQuestion = getQuestion;
	this.removeQuestion = removeQuestion;
	this.getQuestionList = getQuestionList;
	this.getNumberOfQuestions = getNumberOfQuestions;
	this.getDOMElement = getDOMElement;
	
	
}













function AdminPanelQuestion() {
	var adminPanelQuestionTemplate = document.getElementsByClassName("admin-panel-question-template")[0];
	
	var adminPanelQuestion = adminPanelQuestionTemplate.cloneNode(true);
	adminPanelQuestion.classList.remove("admin-panel-question-template");
	adminPanelQuestion.classList.remove("hidden");
	
	var questionID = null;
	var answersList = [];
	
	var numberOfAnswersValue = null;
	var questionTextPermamentValue = null;
	var questionScorePermanentValue = null;
	var questionTimePermanentValue = null;
	
	var questionText = adminPanelQuestion.getElementsByClassName("question-text")[0];
	var numberOfAnswers = adminPanelQuestion.getElementsByClassName("number-of-answers-box")[0];
	var questionScore = adminPanelQuestion.getElementsByClassName("question-score-box")[0];
	var questionScoreText = questionScore.getElementsByClassName("question-score-text")[0];
	var questionTime = adminPanelQuestion.getElementsByClassName("question-time-box")[0];
	var questionTimeText = questionTime.getElementsByClassName("question-time-text")[0];
	var answersBox = adminPanelQuestion.getElementsByClassName("answers-box")[0];
	
	var removeQuestionButton =  adminPanelQuestion.getElementsByClassName("remove-question")[0];
	var addAnswerButton = adminPanelQuestion.getElementsByClassName("add-answer-button")[0];
	
	
	
	
	function setQuestionText(questionTextVal) {
		questionText.value = questionTextVal;
		questionTextPermamentValue = questionTextVal;
	}
	
	function restoreQuestionTextToPermanentValue() {
		questionText.value = questionTextPermamentValue;
	}
	
	function getQuestionText() {
		return questionText.value;
	}
	
	
	
	
	function setQuestionScore(questionScoreVal) {
		questionScoreText.value = questionScoreVal;
		questionScorePermanentValue = questionScoreVal;
	}
	
	function restoreQuestionScoreToPermanentValue() {
		questionScoreText.value = questionScorePermanentValue;
	}
	
	function getQuestionScore() {
		return questionScoreText.value;
	}
	
	
	
	
	function setQuestionTime(questionTimeVal) {
		questionTimeText.value = questionTimeVal;
		questionTimePermanentValue = questionTimeVal;
	}
	
	function restoreQuestionTimeToPermanentValue() {
		questionTimeText.value = questionTimePermanentValue;
	}
	
	function getQuestionTime() {
		return questionTimeText.value;
	}
	
	
	
	
	
	
	function setNumberOfAnswers(numberOfAnswersVal) {
		numberOfAnswersValue = numberOfAnswersVal
		numberOfAnswers.textContent = numberOfAnswersVal;
	}
	
	
	
	

	
	function addAnswer(answer) {
		var answerDOM = answer.getDOMElement();
		
		if(answerDOM.classList.contains("admin-panel-answer"))
		{
			++numberOfAnswersValue;
			numberOfAnswers.textContent = numberOfAnswersValue;
			answersBox.appendChild(answerDOM);
			answersList.push(answer);
		}
	}
	
	
	function removeAnswer(answerID) {
		answersList = answersList.filter(function(value, index, arr){ return value.getAnswerID() !== answerID });
	
		for(var child = answersBox.firstElementChild; child !== null; child = child.nextElementSibling) {
		    
			if(child.getAttribute("answerID") == answerID)
			{
				--numberOfAnswersValue;
				numberOfAnswers.textContent = numberOfAnswersValue;
				answersBox.removeChild(child);
				return true;
			}
		}
		
		return false;
	}
	
	
	
	
	
	function setQuestionID(questionIDVal) {
		if(questionIDVal != null)
		{
			questionID = questionIDVal;
			adminPanelQuestion.setAttribute("questionID", questionIDVal);
		}
	}
	
	
	function getQuestionID() {
		return questionID;
	}
	
	
	function getDOMElement() {
		return adminPanelQuestion;
	}

	
	this.setQuestionText = setQuestionText;
	this.setNumberOfAnswers = setNumberOfAnswers;
	this.setQuestionScore = setQuestionScore;
	this.getQuestionScore = getQuestionScore;
	this.getQuestionTime = getQuestionTime;
	this.setQuestionTime = setQuestionTime;
	this.addAnswer = addAnswer;
	this.removeAnswer = removeAnswer;
	this.setQuestionID = setQuestionID;
	this.getQuestionID = getQuestionID;
	this.getDOMElement = getDOMElement;
	
	
	
	// CALLBACK ON EVENTS
	var setQuestionTextCallback = null;
	var setQuestionScoreCallback = null;
	var setQuestionTimeCallback = null;
	var removeQuestionCallback = null;
	var addAnswerCallback = null;
	

	
	
	function setQuestionTextCallbackFunction() {
		var text = getQuestionText();
		
		if(isFunction(setQuestionTextCallback)) 
			setQuestionTextCallback(questionID, text, function(){ setQuestionText(text); }, restoreQuestionTextToPermanentValue);
	}
	
	
	function setQuestionScoreCallbackFunction() {
		var score = getQuestionScore();
		
		if(isFunction(setQuestionScoreCallback))
			setQuestionScoreCallback(questionID, score, function(){ setQuestionScore(score); }, restoreQuestionScoreToPermanentValue);
	}
	
	
	function setQuestionTimeCallbackFunction() {	
		var time = getQuestionTime();
		
		if(isFunction(setQuestionTimeCallback))
			setQuestionTimeCallback(questionID, time, function() { setQuestionTime(time); }, restoreQuestionTimeToPermanentValue);
	}
	
	
	function addAnswerCallbackFunction(event) {
		event.stopPropagation();
		
		if(isFunction(addAnswerCallback))
			addAnswerCallback(questionID, function() {}, function() {});
	}
	
	
	function removeQuestionCallbackFunction(event) {
		event.stopPropagation();
		
		if(isFunction(removeQuestionCallback))
			removeQuestionCallback(questionID, function(){}, function(){});
	}
	
	
	
	questionText.addEventListener("focusout", setQuestionTextCallbackFunction);
	questionScoreText.addEventListener("focusout", setQuestionScoreCallbackFunction);
	questionTimeText.addEventListener("focusout", setQuestionTimeCallbackFunction);
	addAnswerButton.addEventListener("click", addAnswerCallbackFunction);
	removeQuestionButton.addEventListener("click", removeQuestionCallbackFunction);
	
	
	function setSetQuestionCallbackFunction(setQuestionTextCallbackFunc) {
		setQuestionTextCallback = setQuestionTextCallbackFunc;
	}
	
	function setSetQuestionScoreCallbackFunction(questionScoreCallbackFunc) {
		setQuestionScoreCallback = questionScoreCallbackFunc;
	}	
	
	function setSetQuestionTimeCallbackFunction(questionTimeCallbackFunc) {
		setQuestionTimeCallback = questionTimeCallbackFunc;
	}
	
	function setAddAnswerCallbackFunction(addAnswerCallbackFunc) {
		addAnswerCallback = addAnswerCallbackFunc;
	}
	
	function setRemoveQuestionCallbackFunction(removeQuestionCallbackFunc) {
		removeQuestionCallback = removeQuestionCallbackFunc;
	}
	
	
	
	
	this.setSetQuestionCallbackFunction = setSetQuestionCallbackFunction;
	this.setSetQuestionScoreCallbackFunction = setSetQuestionScoreCallbackFunction;
	this.setSetQuestionTimeCallbackFunction = setSetQuestionTimeCallbackFunction;
	this.setAddAnswerCallbackFunction = setAddAnswerCallbackFunction;
	this.setRemoveQuestionCallbackFunction = setRemoveQuestionCallbackFunction;
}






function AdminPanelAnswer() {
	var adminPanelAnswerTemplate = document.getElementsByClassName("admin-panel-answer-template")[0];
	
	var adminPanelAnswer = adminPanelAnswerTemplate.cloneNode(true);
	adminPanelAnswer.classList.remove("admin-panel-answer-template");
	adminPanelAnswer.classList.remove("hidden");

	var answerID = null;
	var answerTextPermamentValue = null;
	var isAnswerCorrectFlag = null;
	
	var answerText = adminPanelAnswer.getElementsByClassName("answer-text")[0];
	var isAnswerCorrectButton = adminPanelAnswer.getElementsByClassName("is-answer-correct-box")[0];
	var removeAnswerButton = adminPanelAnswer.getElementsByClassName("remove-answer-button")[0];
	
	
	
	function setAnswerText(answerTextVal) {
		answerText.value = answerTextVal;
		answerTextPermamentValue = answerTextVal;
	}
	
	function restoreAnswerTextToPermanentValue() {
		answerText.value = answerTextPermamentValue;
	}
	
	function getAnswerText() {
		return answerText.value;
	}
	
	
	
	
	function setAnswerCorrect() {
		isAnswerCorrectFlag = true;
		isAnswerCorrectButton.classList.remove("false");
	}
	
	
	function setAnswerIncorrect() {
		isAnswerCorrectFlag = false;
		isAnswerCorrectButton.classList.add("false");
	}
	
	
	
	function setAnswerID(answerIDVal) {
		if(answerIDVal != null)
		{
			answerID = answerIDVal;
			adminPanelAnswer.setAttribute("answerID", answerIDVal);
		}
	}
	
	
	function getAnswerID() {
		return answerID;
	}
	

	
	function getDOMElement() {
		return adminPanelAnswer;
	}

	
	this.setAnswerText = setAnswerText;
	this.setAnswerCorrect = setAnswerCorrect;
	this.setAnswerIncorrect = setAnswerIncorrect;
	this.setAnswerID = setAnswerID;
	this.getAnswerID = getAnswerID;
	this.getDOMElement = getDOMElement;
	
	
	
	// CALLBACK ON EVENTS
	var setAnswerTextCallback = null;
	var isAnswerCorrectCallback = null;
	var deleteAnswerCallback = null;
	
	
	
	function setAnswerTextCallbackFunction() {
		var text = getAnswerText();
		
		if(isFunction(setAnswerTextCallback))
			setAnswerTextCallback(answerID, text, function(){ setAnswerText(text); }, restoreAnswerTextToPermanentValue);
	}
	
	
	function isAnswerCorrectCallbackFunction() {
		
		if(isAnswerCorrectFlag == true)
		{	
			if(isFunction(isAnswerCorrectCallback))
				isAnswerCorrectCallback(answerID, false, setAnswerIncorrect, function(){});
		}
		else
		{
			if(isFunction(isAnswerCorrectCallback))
				isAnswerCorrectCallback(answerID, true, setAnswerCorrect, function(){});
		}
	}
	
	
	function deleteAnswerCallbackFunction() {
		deleteAnswerCallback(answerID, function(){}, function(){});
	}
	
	
	answerText.addEventListener("focusout", setAnswerTextCallbackFunction);
	isAnswerCorrectButton.addEventListener("click", isAnswerCorrectCallbackFunction);
	removeAnswerButton.addEventListener("click", deleteAnswerCallbackFunction);
	
	
	
	function setSetAnswerTextCallbackFunction(setAnswerTextCallbackFunc) {
		setAnswerTextCallback = setAnswerTextCallbackFunc;
	}
	
	
	function setAnswerCorrectButtonCallbackFunction(answerCorrectButtonCallbackFunc) {
		isAnswerCorrectCallback = answerCorrectButtonCallbackFunc;
	}

	function setRemoveAnswerCallbackFunction(removeAnswerCallbackFunc) {
		deleteAnswerCallback = removeAnswerCallbackFunc;
	}
	
	
	this.setSetAnswerTextCallbackFunction = setSetAnswerTextCallbackFunction;
	this.setAnswerCorrectButtonCallbackFunction =  setAnswerCorrectButtonCallbackFunction;
	this.setRemoveAnswerCallbackFunction = setRemoveAnswerCallbackFunction;
}









function QuizAdminPanelSession(quizID) {
	
	var displayContainer = document.getElementById("display-container");
	var quizInfoBox = new QuizInfoBox();
	var questionsBox = new QuestionsBox();
	
	
	
	
	
	// CALLBACKS OF MESSAGE TO SERVER
	 
	var addQuestionMessageCallback = function(questionData) {
		
		var newQuestion = new AdminPanelQuestion();
		
		newQuestion.setQuestionText(questionData.questionText);
		newQuestion.setNumberOfAnswers(questionData.answerCollection == null ? 0 : questionData.answerCollection.length);
		newQuestion.setQuestionScore(questionData.questionScore);
		newQuestion.setQuestionTime(questionData.questionTime);
		newQuestion.setQuestionID(questionData.questionID);
		
		newQuestion.setSetQuestionCallbackFunction(sendSetQuestionTextMessage);
		newQuestion.setSetQuestionScoreCallbackFunction(sendSetQuestionScoreMessage);
		newQuestion.setSetQuestionTimeCallbackFunction(sendSetQuestionTimeMessage);
		newQuestion.setAddAnswerCallbackFunction(sendAddAnswerMessage);
		newQuestion.setRemoveQuestionCallbackFunction(sendRemoveQuestionMessage);
		
		questionsBox.addQuestion(newQuestion);
		updateQuizPageView();
	}
	
	
	
	
	var addAnswerMessageCallback = function(questionID, answerData) {
		
		var question = questionsBox.getQuestion(questionID);
		
		if(question != null) {
			var newAnswer = new AdminPanelAnswer();
			
			newAnswer.setAnswerID(answerData.answerID);
			newAnswer.setAnswerText(answerData.answerText);
			if(answerData.isAnswerCorrect)
				newAnswer.setAnswerCorrect();
			else
				newAnswer.setAnswerIncorrect();
			
			
			
			newAnswer.setSetAnswerTextCallbackFunction(sendSetAnswerTextMessage);
			newAnswer.setAnswerCorrectButtonCallbackFunction(sendAnswerCorrectStateMessage);
			newAnswer.setRemoveAnswerCallbackFunction(sendRemoveAnswerMessage);
			
			question.addAnswer(newAnswer);
			
		}
	}
	
	
	
	var removeQuestionMessageCallback = function(questionID) {
		quizInfoBox.updateMaximumScore(-(questionsBox.getQuestion(questionID).getQuestionScore()));
		quizInfoBox.updateTotalTime(-(questionsBox.getQuestion(questionID).getQuestionTime()));
		
		questionsBox.removeQuestion(questionID);
		updateQuizPageView();
	}
	
	var removeAnswerMessageCallback = function (answerID) {
		
		for(var idx = 0; idx < questionsBox.getQuestionList().length; ++idx)
			if(questionsBox.getQuestionList()[idx].removeAnswer(answerID))
				break;
	}
	
	
	
	
	
	// MESSAGES TO SERVER	
	function sendSetQuizTitleMessage(quizID, title, trueFunc, falseFunc) {
		
		var dataJSON = JSON.stringify({quizTitle: title});
		var httpRequest = new XMLHttpRequest();
		
		httpRequest.onreadystatechange = function() {
			if (this.readyState == 4)
			{	
				if(this.status == 200)					
					trueFunc();
				else
					falseFunc();
			}
		};
		
		httpRequest.open("PUT", "/RWA_project/rest/admin/quiz/" + quizID + "/title", true);
		httpRequest.setRequestHeader("Content-type", "application/json");
		httpRequest.send(dataJSON);
	}
	
	
	function sendDeleteQuizMessage(quizID, trueFunc, falseFunc) {
		var httpRequest = new XMLHttpRequest();
		
		httpRequest.onreadystatechange = function() {
			if (this.readyState == 4)
			{	
				if(this.status == 200)
					trueFunc();
				else
					falseFunc();
			}
		};
		
		
		httpRequest.open("DELETE", "/RWA_project/rest/admin/quiz/" + quizID, true);
		httpRequest.send();
	}
	
	function sendChangeActiveStatusOfQuizMessage(quizID, quizState, trueFunc, falseFunc) {
		
		var httpRequest = new XMLHttpRequest();
		
		httpRequest.onreadystatechange = function() {
			if (this.readyState == 4)
			{	
				if(this.status == 200)
					trueFunc();
				else
					falseFunc();
			}
		};
		
		
		httpRequest.open("PUT", "/RWA_project/rest/admin/quiz/" + quizID + ((quizState == true) ? "/active" : "/inactive"), true);
		httpRequest.send();
	}
	
	
	

	
	
	
	function sendAddQuestionMessage(quizID, trueFunc, falseFunc) {
		
		var httpRequest = new XMLHttpRequest();
		
		httpRequest.onreadystatechange = function() {
			if (this.readyState == 4)
			{	
				if(this.status == 200)
				{
					if(isFunction(addQuestionMessageCallback))
						addQuestionMessageCallback(JSON.parse(httpRequest.responseText));
					
					trueFunc();
				}
				else
					falseFunc();
			}
		};
		
		httpRequest.open("POST", "/RWA_project/rest/admin/question/" + quizID, true);
		httpRequest.send();
	}
	
	
	
	
	
	
	
	
	function sendSetQuestionTextMessage(questionID, text, trueFunc, falseFunc) {
		
		var dataJSON = JSON.stringify({questionText: text});
		var httpRequest = new XMLHttpRequest();
		
		httpRequest.onreadystatechange = function() {
			if (this.readyState == 4)
			{	
				if(this.status == 200)					
					trueFunc();
				else
					falseFunc();
			}
		};
		
		httpRequest.open("PUT", "/RWA_project/rest/admin/question/" + questionID + "/text", true);
		httpRequest.setRequestHeader("Content-type", "application/json");
		httpRequest.send(dataJSON);
	}
	
	
	
	
	
	
	
	
	function sendSetQuestionScoreMessage(questionID, score, trueFunc, falseFunc) {
		
		var httpRequest = new XMLHttpRequest();
		
		httpRequest.onreadystatechange = function() {
			if (this.readyState == 4)
			{	
				if(this.status == 200)
				{
					updateQuizPageView();
					trueFunc();
				}
				else
					falseFunc();
			}
		};
		
		httpRequest.open("PUT", "/RWA_project/rest/admin/question/" + questionID + "/score/" + score, true);
		httpRequest.send();
	}
	
	
	
	
	
	
	function sendSetQuestionTimeMessage(questionID, time, trueFunc, falseFunc) {
		
		var httpRequest = new XMLHttpRequest();
		
		httpRequest.onreadystatechange = function() {
			if (this.readyState == 4)
			{	
				if(this.status == 200)
				{
					updateQuizPageView();
					trueFunc();
				}
				else
					falseFunc();
			}
		};
		
		httpRequest.open("PUT", "/RWA_project/rest/admin/question/" + questionID + "/time/" + time, true);
		httpRequest.send();
	}
	
	
	function sendAddAnswerMessage(questionID, trueFunc, falseFunc) {
		
		var httpRequest = new XMLHttpRequest();
		
		httpRequest.onreadystatechange = function() {
			if (this.readyState == 4)
			{	
				if(this.status == 200)
				{
					if(isFunction(addAnswerMessageCallback))
						addAnswerMessageCallback(questionID, JSON.parse(httpRequest.responseText));
					
					trueFunc();
				}
				else
					falseFunc();
			}
		};
		
		httpRequest.open("POST", "/RWA_project/rest/admin/answer/" + questionID, true);
		httpRequest.send();
	}
	
	
	
	function sendRemoveQuestionMessage(questionID, trueFunc, falseFunc) { 
		var httpRequest = new XMLHttpRequest();
		
		httpRequest.onreadystatechange = function() {
			if (this.readyState == 4)
			{	
				if(this.status == 200)
				{
					if(isFunction(removeQuestionMessageCallback))
						removeQuestionMessageCallback(questionID);
					
					trueFunc();
				}
				else
					falseFunc();
			}
		};
		
		httpRequest.open("DELETE", "/RWA_project/rest/admin/question/" + questionID, true);
		httpRequest.send();
	}
	
	
	
	
	function sendSetAnswerTextMessage(answerID, text, trueFunc, falseFunc) {
		
		var dataJSON = JSON.stringify({answerText: text});
		var httpRequest = new XMLHttpRequest();
		
		httpRequest.onreadystatechange = function() {
			if (this.readyState == 4)
			{	
				if(this.status == 200)
					trueFunc();
				else
					falseFunc();
			}
		};
		
		httpRequest.open("PUT", "/RWA_project/rest/admin/answer/" + answerID + "/text", true);
		httpRequest.setRequestHeader("Content-type", "application/json");
		httpRequest.send(dataJSON);
	}
	
	
	
	
	
	function sendAnswerCorrectStateMessage(answerID, answerCorrectState, trueFunc, falseFunc) {
		
		var httpRequest = new XMLHttpRequest();
		
		httpRequest.onreadystatechange = function() {
			if (this.readyState == 4)
			{	
				if(this.status == 200)
					trueFunc();
				else
					falseFunc();
			}
		};
		
		
		httpRequest.open("PUT", "/RWA_project/rest/admin/answer/" + answerID + ((answerCorrectState == true) ? "/correct" : "/incorrect"), true);
		httpRequest.send();
	}
	
	
	
	
	function sendRemoveAnswerMessage(answerID, trueFunc, falseFunc) {
		
		var httpRequest = new XMLHttpRequest();
		
		httpRequest.onreadystatechange = function() {
			if (this.readyState == 4)
			{	
				if(this.status == 200)
				{
					if(isFunction(removeAnswerMessageCallback))
						removeAnswerMessageCallback(answerID);
					
					trueFunc();
				}
				else
					falseFunc();
			}
		};
		
		httpRequest.open("DELETE", "/RWA_project/rest/admin/answer/" + answerID, true);
		httpRequest.send();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	function getQuiz(quizID) {
			
		var httpRequest = new XMLHttpRequest();
			
		httpRequest.onreadystatechange = function() {
			if (this.readyState == 4)
			{	
				if(this.status == 200)
				{
					if(isFunction(addQuestionMessageCallback))
						generateQuizPageView(JSON.parse(httpRequest.responseText));
				}
			};	
		}
		
		httpRequest.open("GET", "/RWA_project/rest/admin/quiz/" + quizID, true);
		httpRequest.send();
	}
	
	
	
	function getMaximumScore(questionList) {
		
		var score = 0;
		
		for(var idx = 0; idx < questionList.length; ++idx)
			score += questionList[idx].questionScore;
		
		return score;
	}
	
	
	function getQuizTime(questionList) {
		var time = 0;
		
		for(var idx = 0; idx < questionList.length; ++idx)
			time += questionList[idx].questionTime;
		
		return time;
	}
	
	
	function generateQuizQuestionView(questionList) {
		
		for(var idx = 0; idx < questionList.length; ++idx)
		{
			var newQuestion = new AdminPanelQuestion();
			
			newQuestion.setQuestionText(questionList[idx].questionText);
			newQuestion.setNumberOfAnswers(0); // will be increased after answers had been added
			newQuestion.setQuestionScore(questionList[idx].questionScore);
			newQuestion.setQuestionTime(questionList[idx].questionTime);
			newQuestion.setQuestionID(questionList[idx].questionID);
			
			generateAnswerView(newQuestion, questionList[idx].answerCollection)
			
			newQuestion.setSetQuestionCallbackFunction(sendSetQuestionTextMessage);
			newQuestion.setSetQuestionScoreCallbackFunction(sendSetQuestionScoreMessage);
			newQuestion.setSetQuestionTimeCallbackFunction(sendSetQuestionTimeMessage);
			newQuestion.setAddAnswerCallbackFunction(sendAddAnswerMessage);
			newQuestion.setRemoveQuestionCallbackFunction(sendRemoveQuestionMessage);
			
			questionsBox.addQuestion(newQuestion);
		}
	}
	
	
	function generateAnswerView(question, answerList) {
		for(var idx = 0; idx < answerList.length; ++idx)
		{
			var newAnswer = new AdminPanelAnswer();
			newAnswer.setAnswerText(answerList[idx].answerText);
			
			if(answerList[idx].isAnswerCorrect)
				newAnswer.setAnswerCorrect();
			else
				newAnswer.setAnswerIncorrect();
			
			newAnswer.setAnswerID(answerList[idx].answerID);
			
			newAnswer.setSetAnswerTextCallbackFunction(sendSetAnswerTextMessage);
			newAnswer.setAnswerCorrectButtonCallbackFunction(sendAnswerCorrectStateMessage);
			newAnswer.setRemoveAnswerCallbackFunction(sendRemoveAnswerMessage);
			
			question.addAnswer(newAnswer);
		}
	}
	
	
	function generateQuizPageView(quizData) {
		console.log(quizData);
		quizInfoBox.setQuizTitle(quizData.title);
		quizInfoBox.setQuizOwner("Unknown");
		quizInfoBox.setNumberOfQuestions(quizData.questionCollection.length);
		quizInfoBox.setMaximumScore(getMaximumScore(quizData.questionCollection));
		quizInfoBox.setTotalTime(getQuizTime(quizData.questionCollection));
		if(quizData.isQuizActive)
			quizInfoBox.setActiveState();
		else
			quizInfoBox.setInactiveState();
		quizInfoBox.setQuizID(quizData.quizID);
		
		quizInfoBox.setActiveInactiveButtonCallback(sendChangeActiveStatusOfQuizMessage);
		quizInfoBox.setAddQuestionCallback(sendAddQuestionMessage);
		quizInfoBox.setSetQuizTitleCallback(sendSetQuizTitleMessage);
		
		generateQuizQuestionView(quizData.questionCollection);
		
		
		displayContainer.appendChild(quizInfoBox.getDOMElement());
		displayContainer.appendChild(questionsBox.getDOMElement());
	}
	

	function updateQuizPageView() {
		var questionList = questionsBox.getQuestionList();
		
		var maximumScore = 0;
		var totalTime = 0;
		
		for(var idx = 0; idx < questionList.length; ++idx) {
			console.log(idx, " ", questionList[idx].getQuestionScore(), " ", questionList[idx].getQuestionTime());
			
			score = questionList[idx].getQuestionScore();
			time = questionList[idx].getQuestionTime();
			
			if(String(score) === score)
				score = parseInt(score);
			
			if(String(time) === time)
				time = parseInt(time);
			
			totalTime += time;
			maximumScore += score;
		}
		
		quizInfoBox.setMaximumScore(maximumScore);
		quizInfoBox.setTotalTime(totalTime);
		quizInfoBox.setNumberOfQuestions(questionList.length);
	}
	
	// SET DELETE QUIZ BUTTON
	var deleteQuizButton = document.getElementsByClassName("delete-quiz-button")[0];
	deleteQuizButton.addEventListener("click", function() { sendDeleteQuizMessage(quizID, redirectToMainQuizPanel, function(){}); });
	
	getQuiz(quizID);
}

var quizAdminPanelSession = new QuizAdminPanelSession(quizID);



function redirectToMainQuizPanel() {
	window.location.replace("http://localhost:8080/RWA_project/rest/admin/panel/");
}         

function redirectToQuizInbox() {
	window.location.replace("http://localhost:8080/RWA_project/rest/admin/panel/quiz/" + quizID + "/inbox");
}   



