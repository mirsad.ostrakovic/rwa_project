function fetchJSONFile(path, callback) {
	var httpRequest = new XMLHttpRequest();
	
	httpRequest.onreadystatechange = function() {
		if (httpRequest.readyState === 4) {
			if (httpRequest.status === 200) {
				console.log("fetchJSONFile: ", httpRequest.responseText)
				var data = JSON.parse(httpRequest.responseText);
				if (callback) callback(data);
			}
		}
	};
	
	httpRequest.open('GET', path);
	httpRequest.send(); 
} 



function isFunction(functionToCheck) {
	return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}


function QuizResultPreview(previewVariant) {
	var quizResultPreviewTemplate = document.getElementsByClassName("quiz-result-preview-template")[0];
	var quizResultPreview = quizResultPreviewTemplate.cloneNode(true);
	
	quizResultPreview.classList.remove("quiz-result-preview-template");
	quizResultPreview.classList.remove("hidden");
	
	var quizResultID = null;
	
	var quizUserName = quizResultPreview.getElementsByClassName("quiz-user-name")[0];
	var quizShortDescription = quizResultPreview.getElementsByClassName("quiz-short-description")[0];
	var quizScoreBox = quizResultPreview.getElementsByClassName("score-box")[0];
	var quizResultPreviewBox = quizResultPreview.getElementsByClassName("quiz-result-preview")[0];
	
	
	function viewQuizResultButtonCallback() {
		window.location.replace("http://localhost:8080/RWA_project/rest/admin/panel/quiz/result/" + quizResultID);	
	}
	
	
	
	
	function setQuizResultID(quizResultIDVal) {
		quizResultID = quizResultIDVal;
	}
	
	function setBackground1() {
		quizResultPreviewBox.classList.remove("bg-color-2");
		quizResultPreviewBox.classList.add("bg-color-1");
	}
	
	function setBackground2() {
		quizResultPreviewBox.classList.remove("bg-color-1");
		quizResultPreviewBox.classList.add("bg-color-2");
	}
	
	function setQuizUserName(userName) {
		quizUserName.textContent = userName;
	}
	
	function setQuizShortDescription(shortDescription) {
		quizShortDescription.textContent = shortDescription;
	}

	function setQuizScore(quizScore) {
		quizScoreBox.textContent = quizScore;
	}
	
	
	function getDOMElement() {
		return quizResultPreview;
	} 
	
	
	
	
	this.setQuizResultID = setQuizResultID;
	this.setQuizUserName = setQuizUserName;
	this.setQuizShortDescription = setQuizShortDescription;
	this.setQuizScore = setQuizScore;
	this.getDOMElement = getDOMElement;
	this.setBackground1 = setBackground1;
	this.setBackground2 = setBackground2;
	
	
	
	quizResultPreviewBox.addEventListener("click", viewQuizResultButtonCallback);
	
	
	
	if(previewVariant === true)
		setBackground1();
	else
		setBackground2();
	
}




function generateQuizResultPreviewList(quizID, from, to) 
{	
	
	var quizResultPreviewContainer = document.getElementsByClassName("quiz-result-previews-box")[0];
	var URL = "http://localhost:8080/RWA_project/rest/admin/quizresult/quiz/" + quizID;
	 
	 fetchJSONFile(URL, function(data) {
	
		 console.log(data);
		
		 var quizResultsList = data;
		 
		 while (quizResultPreviewContainer.firstChild)
			 quizResultPreviewContainer.removeChild(quizResultPreviewContainer.firstChild);
		 
		 for(var idx = 0; idx < quizResultsList.length; ++idx) {
			 var quizResultPreview = new QuizResultPreview(idx % 2 == 1 ? true : false);
			 
			 quizResultPreview.setQuizResultID(quizResultsList[idx].quizResultID);
			 quizResultPreview.setQuizUserName(quizResultsList[idx].userName);
			 quizResultPreview.setQuizShortDescription(quizResultsList[idx].correctAnswerCount + " of " +
					 								   quizResultsList[idx].questionCollection.length + " questions are correct");
			 quizResultPreview.setQuizScore(quizResultsList[idx].quizScore);
		
			 
			 quizResultPreviewContainer.appendChild(quizResultPreview.getDOMElement());
		 }
		
	 });
}

generateQuizResultPreviewList(quizID);

function downloadInbox(quizID) {
	var httpRequest = new XMLHttpRequest();
	
	httpRequest.onreadystatechange = function(data) {
		if (this.readyState == 4)
		{	
			if(this.status == 200)
			{
				console.log(data);
				
				 var file = new Blob([httpRequest.responseText], {type: "text/csv"});
				    if (window.navigator.msSaveOrOpenBlob) // IE10+
				        window.navigator.msSaveOrOpenBlob(file, "quiz_inbox_quiz_" + quizID + ".csv");
				    else { // Others
				        var a = document.createElement("a"),
				                url = URL.createObjectURL(file);
				        a.href = url;
				        a.download = "quiz_inbox_quiz_" + quizID + ".csv";
				        document.body.appendChild(a);
				        a.click();
				        setTimeout(function() {
				            document.body.removeChild(a);
				            window.URL.revokeObjectURL(url);  
				        }, 0); 
				    }
				
			}
		}
	};
	
	httpRequest.open("GET", "http://localhost:8080/RWA_project/rest/admin/quizresult/quiz/" + quizID + "/csv", true);
	httpRequest.send();
}

function downloadInboxButtonCallback() { downloadInbox(quizID); }

function getUserOwner(quizID) {
	var quizOwner = document.getElementsByClassName("quiz-owner")[0];
	var quizTitle = document.getElementsByClassName("quiz-title")[0];
		
	var httpRequest = new XMLHttpRequest();
		
	httpRequest.onreadystatechange = function() {
		if (this.readyState == 4)
		{	
			if(this.status == 200)
			{
				quizData = JSON.parse(httpRequest.responseText)
				if(quizData.owner != null && quizData.owner.username != null)
					quizOwner.textContent = "By " + quizData.owner.username;
				
				console.log("QuizTitle: ", quizData.title);
				quizTitle.textContent = quizData.title;
				
				console.log("QuizData: ", quizData);
			}
		}
	}
		
		httpRequest.open("GET", "/RWA_project/rest/admin/quiz/" + quizID, true);
		httpRequest.send();
}

getUserOwner(quizID);
