function fetchJSONFile(path, callback) {
	var httpRequest = new XMLHttpRequest();
	
	httpRequest.onreadystatechange = function() {
		if (httpRequest.readyState === 4) {
			if (httpRequest.status === 200) {
				console.log("fetchJSONFile: ", httpRequest.responseText)
				var data = JSON.parse(httpRequest.responseText);
				if (callback) callback(data);
			}
		}
	};
	
	httpRequest.open('GET', path);
	httpRequest.send(); 
} 


function isFunction(functionToCheck) {
	return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}





var getRandomQuizPreviewPairFlag = false;

function getRandomQuizPreviewPair() 
{	
	if(getRandomQuizPreviewPairFlag == true)
		return;
	
	getRandomQuizPreviewPairFlag = true;
	
	 URL = "http://localhost:8080/RWA_project/rest/quiz/info/random";
	 
	 fetchJSONFile(URL, function(data) {
		 
			var quizPreviewFrontList = document.querySelectorAll(".front .quiz-preview");
			for(var idx = 0; idx < quizPreviewFrontList.length; ++idx)
			{
				quizPreviewFrontList[idx].getElementsByClassName("quiz-start-button")[0].setAttribute("thisQuizID", data[idx].quizID);
				
				if(idx == 0)
					quizPreviewFrontList[idx].getElementsByClassName("quiz-start-button")[0].setAttribute("otherQuizID", data[1].quizID);
				else
					quizPreviewFrontList[idx].getElementsByClassName("quiz-start-button")[0].setAttribute("otherQuizID", data[0].quizID);
				
				quizPreviewFrontList[idx].getElementsByClassName("quiz-title")[0].textContent = data[idx].quizTitle;
				quizPreviewFrontList[idx].getElementsByClassName("quiz-short-description")[0].textContent = data[idx].quizQuestionCount + " questions - " + Math.ceil(data[idx].quizTotalTime / 60) + "min duration";
			}
			
			var quizPreviewBackList = document.querySelectorAll(".back .quiz-preview");
			for(var idx = 0; idx < quizPreviewFrontList.length; ++idx)
			{
				if(data[idx].description == null || data[idx].description.lengt == 0)
					quizPreviewBackList[idx].getElementsByClassName("quiz-description")[0].textContent = "This quiz has not description.";
				else
					quizPreviewBackList[idx].getElementsByClassName("quiz-description")[0].textContent = data[idx].description;
				
				quizPreviewBackList[idx].getElementsByClassName("quiz-info")[0].textContent = "Created by " + "Admin" + ", maximum score " + data[idx].quizMaxScore;
			}
			
			getRandomQuizPreviewPairFlag = false;
	 });
}


function startQuiz(startQuizButton)
{
	var thisQuizID = startQuizButton.getAttribute("thisQuizID");
	var otherQuizID = startQuizButton.getAttribute("otherQuizID");
	var URL = null;
	
	console.log("startQuiz(): called, thisQuizID: " + thisQuizID + " otherQuizID: " + otherQuizID);
	
	if(thisQuizID != null && thisQuizID.length != 0) {
		console.log("START QUIZ WITH ID: ", startQuizButton.getAttribute("quizID"));
		URL = "http://localhost:8080/RWA_project/rest/quiz/" + thisQuizID;
		
		if(otherQuizID != null && otherQuizID.length != 0)
			URL += "/" + otherQuizID;
		
		location.replace(URL);
	}
}


function UserQuizCountSession(registerAsQuizPlayer) {
	
	const REGISTER_QUIZ_USER = 1;
	const QUIZ_STAT_MSG = 2;

	
	
	function getMessageTemplate() {
		return { messageType: null, userCount: null };
	}
	
	
	function sendRegisterUserMessage() {
		var registerUserMessage = getMessageTemplate();
		registerUserMessage.messageType = REGISTER_QUIZ_USER;
		webSocket.send(JSON.stringify(registerUserMessage));
	}
	
	
	// INIT WEBSOCKET VARIABLES	
	var webSocket = new WebSocket("ws://localhost:8080/RWA_project/user/stat");

	webSocket.onopen 	= function(message)  { processOpen(message);    };
	webSocket.onmessage = function(message)  { processMessage(message); }
	webSocket.onclose 	= function(message)  { processClose(message);   };
	webSocket.onerror 	= function(message)  { processError(message);   };

	function processOpen(message) {
		if(registerAsQuizPlayer)
			sendRegisterUserMessage();
		
		console.log("Connected to server");
	}
	
	function processMessage(message) {
		var serverMessage = JSON.parse(message.data);
		
		if(serverMessage.messageType == QUIZ_STAT_MSG) {
			updateQuizUserCountCallbackFunction(serverMessage.userCount);
			console.log(message.data);
		}
	}
	
	function processClose(message) {
		webSocket.send("Client disconnected");
		console.log("Client disconnected");
	}

	function processError(message) {
		console.log("Session error");
	}
	
	// QUIZ STAT EVENTS => CALLBACK
	var updateQuizUserCountCallback = null;
	
	
	function updateQuizUserCountCallbackFunction(userCount) {
		if(isFunction(updateQuizUserCountCallback))
			updateQuizUserCountCallback(userCount);
	} 
	
	
	function setUpdateQuizUserCountCallback(updateQuizUserCountCallbackFunc) {
		updateQuizUserCountCallback = updateQuizUserCountCallbackFunc;
	}
	
	this.setUpdateQuizUserCountCallback = setUpdateQuizUserCountCallback;
	
	
}

function updateUserCount(userCount) {
	console.log("User count: ", userCount);
	
	if(userCount != null)
		document.querySelectorAll(".user-count .user-count-text")[0].textContent = "User count: " + userCount;
}


var userQuizCountSession = new UserQuizCountSession(false);
userQuizCountSession.setUpdateQuizUserCountCallback(updateUserCount);