function fetchJSONFile(path, callback) {
	var httpRequest = new XMLHttpRequest();
	
	httpRequest.onreadystatechange = function() {
		if (httpRequest.readyState === 4) {
			if (httpRequest.status === 200) {
				var data = JSON.parse(httpRequest.responseText);
				if (callback) callback(data);
			}
		}
	};
	
	httpRequest.open('GET', path);
	httpRequest.send(); 
} 







function QuizResult() {
	var quizResultTemplate = document.getElementsByClassName("quiz-result-template")[0];
	
	var quizResult = quizResultTemplate.cloneNode(true);
	quizResult.classList.remove("quiz-result-template");
	quizResult.classList.remove("hidden");
	
	var quizTitle = quizResult.getElementsByClassName("quiz-title")[0];
	var quizShortDescription = quizResult.getElementsByClassName("quiz-short-description")[0];
	var leftButtonText = quizResult.querySelectorAll(".quiz-info-button.left .text")[0];
	var rightButtonText = quizResult.querySelectorAll(".quiz-info-button.right .text")[0];
	var questionsBox = quizResult.getElementsByClassName("questions-box")[0];
	
	
	
	function setQuizTitle(quizTitleVal) {
		quizTitle.textContent = quizTitleVal;
	}
	
	
	function setQuizShortDescription(quizShortDescriptionVal) {
		quizShortDescription.textContent = quizShortDescriptionVal;
	}
	
	
	function setLeftButtonText(leftButtonTextVal) {
		leftButtonText.textContent = leftButtonTextVal;
	}

	
	function setRightButtonText(rightButtonTextVal) {
		rightButtonText.textContent = rightButtonTextVal;
	}
	
	
	function addQuestion(question) {
		var questionDOM = question.getDOMElement();
		
		if(questionDOM.classList.contains("quiz-result-question"))
			questionsBox.appendChild(questionDOM);
	}
	
	function getDOMElement() {
		return quizResult;
	}
	
	
	this.setQuizTitle = setQuizTitle;
	this.setQuizShortDescription = setQuizShortDescription;
	this.setLeftButtonText = setLeftButtonText;
	this.setRightButtonText = setRightButtonText;
	this.addQuestion = addQuestion;
	this.getDOMElement = getDOMElement;
}




function QuizResultHorizontalLine() {
	var quizResultHorizontalLineTemplate = document.getElementsByClassName("quiz-result-horizontal-line-template")[0];
	
	var quizResultHorizontalLine = quizResultHorizontalLineTemplate.cloneNode(true);
	quizResultHorizontalLine.classList.remove("quiz-result-horizontal-line-template");
	quizResultHorizontalLine.classList.remove("hidden");
	
	function getDOMElement() {
		return quizResultHorizontalLine;
	}
	
	this.getDOMElement = getDOMElement;
}



function QuizResultQuestion() {
	var quizResultQuestionTemplate = document.getElementsByClassName("quiz-result-question-template")[0];
	var answersCount = 0;
	
	var quizResultQuestion = quizResultQuestionTemplate.cloneNode(true);
	quizResultQuestion.classList.remove("quiz-result-question-template");
	quizResultQuestion.classList.remove("hidden");
	
	
	var questionText = quizResultQuestion.getElementsByClassName("question-text")[0];
	var answersBox = quizResultQuestion.getElementsByClassName("answers-box")[0];
	var questionBox = quizResultQuestion.getElementsByClassName("question-box")[0];	
	
	function setQuestionText(questionTextVal) {
		questionText.textContent = questionTextVal;
	}
	

	function setQuestionCorrect() {
		questionBox.classList.remove("correct");
		questionBox.classList.remove("incorrect");
		questionBox.classList.add("correct");
	}
	
	
	function setQuestionIncorrect() {
		questionBox.classList.remove("correct");
		questionBox.classList.remove("incorrect");
		questionBox.classList.add("incorrect");
	}
	
	
	function addAnswer(answer) {
		var answerDOM = answer.getDOMElement();
		
		if(answerDOM.classList.contains("quiz-result-answer"))
		{
			if(answersCount != 0)
			{
				var horizontalLine = new QuizResultHorizontalLine();
				answersBox.appendChild(horizontalLine.getDOMElement());
			}
			
			answersBox.appendChild(answerDOM);
			++answersCount;
		}
	}
	
	function getDOMElement() {
		return quizResultQuestion;
	}
	
	
	
	this.setQuestionText = setQuestionText;
	this.setQuestionCorrect = setQuestionCorrect;
	this.setQuestionIncorrect = setQuestionIncorrect;
	this.addAnswer = addAnswer;
	this.getDOMElement = getDOMElement;
	
}



function QuizResultAnswer() {
	var quizResultAnswerTemplate = document.getElementsByClassName("quiz-result-answer-template")[0];
	
	var quizResultAnswer = quizResultAnswerTemplate.cloneNode(true);
	quizResultAnswer.classList.remove("quiz-result-answer-template");
	quizResultAnswer.classList.remove("hidden");
	
	var answerText = quizResultAnswer.getElementsByClassName("answer-text")[0];
	
	
	function setAnswerCorrect() {
		quizResultAnswer.classList.add("correct");
	}
	
	function setAnswerIncorrect() {
		quizResultAnswer.classList.remove("correct");
	}
	
	function setAnswerText(answerTextVal) {
		answerText.textContent = answerTextVal;	
	}
	
	function setAnswerSelected() {
		quizResultAnswer.classList.add("selected");
	} 
	
	function removeAnswerSelected() {
		quizResultAnswer.classList.remove("selected");
	}
	
	function getDOMElement() {
		return quizResultAnswer;
	}
	
	this.setAnswerText = setAnswerText;
	this.setAnswerSelected = setAnswerSelected;
	this.removeAnswerSelected = removeAnswerSelected;
	this.setAnswerCorrect = setAnswerCorrect;
	this.setAnswerIncorrect = setAnswerIncorrect;
	this.getDOMElement = getDOMElement;
}











function QuizResultSession(quizResultID) {
	
	var URL = "http://localhost:8080/RWA_project/rest/admin/quizresult/" + quizResultID;
	var displayContainer = document.getElementById("display-container");
	var quizResult = new QuizResult();
	
	fetchJSONFile(URL, function(quizResultData) {
		quizResult.setQuizTitle(quizResultData.quizTitle);
		quizResult.setQuizShortDescription(quizResultData.userName);
		quizResult.setLeftButtonText("Number of question: " + quizResultData.questionCollection.length);
		quizResult.setRightButtonText("Correct answers: " + quizResultData.correctAnswerCount);
		
		var numberOfQuestion = quizResultData.questionCollection.length;
		for(var idx = 0; idx < numberOfQuestion; ++idx) {
			var question = new QuizResultQuestion();
			
			question.setQuestionText(quizResultData.questionCollection[idx].questionText);
			
			if(quizResultData.questionCollection[idx].isAnsweredCorrect)
				question.setQuestionCorrect();
			else
				question.setQuestionIncorrect();
				
			var numberOfAnswer = quizResultData.questionCollection[idx].answerCollection.length;
			for(var idx2 = 0; idx2 < numberOfAnswer; ++idx2) {
				
				var answer = new QuizResultAnswer();
				answer.setAnswerText(quizResultData.questionCollection[idx].answerCollection[idx2].answerText);
				
				if(quizResultData.questionCollection[idx].answerCollection[idx2].isAnswerCorrect)
					answer.setAnswerCorrect();
				else
					answer.setAnswerIncorrect();
				
				if(quizResultData.questionCollection[idx].answerCollection[idx2].isAnswerSelected)
					answer.setAnswerSelected();
				else
					answer.removeAnswerSelected();
				
				
				question.addAnswer(answer);
			}
			
						
			quizResult.addQuestion(question);
		}
		
	});
	
	displayContainer.appendChild(quizResult.getDOMElement());
}



var quizResultSession = new QuizResultSession(quizResultID);

