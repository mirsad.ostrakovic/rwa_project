<!DOCTYPE HTML>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Lato"/>
        <link rel="stylesheet" href="/RWA_project/login_page.css">
    </head>
    
    <body>


        <div class="container">
            <div class="row py-5">
                <div class="col-12"></div>
            </div>

            <div class="row py-5">
                <div class="col-12"></div>
            </div>

            <div class="row py-5">
                <div class="col-12"></div>
            </div>

            <div class="row">
                <div class="col-3"></div>
                <div class="col-6 px-5">
                    <span class="welecome-font">Hello there!</span>
                </div>
                <div class="col-3"></div>
            </div>


            <div class="row">
                <div class="col-3"></div>
                <div class="col-6 px-4">
                    <input class="input-style input-font username" type="text" name="Name" placeholder="Please enter your username">
                </div>
                <div class="col-3"></div>
            </div>


            <div class="row my-n2">
                <div class="col-3"></div>
                <div class="col-6 px-5">
                    <div class="horizontal-line"></div>
                </div>
                <div class="col-3"></div>
            </div>


            <div class="row">
                <div class="col-3"></div>
                <div class="col-6 px-4">
                    <input class="input-style input-font password" type="password" name="Email" placeholder="Please enter your password">
                </div>
                <div class="col-3"></div>
            </div>
        </div>      


        <div class="container-fluid">
            <div class="row py-4">
                <div class="col-4"></div>
                <div class="col-4">
                    <div class="row">
                        <div class="col-4"></div>
                        <div class="col-3 user-login-button d-flex">
                            <span class="mx-auto my-auto">Login</span>
                        </div>
                        <div class="col-5"></div>
                    </div>
                </div>
                <div class="col-4"></div>
            </div>
        </div>

    

            
    </body>

 	<!-- MY JAVASCRIPT-->
    <script type="text/javascript" src="/RWA_project/login_page_communication.js"></script>
    <script type="text/javascript" src="/RWA_project/login_page_animation.js"></script>

</html>
