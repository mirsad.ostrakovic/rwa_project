function fetchJSONFile(path, callback) {
	var httpRequest = new XMLHttpRequest();
	
	httpRequest.onreadystatechange = function() {
		if (httpRequest.readyState === 4) {
			if (httpRequest.status === 200) {
				console.log("fetchJSONFile: ", httpRequest.responseText)
				var data = JSON.parse(httpRequest.responseText);
				if (callback) callback(data);
			}
		}
	};
	
	httpRequest.open('GET', path);
	httpRequest.send(); 
} 



function isFunction(functionToCheck) {
	return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}


function QuizPreview(previewVariant) {
	var quizPreviewTemplate = document.getElementsByClassName("quiz-preview-template")[0];
	var quizPreview = quizPreviewTemplate.cloneNode(true);
	
	quizPreview.classList.remove("quiz-preview-template");
	quizPreview.classList.remove("hidden");
	
	var quizID = null;
	
	var quizPreviewBox = quizPreview.getElementsByClassName("quiz-preview")[0];
	var quizTitle = quizPreview.getElementsByClassName("quiz-title")[0];
	var quizShortDescription = quizPreview.getElementsByClassName("quiz-short-description")[0];
	var quizInfo = quizPreview.getElementsByClassName("quiz-info")[0];
	var	quizDescription	= quizPreview.getElementsByClassName("quiz-description")[0];
	var quizViewButton = quizPreview.getElementsByClassName("quiz-view-button")[0];
	
	
	
	
	
	function viewQuizButtonCallback() {
		window.location.replace("http://localhost:8080/RWA_project/rest/admin/panel/quiz/" + quizID);	
	}
	
	function setQuizID(quizIDVal) {
		quizID = quizIDVal;
	}
	
	function setBackground1() {
		quizPreviewBox.classList.remove("bg-color-2");
		quizPreviewBox.classList.add("bg-color-1");
	}
	
	function setBackground2() {
		quizPreviewBox.classList.remove("bg-color-1");
		quizPreviewBox.classList.add("bg-color-2");
	}
	
	function setQuizTitle(title) {
		quizTitle.textContent = title;
	}
	
	function setQuizShortDescription(shortDescription) {
		quizShortDescription.textContent = shortDescription;
	}

	function setQuizInfo(quizInfoVal) {
		quizInfo.textContent = quizInfoVal;
	}
	
	function setQuizDescription(description) {
		quizDescription.textContent = description;
	}
	
	function getDOMElement() {
		return quizPreview;
	} 
	
	
	
	
	this.setQuizID = setQuizID;
	this.setQuizTitle = setQuizTitle;
	this.setQuizShortDescription = setQuizShortDescription;
	this.setQuizInfo = setQuizInfo;
	this.setQuizDescription = setQuizDescription;
	this.getDOMElement = getDOMElement;
	this.setBackground1 = setBackground1;
	this.setBackground2 = setBackground2;
	
	
	
	quizViewButton.addEventListener("click", viewQuizButtonCallback);
	
	
	
	if(previewVariant === true)
		setBackground1();
	else
		setBackground2();
	
}


function getQuizDuration(questionList) {
	
	var questionTime = 0;
	
	for(var idx = 0; idx < questionList.length; ++idx)
		questionTime += questionList[idx].questionTime;
	
	return questionTime;
}


function generateQuizPreviewList(from, to) 
{	
	
	var quizPreviewContainer = document.getElementsByClassName("quiz-preview-container")[0];
	var URL = "http://localhost:8080/RWA_project/rest/admin/quiz";
	 
	 fetchJSONFile(URL, function(data) {
	
		 var quizzesList = data;
		 
		 while (quizPreviewContainer.firstChild)
			 quizPreviewContainer.removeChild(quizPreviewContainer.firstChild);
		 
		 for(var idx = 0; idx < quizzesList.length; ++idx) {
			 var quizPreview = new QuizPreview(idx % 2 == 1 ? true : false);
			 
			 quizPreview.setQuizID(quizzesList[idx].quizID);
			 quizPreview.setQuizTitle(quizzesList[idx].title);
			 quizPreview.setQuizShortDescription(quizzesList[idx].questionCollection.length + " questions - " + 
					 							 Math.round(getQuizDuration(quizzesList[idx].questionCollection) / 60) + "min duration");
			 
			 
			 console.log(quizzesList[idx]);
			 
			 quizPreviewContainer.appendChild(quizPreview.getDOMElement());
		 }
	 });
}

function redirectToQuizView(quizID) {
	window.location.replace("http://localhost:8080/RWA_project/rest/admin/panel/quiz/" + quizID);	
}

function sendCreateQuizMessage(trueFunc, falseFunc) {
	
	var httpRequest = new XMLHttpRequest();
	
	httpRequest.onreadystatechange = function() {
		if (this.readyState == 4)
		{	
			if(this.status == 200)
			{
				var quizData = JSON.parse(httpRequest.responseText);
				trueFunc(quizData.quizID);
			}
			else
				falseFunc();
		}
	};
	
	httpRequest.open("POST", "/RWA_project/rest/admin/quiz/", true);
	httpRequest.send();
}




generateQuizPreviewList();
