<!DOCTYPE HTML>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Lato"/>
        <link rel="stylesheet" href="/RWA_project/quiz_inbox.css">
    </head>
    
    <body>

       
        <div class="container-fluid">
            <div class="row py-3">
                <div class="col-10"></div>
                <div class="col-2">
                    <div class="row">
                        <div class="col-2"></div>
                        <div class="col-8 download-inbox-button d-flex">
                            <span class="mx-auto my-auto" onclick="downloadInboxButtonCallback();">Download inbox</span>
                        </div>
                        <div class="col-2"></div>
                    </div>
                </div>
            </div>
        </div>


        <div class="container">


            <div class="row py-5">
                <div class="col-12"></div>
            </div>

            <div class="row">
                <div class="col-3"></div>
                <div class="col-6 px-5 d-flex">
                    <span class="mx-auto my-auto quiz-welecome-font quiz-title">JS quiz inbox</span>
                </div>
                <div class="col-3"></div>
            </div>


            <div class="row mt-n3">
                <div class="col-3"></div>
                <div class="col-6 px-5 d-flex">
                    <span class="mx-auto my-auto quiz-welecome-font2 quiz-owner">By Unknown</span>
                </div>
                <div class="col-3"></div>
            </div>

            
        
        	<!-- QUIZ RESULT PREVIEW TEMPLATE => START -->
          	<div class="row py-4 quiz-result-preview-template hidden">
            	<div class="col-3"></div>
                <div class="col-6 px-5">
                  	<div class="row quiz-result-preview bg-color-1">
                       	<div class="col-1"></div>
                        <div class="col-8 my-auto">
                        	<div class="row d-flex">
                            	<div class="col-12 quiz-user-name my-auto">Mirsad Ostrakovic</div>
                                <div class="col-12 quiz-short-description my-auto">18 of 21 questions are correct</div>
                            </div>
                        </div>
                        <div class="col-1"></div>
                        <div class="col-2 d-flex">
                        	<span class="mx-auto my-auto score-box text-center">30</span>
                        </div>
                    </div>
                </div>
                <div class="col-3"></div>
            </div>
            <!-- QUIZ RESULT PREVIEW TEMPLATE => END -->
        

            
            <div class="row py-4">
                <div class="col-12 quiz-result-previews-box">
					<!-- PLACE TO ADD QUIZ RESULTS PREVIEW BOX -->
            	</div>
            </div>

        </div>





    </body>
    
     <script> 
    	var quizID = ${it.quizID}    
    </script>
 
 	<!-- MY JAVASCRIPT-->
 	<script type="text/javascript" src="/RWA_project/FileSaverSrc.js"></script>
 	// <script type="text/javascript" src="/RWA_project/FileSaver.js"></script>
 	<script type="text/javascript" src="/RWA_project/quiz_inbox_animation.js"></script>
    <script type="text/javascript" src="/RWA_project/quiz_inbox_communication.js"></script>
    


</html>
