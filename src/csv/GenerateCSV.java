package csv;

import java.util.*;
import view.*;

public class GenerateCSV {

	
	public static String generateCSVFromQuizResultViews(List<QuizResultView> quizResultViews) { 
		String returnString = "";
		
		for(int idx = 0; idx < quizResultViews.size(); ++idx) 
		{
			QuizResultView quizResultView = quizResultViews.get(idx);
			
			returnString += quizResultView.quizResultID  + ",";
			returnString += quizResultView.quizID  + ",";
			returnString += quizResultView.quizTitle + ",";
			returnString += quizResultView.userEmail + ",";
			returnString += quizResultView.userName  + ",";
			returnString += quizResultView.quizScore  + ",";
			returnString += quizResultView.quizMaxScore  + ",";
			returnString += quizResultView.correctAnswerCount  + ",";
			returnString += quizResultView.questionCollection.size();
			
			returnString += "\r\n";
		}
		
		return returnString;
	}
	
}
