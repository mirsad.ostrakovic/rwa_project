package test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.TestClass;

import model.adminpanel.*;

public class Test1 {

	private static final String PERSISTENCE_UNIT_NAME = "RWA_project";
	private static EntityManagerFactory emf;
	private static String password = "zv=)Av3Z-9x1";
	
	public static void main(String[] args) 
	{
		emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		EntityManager em = emf.createEntityManager();
		
		TestClass test1 = new TestClass();
		
		User mirsadUser = new User("mirso", "mirso1234");
	
		Role admin = new Role("admin");
		admin.getQuizPerm = true;
		admin.addQuizPerm = true;
		admin.editQuizPerm = true;
		admin.deleteQuizPerm = true;
		
		admin.getUserPerm = true;
		admin.editUserPerm = true;
		admin.deleteUserPerm = true;
		admin.addUserPerm = true;
		
		mirsadUser.addRole(admin);
		
		
		test1.testText = "TEST1234";
	
		
		em.getTransaction().begin();
		
		em.persist(test1);
		em.persist(admin);
		em.persist(mirsadUser);
		
		em.getTransaction().commit();


	}

}
