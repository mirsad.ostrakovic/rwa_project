package test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.TestClass;
import model.adminpanel.Role;
import model.adminpanel.User;
import model.quiz.Answer;
import model.quiz.AnsweredQuestion;
import model.quiz.Question;
import model.quiz.Quiz;
import model.quiz.QuizResult;

public class Test3 {

	private static final String PERSISTENCE_UNIT_NAME = "RWA_project";
	private static EntityManagerFactory emf;
	
	public static void main(String[] args) 
	{
		emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		EntityManager em = emf.createEntityManager();
		Quiz JSquiz = null;
		QuizResult quizResult1 = null;
		AnsweredQuestion qa1 =  null;
		
		try {
			JSquiz = new Quiz("JS_quiz3", "JS quiz3");
		}
		catch(Exception ex)
		{
			System.out.println("Error happened");
			return;
		}
		
		Question JSquestion1 = new Question("IS JS interpreted programming language?", 1);
		
		Answer JSquestion1answer1 = new Answer("YES", false);
		Answer JSquestion1answer2 = new Answer("NO", true);
		
	
		JSquestion1.addAnswer(JSquestion1answer1);
		JSquestion1.addAnswer(JSquestion1answer2);
		
		
		JSquiz.addQuestion(JSquestion1);
		
		
	
		
		try 
		{
			quizResult1 = new QuizResult(JSquiz);
			qa1 = new AnsweredQuestion(JSquestion1);
			quizResult1.addQuestion(qa1);
			qa1.addAnswer(JSquestion1answer1);
		}
		catch(Exception ex)
		{
			System.out.println("Error happened");
			return;
		}
		
		
		
	
		
		em.getTransaction().begin();
		
		em.persist(JSquiz);
		em.persist(qa1);
		em.persist(quizResult1);
		
		em.getTransaction().commit();


	}

}
