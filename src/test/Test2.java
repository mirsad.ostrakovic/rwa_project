package test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.TestClass;
import model.adminpanel.Role;
import model.adminpanel.User;
import model.quiz.Answer;
import model.quiz.Question;
import model.quiz.Quiz;

public class Test2 {

	private static final String PERSISTENCE_UNIT_NAME = "RWA_project";
	private static EntityManagerFactory emf;
	
	public static void main(String[] args) 
	{
		emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		EntityManager em = emf.createEntityManager();
		Quiz JSquiz = null;
		
		try {
			JSquiz = new Quiz("JS_quiz", "JS quiz");
		}
		catch(Exception ex)
		{
			System.out.println("Error happened");
			return;
		}
		
		Question JSquestion1 = new Question("IS JS interpreted programming language?", 1);
		
		Answer JSquestion1answer1 = new Answer("YES", false);
		Answer JSquestion1answer2 = new Answer("NO", true);
		
	
		JSquestion1.addAnswer(JSquestion1answer1);
		JSquestion1.addAnswer(JSquestion1answer2);
		
		
		JSquiz.addQuestion(JSquestion1);
		
	
		
		em.getTransaction().begin();
		
		em.persist(JSquiz);
		
		em.getTransaction().commit();


	}

}
