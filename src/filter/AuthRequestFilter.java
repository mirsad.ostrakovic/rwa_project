package filter;

import java.io.*;
import java.net.URI;
import java.security.*;

import javax.annotation.*;
import javax.servlet.http.*;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.*;
import javax.ws.rs.core.*;

import sun.security.acl.*;

@PreMatching
public class AuthRequestFilter implements ContainerRequestFilter {
    @Context
    HttpServletRequest webRequest;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        final HttpSession session = webRequest.getSession(false);

        final URI loginURI = requestContext.getUriInfo().getBaseUriBuilder()
        	      .path("login")
        	      .build();
        
        
        //System.out.println("Login URI: " + loginURI.toString());     
        //System.out.println("Context path: " + webRequest.getContextPath());  
        //System.out.println("Path translated: " + webRequest.getPathTranslated());  
        //System.out.println("Servlet path: " +webRequest.getContextPath());  
        //System.out.println("getContextPath(): " + webRequest.getContextPath());
        //System.out.println("Request URI: " + webRequest.getRequestURI());
        
        if(webRequest.getRequestURI().startsWith("/RWA_project/rest/admin"))
        {
        	boolean loggedIn = session != null && session.getAttribute("user") != null;
        	boolean loginRequest = webRequest.getRequestURI().equals(loginURI.toString());
        
        
        	if (!(loggedIn || loginRequest))
        	{
        		requestContext.abortWith(Response.seeOther(loginURI).build());
   
        		System.out.println("Before redirect");
        		requestContext.abortWith(Response.temporaryRedirect(loginURI).build());
        		System.out.println("After redirect");
        	}
        }
        
        
    }
}