package init;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.adminpanel.Role;
import model.adminpanel.User;
import model.quiz.Answer;
import model.quiz.AnsweredQuestion;
import model.quiz.Question;
import model.quiz.Quiz;
import model.quiz.QuizResult;

public class InitServletQuiz {

	private static final String PERSISTENCE_UNIT_NAME = "RWA_project";
	private static EntityManagerFactory emf;
	
	public static void main(String[] args) 
	{
		emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		EntityManager em = emf.createEntityManager();
		Quiz servletQuiz = null;
		
		
		// ADMIN user add		
		User maidUser = new User("maid", "maid1234");
			
		Role editor = new Role("editor");
		editor.getQuizPerm = true;
		editor.addQuizPerm = true;
		editor.editQuizPerm = true;
		editor.deleteQuizPerm = true;
				
		editor.getUserPerm = false;
		editor.editUserPerm = false;
		editor.deleteUserPerm = false;
		editor.addUserPerm = false;
				
		maidUser.addRole(editor);
				
			
				
		em.getTransaction().begin();
				
		em.persist(editor);
		em.persist(maidUser);
				
		em.getTransaction().commit();

		
		
		
		
		
		
		try {
			servletQuiz = new Quiz("Servlet_quiz", "Servlet quiz");
		}
		catch(Exception ex)
		{
			System.out.println("Error happened");
			return;
		}
		
		servletQuiz.setQuizDescription("This is the quiz about basic knowledge of Java Servlet technology.");
		servletQuiz.setQuizOwner(maidUser);
		
		Question serlvetQuestion1 = new Question("Which methods are used to bind the objects on HttpSession instance and get the objects? ", 2);
		
		Answer serlvetQuestion1Answer1 = new Answer("setAttribute", true);
		Answer serlvetQuestion1Answer2 = new Answer("getAttribute", true);
		Answer serlvetQuestion1Answer3 = new Answer("None of the above", false);
		
		serlvetQuestion1.addAnswer(serlvetQuestion1Answer1);
		serlvetQuestion1.addAnswer(serlvetQuestion1Answer2);
		serlvetQuestion1.addAnswer(serlvetQuestion1Answer3);
		
		servletQuiz.addQuestion(serlvetQuestion1);
		
		
		
		
		Question serlvetQuestion2 = new Question("The sendRedirect() method of HttpServletResponse interface can be used to redirect response to another resource, it may be servlet, jsp or html file.", 1);
		
		Answer serlvetQuestion2Answer1 = new Answer("true", true);
		Answer serlvetQuestion2Answer2 = new Answer("false>", false);
		
		serlvetQuestion2.addAnswer(serlvetQuestion2Answer1);
		serlvetQuestion2.addAnswer(serlvetQuestion2Answer2);
		
		
		servletQuiz.addQuestion(serlvetQuestion2);	
		
		
		
		
		 
		 
		 
		 Question serlvetQuestion3 = new Question("Servlets handle multiple simultaneous requests by using threads.", 1);
			
		 Answer serlvetQuestion3Answer1 = new Answer("true", true);
		 Answer serlvetQuestion3Answer2 = new Answer("false>", false);
			
		 serlvetQuestion3.addAnswer(serlvetQuestion3Answer1);
		 serlvetQuestion3.addAnswer(serlvetQuestion3Answer2);
			
			
		 servletQuiz.addQuestion(serlvetQuestion3);	
		
		 
		 
		 
		 
		 Question serlvetQuestion4 = new Question("Which method is used to send the same request and response objects to another servlet in RequestDispacher ?", 2);
			
		 Answer serlvetQuestion4Answer1 = new Answer("forward()", true);
		 Answer serlvetQuestion4Answer2 = new Answer("sendRedirect()", false);
		 Answer serlvetQuestion4Answer3 = new Answer("Both A & B", false);
		 Answer serlvetQuestion4Answer4 = new Answer("None of the above", false);
			
		 serlvetQuestion4.addAnswer(serlvetQuestion4Answer1);
		 serlvetQuestion4.addAnswer(serlvetQuestion4Answer2);
		 serlvetQuestion4.addAnswer(serlvetQuestion4Answer3);
		 serlvetQuestion4.addAnswer(serlvetQuestion4Answer4);
			
		 servletQuiz.addQuestion(serlvetQuestion4);
		
		 
		
				 
				 
		 Question serlvetQuestion5 = new Question("What is the lifecycle of a servlet?", 2);
			
		 Answer serlvetQuestion5Answer1 = new Answer("Servlet class is loaded", true);
		 Answer serlvetQuestion5Answer2 = new Answer("Servlet instance is created", true);
		 Answer serlvetQuestion5Answer3 = new Answer("init,Service,destroy method is invoked", true);
		 Answer serlvetQuestion5Answer4 = new Answer("None of the above", false);
			
		 serlvetQuestion5.addAnswer(serlvetQuestion5Answer1);
		 serlvetQuestion5.addAnswer(serlvetQuestion5Answer2);
		 serlvetQuestion5.addAnswer(serlvetQuestion5Answer3);
		 serlvetQuestion5.addAnswer(serlvetQuestion5Answer4);
			
		 servletQuiz.addQuestion(serlvetQuestion5);
		 
		 
		
		
	
		
		em.getTransaction().begin();
		em.persist(servletQuiz);
		em.getTransaction().commit();
		


	}

}