package init;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.TestClass;
import model.adminpanel.Role;
import model.adminpanel.User;
import model.quiz.Answer;
import model.quiz.AnsweredQuestion;
import model.quiz.Question;
import model.quiz.Quiz;
import model.quiz.QuizResult;

public class InitJSQuiz {

	private static final String PERSISTENCE_UNIT_NAME = "RWA_project";
	private static EntityManagerFactory emf;
	
	public static void main(String[] args) 
	{
		emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		EntityManager em = emf.createEntityManager();
		
		
		
		
		// ADMIN user add		
		User mirsadUser = new User("mirso", "mirso1234");
	
		Role admin = new Role("admin");
		admin.getQuizPerm = true;
		admin.addQuizPerm = true;
		admin.editQuizPerm = true;
		admin.deleteQuizPerm = true;
		
		admin.getUserPerm = true;
		admin.editUserPerm = true;
		admin.deleteUserPerm = true;
		admin.addUserPerm = true;
		
		mirsadUser.addRole(admin);
		
	
		
		em.getTransaction().begin();
		
		em.persist(admin);
		em.persist(mirsadUser);
		
		em.getTransaction().commit();

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		// JSquiz ADD
		
		Quiz JSquiz = null;
		QuizResult quizResult1 = null;
	
		
		try {
			JSquiz = new Quiz("JS_quiz", "JavaScript quiz");
		}
		catch(Exception ex)
		{
			System.out.println("Error happened");
			return;
		}
		
		JSquiz.setQuizOwner(mirsadUser);
		
		JSquiz.setQuizDescription("This is the quiz about basic knowledge of JavaScript programming language.");
		
		
		Question JSquestion1 = new Question("Inside which HTML element do we put the JavaScript?", 1);
		
		Answer JSquestion1answer1 = new Answer("<scripting>", false);
		Answer JSquestion1answer2 = new Answer("<js>", false);
		Answer JSquestion1answer3 = new Answer("<javascript>", false);
		Answer JSquestion1answer4 = new Answer("<script>", true);
		
		JSquestion1.addAnswer(JSquestion1answer1);
		JSquestion1.addAnswer(JSquestion1answer2);
		JSquestion1.addAnswer(JSquestion1answer3);
		JSquestion1.addAnswer(JSquestion1answer4);
		
		JSquiz.addQuestion(JSquestion1);
		
		
		
		
		
		
		Question JSquestion2 = new Question("What is the correct JavaScript syntax to change the content of the HTML element below?\n\n" + 
											"<p id=\"demo\">This is a demonstration.</p>", 2);
		
		Answer JSquestion2answer1 = new Answer("#demo.innerHTML = \"Hello world!\";", false);
		Answer JSquestion2answer2 = new Answer("document.getElementById(\"demo\").innerHTML = \"Hello world!\";", true);
		Answer JSquestion2answer3 = new Answer("document.getElement(\"p\").innerHTML = \"Hello world!\";", false);
		Answer JSquestion2answer4 = new Answer("document.getElementByName(\"p\").innerHTML = \"Hello world!\";", false);
		
		JSquestion2.addAnswer(JSquestion2answer1);
		JSquestion2.addAnswer(JSquestion2answer2);
		JSquestion2.addAnswer(JSquestion2answer3);
		JSquestion2.addAnswer(JSquestion2answer4);
		
		JSquiz.addQuestion(JSquestion2);
		
		
		
		
		
		
		Question JSquestion3 = new Question("Where is the correct place to insert a JavaScript?", 1);
		
		Answer JSquestion3answer1 = new Answer("The <body> section", false);
		Answer JSquestion3answer2 = new Answer("The <head> section", false);
		Answer JSquestion3answer3 = new Answer("Both the <head> and the <body> section are correct", true);
		
		JSquestion3.addAnswer(JSquestion3answer1);
		JSquestion3.addAnswer(JSquestion3answer2);
		JSquestion3.addAnswer(JSquestion3answer3);
		
		JSquiz.addQuestion(JSquestion3);
		
		
		
		
		
		
		Question JSquestion4 = new Question("What is the correct syntax for referring to an external script called \"xxx.js\"?", 1);
		
		Answer JSquestion4answer1 = new Answer("<script href=\"xxx.js\">", false);
		Answer JSquestion4answer2 = new Answer("<script name=\"xxx.js\">", false);
		Answer JSquestion4answer3 = new Answer("<script src=\"xxx.js\">", true);
		
		JSquestion4.addAnswer(JSquestion4answer1);
		JSquestion4.addAnswer(JSquestion4answer2);
		JSquestion4.addAnswer(JSquestion4answer3);
		
		JSquiz.addQuestion(JSquestion4);
		
		
		
		
		
		Question JSquestion5 = new Question("How can you add a comment in a JavaScript?", 2);
		
		Answer JSquestion5answer1 = new Answer("//This is a comment", true);
		Answer JSquestion5answer2 = new Answer("<!--This is a comment-->", false);
		Answer JSquestion5answer3 = new Answer("/*This is a comment*/", true);
		Answer JSquestion5answer4 = new Answer("//This is a comment//", false);
		
		JSquestion5.addAnswer(JSquestion5answer1);
		JSquestion5.addAnswer(JSquestion5answer2);
		JSquestion5.addAnswer(JSquestion5answer3);
		JSquestion5.addAnswer(JSquestion5answer4);
		
		JSquiz.addQuestion(JSquestion5);
		
		
		em.getTransaction().begin();
		em.persist(JSquiz);
		em.getTransaction().commit();
		
		if(JSquiz.getQuizID() == null)
			System.out.println("Quiz result ID is null");
		else
			System.out.println("Quiz result ID: " + Long.toString(JSquiz.getQuizID()));
		
		
		
		
		
		
		
		AnsweredQuestion answeredQuestion1 = null;
		AnsweredQuestion answeredQuestion2 = null;
		AnsweredQuestion answeredQuestion4 = null;
		AnsweredQuestion answeredQuestion5 = null;
		
		try 
		{
			
			quizResult1 = new QuizResult(JSquiz);
			
			answeredQuestion1 = new AnsweredQuestion(JSquestion1);
			answeredQuestion2 = new AnsweredQuestion(JSquestion2);
			answeredQuestion4 = new AnsweredQuestion(JSquestion4);
			answeredQuestion5 = new AnsweredQuestion(JSquestion5);
			
			System.out.println("Create AnseredQuestion compleated");
			
		}
		catch(Exception ex)
		{
			System.out.println("Exception: " + ex.toString());
			System.out.println("Error happened");
			return;
		}
		
		System.out.println("QuizResult.quiz.quizID: " + Long.toString(quizResult1.getQuiz().getQuizID()));
		
		
		quizResult1.addQuestion(answeredQuestion1);
		quizResult1.addQuestion(answeredQuestion2);
		quizResult1.addQuestion(answeredQuestion4);
		quizResult1.addQuestion(answeredQuestion5);
			
		System.out.println("QuizResult.addQuestion() compleated");
			
		
		
		answeredQuestion1.addAnswer(JSquestion1answer1);
		System.out.println("AnseredQuestion.addAnswer 1() compleated");
		answeredQuestion2.addAnswer(JSquestion2answer1);
		System.out.println("AnseredQuestion.addAnswer 2() compleated");
		answeredQuestion5.addAnswer(JSquestion5answer3);
		System.out.println("AnseredQuestion.addAnswer 5.1() compleated");
		answeredQuestion5.addAnswer(JSquestion5answer1);
			
		System.out.println("AnseredQuestion.addAnswer() compleated");
		
		
		
		
		
	
		
		em.getTransaction().begin();
		em.persist(JSquiz);
		em.getTransaction().commit();


	}

}