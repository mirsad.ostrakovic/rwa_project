package init;

import java.util.Set;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.*;
import org.glassfish.jersey.server.mvc.*;
import org.glassfish.jersey.server.mvc.jsp.*;
import com.google.common.base.*;

@javax.ws.rs.ApplicationPath("/rest")
public class JerseyApplication extends ResourceConfig {
    
	public JerseyApplication() 
    {
		System.out.println("Init Jersey(JAX-RS)");
		 
		packages("web.resources");
		packages("com.google.common.base");
		
		//packages("org.glassfish.jersey.examples.multipart");
		//register(MultiPartFeature.class);
		
	    property(JspMvcFeature.TEMPLATE_BASE_PATH, "/");
		register(org.glassfish.jersey.server.mvc.jsp.JspMvcFeature.class);
		
		
		register(org.glassfish.jersey.media.multipart.MultiPartFeature.class);
		
	
		register(filter.AuthRequestFilter.class);
		
		 //property("jersey.config.server.provider.classnames",
	     //         "org.glassfish.jersey.server.mvc.jsp.JspMvcFeature");
		
    }
	
}