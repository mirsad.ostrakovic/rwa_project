package view;

import com.google.gson.annotations.Expose;

public class UserCountSessionMessage {
	@Expose
	public Integer messageType;
	
	@Expose
	public Integer userCount = null;
	
	
	
	public UserCountSessionMessage(int messageType, int userCount)
	{
		this.messageType = messageType;
		this.userCount = userCount;
	}
}
