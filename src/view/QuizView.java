package view;

import java.util.*;
import com.google.gson.annotations.*;
import model.quiz.*;

public class QuizView {
	@Expose
	public Long quizID;
	
	@Expose
	public String quizTitle;
	
	@Expose
	public String description;
	
	@Expose
	boolean hasQuizImage;
	
	@Expose
	public Integer quizQuestionCount;
	
	@Expose
	public Integer quizMaxScore;
	
	@Expose
	public Integer quizTotalTime;
	

	
	public QuizView(Quiz quiz)
	{
		this.quizID = quiz.getQuizID();
		
		this.quizTitle = quiz.getQuizTitle();
		
		this.description = quiz.getQuizDescription();
		
		this.hasQuizImage = quiz.hasQuizImage();
		
		this.quizMaxScore = 0;
		
		this.quizTotalTime = 0;
		
		this.quizQuestionCount = quiz.getQuestionCollection().size();
		
		for(Question question : quiz.getQuestionCollection())
		{
			quizMaxScore += question.getQuestionScore();
			quizTotalTime += question.getQuestionTime();
		}
	}

}
