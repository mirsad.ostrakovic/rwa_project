package view;

import java.util.ArrayList;
import java.util.Collection;

import com.google.gson.annotations.*;
import model.quiz.*;

public class AnswerView {

	@Expose
	public Long answerID;
	
	
	@Expose
	public String answerText;
	
	
	
	public AnswerView(Answer answer)
	{
		this.answerID = answer.getAnswerID();
		this.answerText = answer.getAnswerText();
	}
	
}
