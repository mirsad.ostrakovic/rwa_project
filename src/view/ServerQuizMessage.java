package view;

import java.util.ArrayList;
import java.util.Collection;

import com.google.gson.annotations.*;
import model.quiz.*;

public class ServerQuizMessage {
	
	@Expose
	public Long messageType;
	
	@Expose
	public QuestionView question;
	
	@Expose
	public Long quizResultID = null;
	
	
	
	public ServerQuizMessage(long messageType, QuestionView question)
	{
		this.messageType = messageType;
		this.question = question;
	}
	
}

