package view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.gson.annotations.*;
import model.quiz.*;

public class ClientQuizMessage {

	@Expose
	public Long messageType;
	
	@Expose
	public List<Long> answerIDs = new ArrayList<Long>();
	
	@Expose
	public String email;
	
	@Expose
	public String name;
	
	public ClientQuizMessage(long messageType) 
	{
		this.messageType = messageType;
	}
}
