package view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.gson.annotations.*;

import model.quiz.*;

public class AnsweredQuestionView {
	
	@Expose
	public Long questionID;

	@Expose
	public String questionText;
	
	@Expose
	public Integer questionScore;
	
	@Expose
	public Boolean isAnsweredCorrect;
	
	@Expose
	public List<AnsweredQuestionAnswerView> answerCollection = new ArrayList<AnsweredQuestionAnswerView>();
	
	
	
	public AnsweredQuestionView(AnsweredQuestion answeredQuestion) {
		
		Boolean isAnsweredCorrect = true;
		
		Question question = answeredQuestion.getQuestion();
		
		this.questionID = question.getQuestionID();
		
		this.questionText = question.getQuestionText();
		
		this.questionScore = question.getQuestionScore();
		
		//System.out.println("Question: " + questionText);
		
		for(Answer answer : question.getAnswerCollection())
		{
			//System.out.println("Answer: " + answer.getAnswerText());
			Boolean isAnswerSelected = answeredQuestion.getSelectedAnswers().contains(answer);
			
			if( (isAnswerSelected  && !answer.isAnswerCorrect()) || 
				(!isAnswerSelected && answer.isAnswerCorrect()) 
			  )
				isAnsweredCorrect = false;
			
			answerCollection.add(new AnsweredQuestionAnswerView(answer, isAnswerSelected));
		}
		
		this.isAnsweredCorrect = isAnsweredCorrect;
	}
}
