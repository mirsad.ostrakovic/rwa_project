package view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.gson.annotations.*;
import model.quiz.*;

public class QuestionView {
	
	@Expose
	public Long questionID;
	
	
	@Expose
	public String questionText;
	
	
	@Expose
	public long totalTime;
	
	
	@Expose
	public long remainTime;
	
	
	@Expose
	public int questionScore;
	
	@Expose 
	int questionOrdinalNumber;
	
	
	
    @Expose
	public List<AnswerView> answerCollection = new ArrayList<AnswerView>();
    
    
    public QuestionView(Question question, long remainTime, int questionOrdinalNumber)
    {
    	this.questionID = question.getQuestionID();
    	
    	this.questionText = question.getQuestionText();
    	
    	this.totalTime = 1000 * question.getQuestionTime();
    	
    	this.remainTime = remainTime;
    	
    	this.questionOrdinalNumber = questionOrdinalNumber;
    	
    	this.questionScore = question.getQuestionScore();
    
    	for(Answer answer : question.getAnswerCollection())
    	{
    		answerCollection.add(new AnswerView(answer));
    	}
    	
    }

}
