package view;


import com.google.gson.annotations.*;
import model.quiz.*;

public class AnsweredQuestionAnswerView {
	
	@Expose
	public Long answerID;
	
	@Expose
	public String answerText;
	
	@Expose
	public Boolean isAnswerCorrect;
	
	@Expose
	public Boolean isAnswerSelected;
	
	
	public AnsweredQuestionAnswerView(Answer answer, Boolean isAnswerSelected)
	{
		this.answerID = answer.getAnswerID();
		this.answerText = answer.getAnswerText();
		this.isAnswerCorrect = answer.isAnswerCorrect();
		this.isAnswerSelected = isAnswerSelected;
	}
}
