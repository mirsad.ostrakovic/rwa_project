package view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.gson.annotations.*;
import model.quiz.*;

public class QuizResultView {
	
	@Expose
	public Long quizResultID;
	
	@Expose
	public Long quizID;
	
	@Expose
	public String quizTitle;
	
	@Expose
	public Integer quizMaxScore;
	
	@Expose
	public Integer quizScore;
	
	@Expose
	public Integer correctAnswerCount;
	
	@Expose
	public String userName;
	
	@Expose
	public String userEmail;
	
	@Expose
	public List<AnsweredQuestionView> questionCollection = new ArrayList<AnsweredQuestionView>();
	
	
	
	
	
	
	public QuizResultView(QuizResult quizResult) 
	{
		Quiz quiz = quizResult.getQuiz();
	
		
		
		this.userName = quizResult.getUserName();
		
		this.userEmail = quizResult.getUserEmail();
		
		
		
		this.quizMaxScore = 0;
		
		this.quizScore = 0;
		
		this.correctAnswerCount = 0;
		
		this.quizResultID = quizResult.getQuizResultID();
		
		this.quizID = quiz.getQuizID();
		
		this.quizTitle = quiz.getQuizTitle();
		
	
		
		for(AnsweredQuestion answeredQuestion : quizResult.getAnsweredQuestionCollection())
		{
			AnsweredQuestionView newQuestion = new AnsweredQuestionView(answeredQuestion);
			
			quizMaxScore += newQuestion.questionScore;
			
			if(newQuestion.isAnsweredCorrect)
			{
				++correctAnswerCount;
				quizScore += newQuestion.questionScore;
			}
			
			questionCollection.add(newQuestion);
		}
		
	}

}
