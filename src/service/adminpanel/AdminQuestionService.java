package service.adminpanel;

import java.util.*;
import javax.persistence.*;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.quiz.*;
import service.jpa.*;

public class AdminQuestionService {

	
	public static Question createNewQuestion(Long quizID)
	{
		if(quizID == null)
			return null;
		
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {
			TypedQuery<Quiz> getQuizQuery = em.createNamedQuery("Quiz.getQuizByID", Quiz.class);
			
			getQuizQuery.setParameter("quizID", quizID);
			List<Quiz> quizList = getQuizQuery.getResultList();
			
			if(quizList == null || quizList.size() == 0)
				return null;
			
			Question newQuestion = new Question(null, 0);
			quizList.get(0).addQuestion(newQuestion);
			
			
			em.getTransaction().begin();
			
			em.persist(newQuestion);
			em.persist(quizList.get(0));
			
			em.getTransaction().commit();
			
			return newQuestion;
			
		}
		catch(Exception e)
		{	
			return null;
		}
	}
	
	
	
	
	public static Question createNewQuestion(String quizShareTag)
	{
		if(quizShareTag == null)
			return null;
		
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {
			TypedQuery<Quiz> getQuizQuery = em.createNamedQuery("Quiz.getQuizByShareTag", Quiz.class);
			
			getQuizQuery.setParameter("quizShareTag", quizShareTag);
			List<Quiz> quizList = getQuizQuery.getResultList();
			
			if(quizList == null || quizList.size() == 0)
				return null;
			
			Question newQuestion = new Question(null, 0);
			quizList.get(0).addQuestion(newQuestion);
			
			Answer trueAnswer = new Answer("Generic answer text", true);
			Answer falseAnswer = new Answer("Generic answer text", false);
			
			newQuestion.addAnswer(trueAnswer);
			newQuestion.addAnswer(falseAnswer);
			
			em.getTransaction().begin();
			
			// em.persist(trueAnswer);
			// em.persist(falseAnswer);
			em.persist(newQuestion);
			em.persist(quizList.get(0));
			
			em.getTransaction().commit();
			
			return newQuestion;
			
		}
		catch(Exception e)
		{	
			return null;
		}
	}
	
	
	public static Question getQuestion(Long questionID)
	{
		if(questionID == null)
			return null;
		
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {
			TypedQuery<Question> getQuestionQuery = em.createNamedQuery("Question.getQuestionByID", Question.class);
			
			getQuestionQuery.setParameter("questionID", questionID);
			List<Question> questionList = getQuestionQuery.getResultList();
			
			if(questionList == null || questionList.size() == 0)
				return null;
			else
				return questionList.get(0);
			
		}
		catch(Exception e)
		{	
			return null;
		}
	}
	
	
	
	
	
	public static boolean editQuestionText(Long questionID, String questionText)
	{
		if(questionID == null || questionText == null || questionText.length() == 0)
			return false;
		
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {
			TypedQuery<Question> getQuestionQuery = em.createNamedQuery("Question.getQuestionByID", Question.class);
			
			getQuestionQuery.setParameter("questionID", questionID);
			List<Question> questionList = getQuestionQuery.getResultList();
			
			if(questionList == null || questionList.size() == 0)
				return false;
			
			questionList.get(0).setQuestionText(questionText);
			
			em.getTransaction().begin();
			em.persist(questionList.get(0));
			em.getTransaction().commit();
			
			return true;
			
		}
		catch(Exception e)
		{	
			return false;
		}
	}
	
	
	
	
	
	
	public static boolean setQuestionPriority(Long questionID, Integer questionPriority)
	{
		if(questionID == null || questionPriority == null)
			return false;
		
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {
			TypedQuery<Question> getQuestionQuery = em.createNamedQuery("Question.getQuestionByID", Question.class);
			
			getQuestionQuery.setParameter("questionID", questionID);
			List<Question> questionList = getQuestionQuery.getResultList();
			
			if(questionList == null || questionList.size() == 0)
				return false;
			
			questionList.get(0).setQuestionPriority(questionPriority);
			
			em.getTransaction().begin();
			em.persist(questionList.get(0));
			em.getTransaction().commit();
			
			return true;
			
		}
		catch(Exception e)
		{	
			return false;
		}
	}
	
	
	
	
	public static boolean setQuestionTime(Long questionID, Integer questionTime)
	{
		if(questionID == null || questionTime == null)
			return false;
		
		// Test for question time constraints
		if(questionTime < 10 || questionTime > 60)
			return false;
		
		
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {
			TypedQuery<Question> getQuestionQuery = em.createNamedQuery("Question.getQuestionByID", Question.class);
			
			getQuestionQuery.setParameter("questionID", questionID);
			List<Question> questionList = getQuestionQuery.getResultList();
			
			if(questionList == null || questionList.size() == 0)
				return false;
			
			questionList.get(0).setQuestionTime(questionTime);
			
			em.getTransaction().begin();
			em.persist(questionList.get(0));
			em.getTransaction().commit();
			
			return true;
			
		}
		catch(Exception e)
		{	
			return false;
		}
	}
	
	
	
	
	public static boolean setQuestionScore(Long questionID, Integer questionScore)
	{
		if(questionID == null || questionScore == null)
			return false;
		
		// Test for question score constraints
		if(questionScore < 1 || questionScore > 20)
			return false;
		
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {
			TypedQuery<Question> getQuestionQuery = em.createNamedQuery("Question.getQuestionByID", Question.class);
			
			getQuestionQuery.setParameter("questionID", questionID);
			List<Question> questionList = getQuestionQuery.getResultList();
			
			if(questionList == null || questionList.size() == 0)
				return false;
			
			questionList.get(0).setQuestionScore(questionScore);
			
			em.getTransaction().begin();
			em.persist(questionList.get(0));
			em.getTransaction().commit();
			
			return true;
			
		}
		catch(Exception e)
		{	
			return false;
		}
	}
	
	
	
	
	public static boolean deleteQuestion(Long questionID)
	{
		if(questionID == null)
			return false;
		
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {
			TypedQuery<Question> getQuestionQuery = em.createNamedQuery("Question.getQuestionByID", Question.class);
			
			getQuestionQuery.setParameter("questionID", questionID);
			List<Question> questionList = getQuestionQuery.getResultList();
			
			if(questionList == null || questionList.size() == 0)
				return false;
			
			Quiz quiz = questionList.get(0).getQuiz();
			quiz.removeQuestion(questionList.get(0));
			
			em.getTransaction().begin();
			em.persist(quiz);
			em.remove(questionList.get(0));
			em.getTransaction().commit();
			
			return true;
			
		}
		catch(Exception e)
		{	
			return false;
		}
	}
	
	
	
}

