package service.adminpanel;

import java.util.*;
import javax.persistence.*;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.quiz.*;
import service.jpa.*;


public class AdminAnswerService {
	
	
	public static Answer createNewAnswer(Long questionID)
	{
		if(questionID == null)
			return null;
		
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {
			TypedQuery<Question> getQuestionQuery = em.createNamedQuery("Question.getQuestionByID", Question.class);
				
			getQuestionQuery.setParameter("questionID", questionID);
			List<Question> questionList = getQuestionQuery.getResultList();
				
			
			if(questionList == null || questionList.size() == 0)
				return null;
	
			// Test for maximum question constraint
			if(questionList.get(0).getAnswerCollection().size() >= 5)
				return null;
			
			Answer newAnswer = new Answer(null, false);
			questionList.get(0).addAnswer(newAnswer);
			
			
			em.getTransaction().begin();
			
			em.persist(newAnswer);
			em.persist(questionList.get(0));
			
			em.getTransaction().commit();
			
			return newAnswer;
			
		}
		catch(Exception e)
		{	
			return null;
		}
	}
	
	
	
	
	
	
	public static Answer getAnswer(Long answerID)
	{
		if(answerID == null)
			return null;
		
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {
			TypedQuery<Answer> getAnswerQuery = em.createNamedQuery("Answer.getAnswerByID", Answer.class);
			
			getAnswerQuery.setParameter("answerID", answerID);
			List<Answer> answerList = getAnswerQuery.getResultList();
			
			if(answerList == null || answerList.size() == 0)
				return null;
			else
				return answerList.get(0);
			
		}
		catch(Exception e)
		{	
			return null;
		}
	}

	
	
	
	
	
	public static boolean editAnswerText(Long answerID, String answerText)
	{
		if(answerID == null || answerText == null || answerText.length() == 0)
			return false;
		
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {
			TypedQuery<Answer> getAnswerQuery = em.createNamedQuery("Answer.getAnswerByID", Answer.class);
			
			getAnswerQuery.setParameter("answerID", answerID);
			List<Answer> answerList = getAnswerQuery.getResultList();
			
			if(answerList == null || answerList.size() == 0)
				return false;
			
			answerList.get(0).setAnswerText(answerText);
			
			em.getTransaction().begin();
			em.persist(answerList.get(0));
			em.getTransaction().commit();
			
			return true;
			
		}
		catch(Exception e)
		{	
			return false;
		}
	}
	
	
	
	
	
	public static boolean setAnswerCorrect(Long answerID)
	{
		if(answerID == null)
			return false;
		
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {
			TypedQuery<Answer> getAnswerQuery = em.createNamedQuery("Answer.getAnswerByID", Answer.class);
			
			getAnswerQuery.setParameter("answerID", answerID);
			List<Answer> answerList = getAnswerQuery.getResultList();
			
			if(answerList == null || answerList.size() == 0)
				return false;
			
			answerList.get(0).setAnswerCorrect();
			
			em.getTransaction().begin();
			em.persist(answerList.get(0));
			em.getTransaction().commit();
			
			return true;
			
		}
		catch(Exception e)
		{	
			return false;
		}
	}
	
	
	
	
	
	public static boolean setAnswerIncorrect(Long answerID)
	{
		if(answerID == null)
			return false;
		
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {
			TypedQuery<Answer> getAnswerQuery = em.createNamedQuery("Answer.getAnswerByID", Answer.class);
			
			getAnswerQuery.setParameter("answerID", answerID);
			List<Answer> answerList = getAnswerQuery.getResultList();
			
			if(answerList == null || answerList.size() == 0)
				return false;
			
			Answer answer = answerList.get(0);
			
			// Test for minimum correct answers of question constraint
			
			int correctAnswersCount = 0;
			
			List<Answer> answersList = answer.getQuestion().getAnswerCollection();
			for(int idx = 0; idx < answersList.size(); ++idx)
				if(answersList.get(idx).isAnswerCorrect())
					++correctAnswersCount;
			
			if(correctAnswersCount <= 1)
				return false;
			
			answer.setAnswerIncorrect();
			
			em.getTransaction().begin();
			em.persist(answerList.get(0));
			em.getTransaction().commit();
			
			return true;
			
		}
		catch(Exception e)
		{	
			return false;
		}
	}
	
	
	
	public static boolean deleteAnswer(Long answerID)
	{
		if(answerID == null)
			return false;
		
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {
			TypedQuery<Answer> getAnswerQuery = em.createNamedQuery("Answer.getAnswerByID", Answer.class);
			
			getAnswerQuery.setParameter("answerID", answerID);
			List<Answer> answerList = getAnswerQuery.getResultList();
			
			if(answerList == null || answerList.size() == 0)
				return true;
			
			Question question = answerList.get(0).getQuestion();
			
			// Test for minimum question constraint
			if(question.getAnswerCollection().size() <= 2)
				return false;
			
			question.removeAnswer(answerList.get(0));
			
			
			em.getTransaction().begin();
			em.persist(question);
			em.remove(answerList.get(0));
			em.getTransaction().commit();
			
			return true;
			
		}
		catch(Exception e)
		{	
			return false;
		}
	}
	
	
	
}
