package service.adminpanel;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import model.quiz.*;
import view.*;
import service.jpa.PersistenceContextHandler;

public class AdminQuizResultService {
	
	
	public static List<QuizResultView> getQuizResults(String quizID, Integer from, Integer to)
	{	
		
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		List<QuizResultView> returnList = new ArrayList<QuizResultView>();
		
		if(quizID == null || quizID.length() == 0 || from == null || to == null || from < 0 || to < 0 || from > to)
			return null;
	
		try {	
			TypedQuery<QuizResult> getQuizResultsQuery = em.createNamedQuery("QuizResult.getQuizResultByQuizID", QuizResult.class);
			getQuizResultsQuery.setParameter("quizID", Long.parseLong(quizID));
			getQuizResultsQuery.setMaxResults(to - from + 1);
			getQuizResultsQuery.setFirstResult(from);
			System.out.println("Before getResultList()");
			List<QuizResult> quizResultsList = getQuizResultsQuery.getResultList();
			System.out.println("After getResultList()");
			
			if(quizResultsList != null)
				for(QuizResult elem : quizResultsList)
					returnList.add(new QuizResultView(elem));
			
			return returnList;	
		}
		catch(Exception e)
		{	
			System.out.println("getQuizResults(): " + e.toString());
			e.printStackTrace();
			return null;
		}
		
	}
	
	
	
	
	
	public static QuizResultView getQuizResultViewByID(String quizResultID)
	{
		if(quizResultID == null)
			return null;
		
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {	
			TypedQuery<QuizResult> getQuizResultQuery = em.createNamedQuery("QuizResult.getQuizResultByID", QuizResult.class);
			
			getQuizResultQuery.setParameter("quizResultID", Long.parseLong(quizResultID));
			List<QuizResult> quizResultList = getQuizResultQuery.getResultList();
			
			if(quizResultList == null || quizResultList.size() == 0)
				return null;
			else
				return new QuizResultView(quizResultList.get(0));	
		}
		catch(Exception e)
		{	
			return null;
		}
	}
	
	
	
	public static boolean saveQuizResult(QuizResult quizResult)
	{
		if(quizResult == null)
			return false;
		
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {	
			em.getTransaction().begin();
			// HACK
			QuizResult newQuizResult = em.merge(quizResult);
			quizResult.setQuizResultID(newQuizResult.getQuizResultID());
			
			if(newQuizResult.getQuizResultID() == null)
				System.out.println("newQuizResult: null");
			else
				System.out.println("newQuizResult:" + Long.toString(newQuizResult.getQuizResultID()));
			//em.persist(quizResult);
			//em.merge(quizResult.getQuiz());
			em.getTransaction().commit();
			if(quizResult.getQuizResultID() == null)
				System.out.println("saveQuizResult(): quizResult is null");
			System.out.println("saveQuizResult(): successfully merge");
			return true;
		}
		catch(Exception e)
		{	
			System.out.println("saveQuizResult(): problem happened");
			return false;
		}
	}

}
