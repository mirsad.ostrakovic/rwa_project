package service.adminpanel;

import java.util.*;
import javax.persistence.*;

import model.adminpanel.Role;
import model.adminpanel.User;
import model.quiz.*;
import service.jpa.*;

public class AdminQuizService {
	
	private static Random randomGenerator = new Random();
	
	
	// NEED TO DO
	public static boolean checkUserPermission(Quiz quiz, User user) {
		return true;
	}
	
	// NEED TO IMPROVE
	public static boolean checkIsUserAdmin(User user) {
		Collection<Role> userRoles = user.getUserRolesCollection();
		
		for(Role role : userRoles)
			if(role.getRoleTitle().equals("admin"))
				return true;
		
		return false;
	}
	
	
	public static Quiz createNewQuiz()
	{
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		String newQuizShareTag = "";
		int generateQuizShareTagCounter = 16;
		
		try {
			
				while((--generateQuizShareTagCounter) != 0)
				{
					TypedQuery<Quiz> getQuizQuery = em.createNamedQuery("Quiz.getQuizByShareTag", Quiz.class);
			
					newQuizShareTag = "quiz" + Integer.toString(randomGenerator.nextInt()) + "00" + Integer.toString(randomGenerator.nextInt());
					newQuizShareTag = newQuizShareTag.replaceAll("-","");
					
					getQuizQuery.setParameter("quizShareTag", newQuizShareTag);
					List<Quiz> quizList = getQuizQuery.getResultList();
					
					if(quizList == null || quizList.size() == 0)
						break;
				}
				
				if(generateQuizShareTagCounter == 0)
					return null;
				
				Quiz newQuiz = new Quiz(newQuizShareTag, null);
				
				em.getTransaction().begin();
				em.persist(newQuiz);
				em.getTransaction().commit();
				
				return newQuiz;	
		}
		catch(Exception e)
		{	
			return null;
		}
	}
	
	
	
	public static Collection<Quiz> getQuizzes(User user, Integer from, Integer to)
	{
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		if(user == null || from == null || to == null || from < 0 || to < 0 || from > to)
			return null;
		
		try {	
			if(checkIsUserAdmin(user))
			{
				TypedQuery<Quiz> getQuizesQuery = em.createNamedQuery("Quiz.getQuizAll", Quiz.class);
				getQuizesQuery.setMaxResults(to - from + 1);
				getQuizesQuery.setFirstResult(from);
				List<Quiz> quizesList = getQuizesQuery.getResultList();
			
				return quizesList;
			}
			else
			{
				TypedQuery<Quiz> getQuizesQuery = em.createNamedQuery("Quiz.getQuizAllFromUser", Quiz.class);
				getQuizesQuery.setParameter("userDatabaseID", user.getDatabaseID());
				getQuizesQuery.setMaxResults(to - from + 1);
				getQuizesQuery.setFirstResult(from);
				List<Quiz> quizesList = getQuizesQuery.getResultList();
			
				return quizesList;
				
			}
		}
		catch(Exception e)
		{	
			return null;
		}
	}
	
	
	
	
	
	
	public static Quiz getQuizByID(String quizID)
	{
		if(quizID == null)
			return null;
		
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {	
			TypedQuery<Quiz> getQuizQuery = em.createNamedQuery("Quiz.getQuizByID", Quiz.class);
			
			getQuizQuery.setParameter("quizID", Long.parseLong(quizID));
			List<Quiz> quizList = getQuizQuery.getResultList();
			
			if(quizList == null || quizList.size() == 0)
				return null;
			else
				return quizList.get(0);	
		}
		catch(Exception e)
		{	
			return null;
		}
	}
	
	
	
	
	
	
	
	
	public static boolean editQuizTitle(String quizID, String quizTitle)
	{
		if(quizID == null || quizTitle == null)
			return false;
		
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
	
		try {
			TypedQuery<Quiz> getQuizQuery = em.createNamedQuery("Quiz.getQuizByID", Quiz.class);
			
			getQuizQuery.setParameter("quizID",  Long.parseLong(quizID));
			List<Quiz> quizList = getQuizQuery.getResultList();
			
			if(quizList == null || quizList.size() == 0)
				return false;
			
			quizList.get(0).setQuizTitle(quizTitle);
			
			em.getTransaction().begin();
			em.persist(quizList.get(0));
			em.getTransaction().commit();
			
			return true;
		}
		catch(Exception e)
		{	
			return false;
		}
	}

	
	
	
	
	
	public static boolean setQuizShareTag(String quizID, String newQuizShareTag)
	{
		if(quizID == null || newQuizShareTag == null)
			return false;
		
		newQuizShareTag = newQuizShareTag.replaceAll("\\s","");
		
		
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
	
		try {
			TypedQuery<Quiz> getQuizQuery = em.createNamedQuery("Quiz.getQuizByID", Quiz.class);
			
			em.getTransaction().begin();
			
			
			getQuizQuery.setParameter("quizID", Long.parseLong(quizID));
			List<Quiz> quizList = getQuizQuery.getResultList();
			
			if(quizList == null || quizList.size() == 0)
			{
				em.getTransaction().rollback();
				return false;
			}
			
			
			getQuizQuery = em.createNamedQuery("Quiz.getQuizByShareTag", Quiz.class);
			getQuizQuery.setParameter("quizShareTag", newQuizShareTag);
			
			List<Quiz> isAlreadyExistQuizList = getQuizQuery.getResultList();
			
			if(isAlreadyExistQuizList != null && isAlreadyExistQuizList.size() != 0)
			{
				em.getTransaction().rollback();
				return false;
			}
				
			
			quizList.get(0).setQuizShareTag(newQuizShareTag);
			
			em.persist(quizList.get(0));
			em.getTransaction().commit();
			
			return true;
		}
		catch(Exception e)
		{	
			em.getTransaction().rollback();
			return false;
		}
	}
	
	
	
	
	
	
	public static boolean setQuizActive(String quizID)
	{
		if(quizID == null)
			return false;
		
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
	
		try {
			TypedQuery<Quiz> getQuizQuery = em.createNamedQuery("Quiz.getQuizByID", Quiz.class);
			
			getQuizQuery.setParameter("quizID", Long.parseLong(quizID));
			List<Quiz> quizList = getQuizQuery.getResultList();
			
			if(quizList == null || quizList.size() == 0)
				return false;
			
			quizList.get(0).setQuizActiveState(true);
			
			em.getTransaction().begin();
			em.persist(quizList.get(0));
			em.getTransaction().commit();
			
			return true;
		}
		catch(Exception e)
		{	
			return false;
		}	
	}
	
	
	
	
	
	public static boolean setQuizInactive(String quizID)
	{
		if(quizID == null)
			return false;
		
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
	
		try {
			TypedQuery<Quiz> getQuizQuery = em.createNamedQuery("Quiz.getQuizByID", Quiz.class);
			
			getQuizQuery.setParameter("quizID", Long.parseLong(quizID));
			List<Quiz> quizList = getQuizQuery.getResultList();
			
			if(quizList == null || quizList.size() == 0)
				return false;
			
			quizList.get(0).setQuizActiveState(false);
			
			em.getTransaction().begin();
			em.persist(quizList.get(0));
			em.getTransaction().commit();
			
			return true;
		}
		catch(Exception e)
		{	
			return false;
		}	
	}
	
	
	
	
	public static boolean deleteQuiz(Long quizID)
	{
		if(quizID == null)
			return false;
		
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {
			TypedQuery<Quiz> getQuizQuery = em.createNamedQuery("Quiz.getQuizByID", Quiz.class);
			
			getQuizQuery.setParameter("quizID", quizID);
			List<Quiz> quizList = getQuizQuery.getResultList();
			
			if(quizList == null || quizList.size() == 0)
				return true;
			
			
			
			em.getTransaction().begin();
			em.remove(quizList.get(0));
			em.getTransaction().commit();
			
			return true;
			
		}
		catch(Exception e)
		{	
			return false;
		}
	}
	
	
}
