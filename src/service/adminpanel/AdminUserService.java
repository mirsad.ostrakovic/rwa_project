package service.adminpanel;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import model.adminpanel.*;
import service.*;
import service.jpa.*;

public class AdminUserService {

	
	
	public static User authenticateUser(String username, String password)
	{
		User user = null;
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		
		try {
			TypedQuery<User> getUserQuery = em.createNamedQuery("User.getUserByUsername", User.class);
			
			getUserQuery.setParameter("username", username);
			List<User> userList = getUserQuery.getResultList();
			
			if(userList.size() == 0)
				return null;
			
			user = userList.get(0);
					
			if(user.checkPassword(password))
				return user;
			else
				return null;	
		}
		catch(Exception e)
		{	
			return null;
		}
	}
	
	
	
	
	

	public static boolean changeUserPassword(String username, String password)
	{
		User user = null;
		Role role = null;
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {
			TypedQuery<User> getUserQuery = em.createNamedQuery("User.getUserByUsername", User.class);
			
			getUserQuery.setParameter("username", username);
			List<User> userList = getUserQuery.getResultList();
			
			if(userList.size() == 0)
				return false;
			
			user = userList.get(0);
			
			user.changePassword(password);
			
			em.getTransaction().begin();
			em.merge(user);
			em.getTransaction().commit();
			
			
		}
		catch(Exception e)
		{	
			return false;
		}
		
		return true;
	}
	
	
	
	public static boolean deleteUser(String username) 
	{
		User user = null;
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {
			TypedQuery<User> getUserQuery = em.createNamedQuery("User.getUserByUsername", User.class);
			
			getUserQuery.setParameter("username", username);
			List<User> userList = getUserQuery.getResultList();
			
			if(userList.size() == 0)
				return false;
			
			user = userList.get(0);
			
			System.out.println("AdminService.deleteUserRole(): user != null");
			
			
			em.getTransaction().begin();
			em.remove(user);
			em.getTransaction().commit();
		}
		catch(Exception e)
		{	
			return false;
		}
		
		return true;
		
	}
	
	
	
	
	public static boolean addUser(String username, String password) 
	{
		User user = null;
		Role role = null;
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {
			TypedQuery<User> getUserQuery = em.createNamedQuery("User.getUserByUsername", User.class);
			
			getUserQuery.setParameter("username", username);
			List<User> userList = getUserQuery.getResultList();
			
			if(userList.size() != 0)
				return false;
			
			user = new User(username, password);
			
			em.getTransaction().begin();		
			em.persist(user);
			em.getTransaction().commit();
			
			
		}
		catch(Exception e)
		{	
			return false;
		}
		
		return true;

	}
	
	
	
	
	public static boolean deleteUserRole(String username, String roleTitle)
	{
		User user = null;
		Role role = null;
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {
			TypedQuery<User> getUserQuery = em.createNamedQuery("User.getUserByUsername", User.class);
			
			getUserQuery.setParameter("username", username);
			List<User> userList = getUserQuery.getResultList();
			
			if(userList.size() == 0)
				return false;
			
			user = userList.get(0);
			
			System.out.println("AdminUserService.deleteUserRole(): user != null");
			
			TypedQuery<Role> getRoleQuery = em.createNamedQuery("Role.getRoleByRoleTitle", Role.class);
			
			getRoleQuery.setParameter("roleTitle", roleTitle);
			List<Role> roleList = getRoleQuery.getResultList();
			
			if(roleList.size() == 0)
				return false;
			
			role = roleList.get(0);
			
			System.out.println("AdminUserService.deleteUserRole(): role != null");
		
			
			if(!user.getUserRolesCollection().contains(role))
				return false;
			
			
			System.out.println("AdminUserService.deleteUserRole(): user play 'role'");
			
			
			em.getTransaction().begin();
			
			user.removeRole(role);
			
			em.merge(user);
			em.merge(role);
		
			em.getTransaction().commit();
			
			
		}
		catch(Exception e)
		{	
			return false;
		}
		
		return true;
	}
	
	
	
	
	

	
	public static boolean addUserRole(String username, String roleTitle)
	{
		
		User user = null;
		Role role = null;
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {
			TypedQuery<User> getUserQuery = em.createNamedQuery("User.getUserByUsername", User.class);
			
			getUserQuery.setParameter("username", username);
			List<User> userList = getUserQuery.getResultList();
			
			if(userList.size() == 0)
				return false;
			
			user = userList.get(0);
			
			
			
			TypedQuery<Role> getRoleQuery = em.createNamedQuery("Role.getRoleByRoleTitle", Role.class);
			
			getRoleQuery.setParameter("roleTitle", roleTitle);
			List<Role> roleList = getRoleQuery.getResultList();
			
			if(roleList.size() == 0)
				return false;
			
			role = roleList.get(0);
			
			
			
			
			if(user.getUserRolesCollection().contains(role))
				return false;
			
			
			
			em.getTransaction().begin();
			
			user.addRole(role);
			
			em.merge(user);
			em.merge(role);
		
			em.getTransaction().commit();
			
			
		}
		catch(Exception e)
		{	
			return false;
		}
		
		return true;

	}

	
	
	
	
	
	
	public static Long getUserCountDB()
	{
		try {
	   
			EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
			TypedQuery<Long> userCountQuery = em.createNamedQuery("User.getUserCount", Long.class);
			Long userCount = userCountQuery.getSingleResult();
			
			em.close();
			
			return userCount;
		
		} catch (Throwable t) {
		    System.out.println("AdminUserService.getUserCountDB()");
		    throw t;
		}
		
	}
	
	
	

	public static List<User> getUsers(Integer startResult, Integer maxCount)
	{
		try {
			   
			EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
			TypedQuery<User> getUserQuery = em.createNamedQuery("User.getAll", User.class);
			getUserQuery.setMaxResults(maxCount);
			getUserQuery.setFirstResult(startResult);
			List<User> users = getUserQuery.getResultList();
		
			em.close();
			
			return users;
		
		} catch (Throwable t) {
		    System.out.println("AdminUserService.getUsers()");
		    throw t;
		}
		
	}

	
}
