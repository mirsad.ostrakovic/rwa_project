package service.jpa;


import javax.persistence.*;

public class PersistenceContextHandler {
	
	private static final String PERSISTENCE_UNIT_NAME = "RWA_project";
	private static EntityManagerFactory emf = null;
	

	public static EntityManagerFactory getEMF()
	{
		if(emf == null)
			emf =  Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
	
		return emf;
	}

}
