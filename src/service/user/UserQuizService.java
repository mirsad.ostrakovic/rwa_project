package service.user;

import java.util.*;
import javax.persistence.*;
import model.quiz.*;
import service.*;
import service.jpa.*;
import view.*;

public class UserQuizService {
	
	private static Random randomGenerator = new Random();
	
	
	public static List<QuizView> getRandomQuizes(int getQuizCount)
	{
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		if(getQuizCount <= 0)
			return null;
		
		
		try {
			TypedQuery<Long> getQuizCountQuery = em.createNamedQuery("Quiz.getQuizCount", Long.class);
			
			Long quizCount = getQuizCountQuery.getSingleResult();
			
			if(quizCount == null || quizCount <= 0)
				return null;
			
			//LOG
			System.out.println("Quiz count: " + quizCount);
		
		
			int firstQuizHash = randomGenerator.nextInt(Math.toIntExact(quizCount));
			int secondQuizHash;
		
			while((secondQuizHash = randomGenerator.nextInt(Math.toIntExact(quizCount))) == firstQuizHash);
		
		
			//LOG
			System.out.println("First quiz hash: " + firstQuizHash);
			System.out.println("Second quiz hash: " + secondQuizHash);
		
		
			TypedQuery<Quiz> getRandomQuizQuery = em.createNamedQuery("Quiz.getQuizAll", Quiz.class);
			getRandomQuizQuery.setMaxResults(1);
		  
			getRandomQuizQuery.setFirstResult(firstQuizHash);
			Quiz firstQuiz = getRandomQuizQuery.getSingleResult();
		
		
		
			getRandomQuizQuery.setFirstResult(secondQuizHash);
			Quiz secondQuiz = getRandomQuizQuery.getSingleResult();
		
			List<QuizView> returnList = new ArrayList<QuizView>();
			
			returnList.add(new QuizView(firstQuiz));
			returnList.add(new QuizView(secondQuiz));
			
			return returnList;
		}
		catch(Exception e)
		{	
			return null;
		}
	}
	
	
	
	
	public static QuizView getQuiz(String quizID) 
	{
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		if(quizID == null || quizID.length() == 0)
			return null;
		

		try {	
			TypedQuery<Quiz> getQuizQuery = em.createNamedQuery("Quiz.getQuizByID", Quiz.class);
			
			getQuizQuery.setParameter("quizID", Long.parseLong(quizID));
			List<Quiz> quizList = getQuizQuery.getResultList();
			
			if(quizList == null || quizList.size() == 0)
				return null;
		
			QuizView returnValue = new QuizView(quizList.get(0));
			
			return returnValue;
		}
		catch(Exception e)
		{	
			return null;
		}
	}
	
	
	
	
	
	
	
	public static QuizView getRandomQuiz()
	{
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		
		try {
			TypedQuery<Long> getQuizCountQuery = em.createNamedQuery("Quiz.getQuizCount", Long.class);
			
			Long quizCount = getQuizCountQuery.getSingleResult();
			
			if(quizCount == null || quizCount <= 0)
				return null;
			
		
			int firstQuizHash = randomGenerator.nextInt(Math.toIntExact(quizCount));
	
		
			TypedQuery<Quiz> getRandomQuizQuery = em.createNamedQuery("Quiz.getQuizAll", Quiz.class);
			getRandomQuizQuery.setMaxResults(1);
		  
			getRandomQuizQuery.setFirstResult(firstQuizHash);
			Quiz firstQuiz = getRandomQuizQuery.getSingleResult();
		
			
			return new QuizView(firstQuiz);
		}
		catch(Exception e)
		{	
			return null;
		}
	}
	
	
	
	
	
	

}

