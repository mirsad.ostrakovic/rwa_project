package model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.*;

import com.google.gson.annotations.Expose;

@Entity(name = "TestClass")
@Table(name = "TEST_CLASS")
public class TestClass {
	@Id
	@GeneratedValue
	@Column(name = "TEST_CLASS_DATABASE_ID")
	private Long testClassDatabaseID;
	
	@Column(name = "TEST_TEXT")
	@Expose
	public String testText;
	
}
