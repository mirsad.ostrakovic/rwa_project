package model.adminpanel;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

import model.adminpanel.User;

@Entity(name = "Role")
@Table(name = "ROLE")
@NamedQueries({
	@NamedQuery(name = "Role.getRoleByRoleTitle",
			query = "SELECT r FROM Role r WHERE r.roleTitle = :roleTitle")
})
public class Role {
	
	@Id
	@GeneratedValue
	@Column(name = "ROLE_DATABASE_ID")
	private Long roleDatabaseID;
	
	/*
	@ManyToMany(mappedBy = "rolesCollection")
	private Collection<RolesRegistration> rolesRegistrationCollection = new ArrayList<>();
	*/
	@ManyToMany(mappedBy = "userRolesCollection")
	private Collection<User> rolesUserCollection = new ArrayList<>();
	
	
	@Column(name = "ROLE_TITLE", nullable = false)
	@Expose
	private String roleTitle;
	
	

	@Column(name = "GET_QUIZ_PERM", nullable = false)
	@Expose
	public boolean getQuizPerm;
	
	@Column(name = "ADD_QUIZ_PERM", nullable = false)
	@Expose
	public boolean addQuizPerm;
	
	@Column(name = "DELETE_QUIZ_PERM", nullable = false)
	@Expose
	public boolean deleteQuizPerm;
	
	@Column(name = "EDIT_QUIZ_PERM", nullable = false)
	@Expose
	public boolean editQuizPerm;
	
	
	

	@Column(name = "GET_USER_PERM", nullable = false)
	@Expose
	public boolean getUserPerm;
	
	@Column(name = "ADD_USER_PERM", nullable = false)
	@Expose
	public boolean addUserPerm;
	
	@Column(name = "DELETE_USER_PERM", nullable = false)
	@Expose
	public boolean deleteUserPerm;
	
	@Column(name = "EDIT_USER_PERM", nullable = false)
	@Expose
	public boolean editUserPerm;

	
	
	public Collection<User> getRolesUserCollection() {
		return rolesUserCollection;
	}
	
	public String getRoleTitle() { return roleTitle; }
	
	
	// CONSTRUCTORS

	public Role() {
	} // provided because of the need of JPA

	public Role(String roleTitle) {
		this.roleTitle = roleTitle;
		
		getQuizPerm = false;
		addQuizPerm = false;
		deleteQuizPerm = false;
		editQuizPerm = false;
		
		getUserPerm = false;
		addUserPerm = false;
		deleteUserPerm = false;
		editUserPerm = false;
	}
}

