package model.quiz;

import java.util.*;
import javax.persistence.*;
import com.google.gson.annotations.Expose;

@Entity(name = "Question")
@Table(name = "QUESTION")
@NamedQueries({
	@NamedQuery(name = "Question.getQuestionByID",
				query = "SELECT q FROM Question q WHERE q.questionID = :questionID")

})
public class Question {
	
	@Id
	@GeneratedValue
	@Column(name = "QUESTION_ID")
	@Expose
	private Long questionID;
	
	
	@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	@JoinColumn(name = "QUIZ_ID")
	private Quiz quiz;
	
	
	@Column(name = "QUESTION_TEXT", nullable = false)
	@Expose
	private String questionText;
	
	
	@Column(name = "QUESTION_PRIORITY", nullable = false)
	@Expose
	private int questionPriority;
	
	
	@Column(name = "QUESTION_TIME", nullable = false)
	@Expose
	private int questionTime;
	
	
	@Column(name = "QUESTION_SCORE", nullable = false)
	@Expose
	private int questionScore;
	
	
    @OneToMany(mappedBy = "question", cascade = CascadeType.ALL, orphanRemoval = true)
    @Expose
	private List<Answer> answerCollection = new ArrayList<Answer>();
	
    
    
    
    @Override
    public boolean equals(Object o) 
	{ 
        if (o == this)
            return true; 
  
        if (!(o instanceof Question)) 
            return false; 
     
        Question q = (Question) o; 
         
        return this.questionID.equals(q.questionID);
    } 
    
    
	
    public Long getQuestionID() { return this.questionID; }
	
    public Quiz getQuiz() { return this.quiz; }
    
    public String getQuestionText() { return this.questionText; }
    
    public int getQuestionPriority() { return this.questionPriority; }
    
    public int getQuestionTime() { return this.questionTime; }
    
    public int getQuestionScore() { return this.questionScore; }
	
    public List<Answer> getAnswerCollection() { return answerCollection; }
	
	
	
	public void setQuestionScore(int questionScore)
	{
		if(questionScore <= 0)
			questionScore = 1;
		
		this.questionScore = questionScore;
	}
	
	
	public void setQuestionTime(int questionTime)
	{
		if(questionTime <= 5)
			questionTime = 5;
		
		this.questionTime = questionTime;
	}
	
	
	public void setQuestionText(String questionText)
	{
		if(questionText == null || questionText.length() == 0)
			questionText = "Generic question text";
		
		this.questionText = questionText;
	}
	
	
	public void setQuestionPriority(int questionPriority)
	{
		if(questionPriority < 0)
			questionPriority = 0;
		
		this.questionPriority = questionPriority;
	}
	
	
	public void addAnswer(Answer answer) 
	{
		if(answer != null)
		{
			this.answerCollection.add(answer);
			answer.setQuestion(this);
		}
	}	
	
	
	public void removeAnswer(Answer answer) 
	{
		if(answer != null)
		{
			this.answerCollection.remove(answer);
			answer.removeQuestion();
		}
	}
	
	
	public void setQuiz(Quiz quiz)
	{
		if(quiz != null)
			this.quiz = quiz;
	}
	
	
	public void removeQuiz() { quiz = null; }
	
	
	
	
	
	
	public Question() 
	{
		// provided because of the need of 'JPA'
	}
	
	public Question(String questionText, int questionScore)
	{			
		setQuestionText(questionText);
		setQuestionScore(questionScore);
		setQuestionTime(30);
		setQuestionPriority(255);
	}

}
