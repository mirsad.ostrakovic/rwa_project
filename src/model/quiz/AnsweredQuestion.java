package model.quiz;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.*;
import com.google.gson.annotations.*;

import model.adminpanel.Role;

@Entity(name = "AnsweredQuestion")
@Table(name = "ANSWERED_QUESTION")
@NamedQueries({	
})
public class AnsweredQuestion {

	@Id
	@GeneratedValue
	@Column(name = "ANSWERED_QUESTION_ID")
	@Expose
	private Long answeredQuestionID;
	
	
	@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	@JoinColumn(name = "QUIZ_RESULT_ID")
	private QuizResult quizResult;
	
	
	@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	@JoinColumn(name = "QUESTION_ID", nullable = false)
	@Expose
	private Question question;
	
	
	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch=FetchType.EAGER)
	@JoinTable(name = "ANSWERED_QUESTION_ANSWERS",
			   joinColumns = @JoinColumn(name = "ANSWERED_QUESTION_ID", referencedColumnName = "ANSWERED_QUESTION_ID"),
			   inverseJoinColumns = @JoinColumn(name = "ANSWER_ID"))
	@Expose
	private List<Answer> answerCollection = new ArrayList<Answer>();
	
	
	
	
	public Question getQuestion() { return question; }
	
	public List<Answer> getSelectedAnswers() { return answerCollection; }
	
	
	
	public boolean addAnswer(Answer answer) 
	{
		if (answerCollection.contains(answer))
			return true;
		
		 answerCollection.add(answer);
		return true;
	}

	
	public void removeAnswer(Answer answer) 
	{
		answerCollection.remove(answer);
	}
	
	
	
	public void setQuizResult(QuizResult quizResult)
	{
		this.quizResult = quizResult;
	}
	
	
	public void removeQuizResult()
	{
		this.quizResult = null;
	}
	
	
	
	
	public AnsweredQuestion() 
	{
		// provided because of the need of 'JPA'
	}
	
	
	public AnsweredQuestion(Question question) throws Exception
	{
		if(question == null)
			throw new Exception("QuestionAnswerPair::QuestionAnswerPair(): invalid params");
		
		this.question = question;
	}
	

	
}
