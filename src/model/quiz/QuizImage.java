package model.quiz;

import javax.persistence.*;

import com.google.gson.annotations.Expose;

@Entity(name = "QuizImage")
@Table(name = "QUIZ_IMAGE")
@NamedQueries({
})
public class QuizImage {

	@Id
	@GeneratedValue
	@Column(name = "QUIZ_IMAGE_DATABASE_ID")
	private Long quizImageDatabaseID;
	
	
	
	@OneToOne(mappedBy = "quizImage")
	private Quiz quiz;
	
	
	
	@Lob
	@Column(name="IMAGE", nullable = false)
	@Expose
	private byte[] image;
	
	
	
	@Column(name = "MIME_TYPE", nullable = false)
	@Expose
	private String mimeType;
	
	
	
	public QuizImage() {
		// provided because of the need of 'JPA'
	}
	
	
	
	public QuizImage(Quiz quiz, String mimeType, byte[] image)
	{
		this.quiz = quiz;
		this.image = image;
		this.mimeType = mimeType;
	}
	
	
	
	public byte[] getImage() { return image; }
	public Quiz getImageQuiz() { return quiz; }
	public String getMimeType() { return mimeType; }
	
}
