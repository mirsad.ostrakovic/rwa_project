package model.quiz;

import java.util.List;
import java.util.*;

import javax.persistence.*;
import com.google.gson.annotations.*;

import model.adminpanel.User;



@Entity(name = "Quiz")
@Table(name = "QUIZ")
@NamedQueries({
	@NamedQuery(name = "Quiz.getQuizAll",
				query = "SELECT q FROM Quiz q"),
	
	@NamedQuery(name = "Quiz.getQuizAllFromUser",
	   			query = "SELECT q FROM Quiz q JOIN q.owner u WHERE u.userDatabaseID = :userDatabaseID"),
	
	@NamedQuery(name = "Quiz.getQuizByID",
				query = "SELECT q FROM Quiz q WHERE q.quizID = :quizID"),
	
	@NamedQuery(name = "Quiz.getQuizByShareTag",
				query = "SELECT q FROM Quiz q WHERE q.quizShareTag = :quizShareTag"),
	
	@NamedQuery(name = "Quiz.getQuizCount",
				query = "SELECT COUNT(q) FROM Quiz q")
	
})
public class Quiz {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "QUIZ_ID")
	@Expose
	private Long quizID;

	
	@Column(name = "QUIZ_SHARE_TAG", nullable = false, unique = true)
	@Expose
	private String quizShareTag;
	
	
	@Column(name = "QUIZ_TITLE", nullable = false)
	@Expose
	private String title;
	
	
	@Column(name = "QUIZ_DESCRIPTION")
	@Expose
	private String description;
	
		
	@Column(name = "IS_QUIZ_ACTIVE", nullable = false)
	@Expose
	private boolean isQuizActive;
	
	
	@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_DATABASE_ID")
	@Expose
	private User owner = null;
	
	
	@OneToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinColumn(name = "QUIZ_IMAGE_DATABASE_ID")
	private QuizImage quizImage;
	
	
	
	@Column(name = "HAS_QUIZ_IMAGE", nullable = false)
	@Expose
	private boolean hasQuizImage;
	
	
	
	@OneToMany(mappedBy = "quiz", cascade = CascadeType.ALL, orphanRemoval = true)
	@Expose
	private List<Question> questionCollection = new ArrayList<Question>();
	
	
	
	@OneToMany(mappedBy="quiz", cascade=CascadeType.PERSIST)
    private List<QuizResult> quizResultList = new ArrayList<QuizResult>();
	
	

	
	@Override
	public boolean equals(Object o) 
	{ 
		if (o == this)
			return true; 
	  
	    if (!(o instanceof Question)) 
	    	return false; 
	     
	    Quiz q = (Quiz) o; 
	         
	    return this.quizID.equals(q.quizID);
	} 
	    
	
	
	
	
	public Long getQuizID() { return quizID; }
	public User getQuizOwner() { return owner; }
	public String getQuizTitle() { return title; }
	public String getQuizDescription() { return description; }
	public boolean isQuizActive() { return isQuizActive; }
	public boolean hasQuizImage() { return hasQuizImage; }
	public List<QuizResult> getQuizResultList() { return quizResultList; }
	
	
	public void setQuizOwner(User user) {
		this.owner = user;
	}
	
	public void addQuestion(Question question) 
	{
		if(question != null)
		{
			questionCollection.add(question);
			question.setQuiz(this);
		}
	}
	
	public void removeQuestion(Question question) 
	{
		if(question != null)
		{
			questionCollection.remove(question);
			question.removeQuiz();
		}
	}
	
	
	public List<Question> getQuestionCollection() { return questionCollection; }
	
	
	public void setQuizImage(QuizImage quizImage) 
	{
		if(quizImage != null)
			hasQuizImage = true;
		else
			hasQuizImage = false;
		
		this.quizImage = quizImage;
	}
	
	
	public void setQuizTitle(String title)
	{
		if(title == null || title.length() == 0)
			title = "Generic quiz title";
		
		this.title = title;
	}
	
	public void setQuizDescription(String description)
	{
		if(description != null)
			this.description = description;
	}
	
	
	public void setQuizShareTag(String quizShareTag) throws Exception
	{	
		if(quizShareTag == null || quizShareTag.length() == 0)
			throw new Exception("Quiz::setQuizShareTag(): quizShareTag has invalid value");
			
		this.quizShareTag = quizShareTag;
	}
	
	
	public void setQuizActiveState(boolean isQuizActive)
	{
		this.isQuizActive = isQuizActive;
	}
	
	
	
	
	
	public Quiz() 
	{
		// provided because of the need of 'JPA'
	}
	
	public Quiz(String quizShareTag, String title) throws Exception
	{
		setQuizShareTag(quizShareTag);
		setQuizTitle(title);
		setQuizActiveState(false);
		this.quizImage = null;
		this.hasQuizImage = false;
		this.description = null;
	}
	
	
	
	
	
	
}
