package model.quiz;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.*;
import com.google.gson.annotations.*;

@Entity(name = "QuizResult")
@Table(name = "QUIZ_RESULT")
@NamedQueries({	
	@NamedQuery(name = "QuizResult.getQuizResultAll",
				query = "SELECT qr FROM QuizResult qr"),
	
	@NamedQuery(name = "QuizResult.getQuizResultByQuizID",
				query = "SELECT qr FROM QuizResult qr JOIN qr.quiz q WHERE q.quizID = :quizID"),
	
	@NamedQuery(name = "QuizResult.getQuizResultByID",
				query = "SELECT qr FROM QuizResult qr WHERE qr.quizResultID = :quizResultID")
})
public class QuizResult {
	
	@Id
	@GeneratedValue
	@Column(name = "QUIZ_RESULT_ID")
	@Expose
	private Long quizResultID;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "QUIZ_ID")
	private Quiz quiz;
	
	
	@Expose
	private String userName;
	
	
	@Expose
	private String userEmail;
	
	
	@OneToMany(mappedBy = "quizResult", cascade = CascadeType.ALL, orphanRemoval = true)
	@Expose
	private List<AnsweredQuestion> answeredQuestionCollection = new ArrayList<AnsweredQuestion>();
	
	
	public String getUserName() { return userName; }
	
	public String getUserEmail() { return userEmail; }
	
	public Long getQuizResultID() { return this.quizResultID; }
	
	public Quiz getQuiz() { return this.quiz; }
	
	public List<AnsweredQuestion> getAnsweredQuestionCollection() { return this.answeredQuestionCollection; }
	
	
	
	// HACK
	public void setQuizResultID(Long quizResultID) { this.quizResultID = quizResultID; }
	
	public void setUserName(String userName) { this.userName = userName; }
	
	public void setUserEmail(String userEmail) { this.userEmail = userEmail; }
	
	
	public void setQuiz(Quiz quiz) throws Exception
	{
		if(quiz == null)
			throw new Exception("QuizResult::setQuiz(): invalid param");
		
		this.quiz = quiz;
		quiz.getQuizResultList().add(this);
	}
	
	
	public void addQuestion(AnsweredQuestion answeredQuestion) 
	{
		if(answeredQuestion != null)
		{
			answeredQuestionCollection.add(answeredQuestion);
			answeredQuestion.setQuizResult(this);
		}
	}
	
	
	public void removeQuestion(AnsweredQuestion answeredQuestion) 
	{
		if(answeredQuestion != null)
		{
			answeredQuestionCollection.remove(answeredQuestion);
			answeredQuestion.removeQuizResult();
		}
	}
	
	
	
	
	
	
	
	public QuizResult() 
	{
		// provided because of the need of 'JPA'
	}
	
	public QuizResult(Quiz quiz) throws Exception
	{
		setQuiz(quiz);
		userName = "Unknown";
		userEmail = "Unknown";
	}
	
	
}
