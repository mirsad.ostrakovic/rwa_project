package model.quiz;

import javax.persistence.*;

import com.google.gson.annotations.Expose;

@Entity(name = "Answer")
@Table(name = "ANSWER")
@NamedQueries({
	@NamedQuery(name = "Answer.getAnswerByID",
			    query = "SELECT a FROM Answer a WHERE a.answerID = :answerID")
})
public class Answer {
	
	@Id
	@GeneratedValue
	@Column(name = "ANSWER_ID")
	@Expose
	private Long answerID;
	
	
	@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	@JoinColumn(name = "QUESTION_ID")
	private Question question;
	
	
	@Column(name = "ANSWER_TEXT")
	@Expose
	private String answerText;
	
	
	@Column(name = "IS_ANSWER_CORRECT")
	@Expose
	private boolean isAnswerCorrect;
	
	
	
	@Override
    public boolean equals(Object o) 
	{ 
        if (o == this)
            return true; 
  
        if (!(o instanceof Answer)) 
            return false; 
     
        Answer a = (Answer) o; 
        
        if(this.answerID == null || a.answerID == null)
        	return false;
        
        //System.out.println("this.answerID is " + (this.answerID == null ? "" : "not") + " null");
        //System.out.println("a.answerID is " + (a.answerID == null ? "" : "not") + " null");
        
        return this.answerID.equals(a.answerID);
    } 

	
	
	
	
	
	public Long getAnswerID() { return this.answerID; }
	public String getAnswerText() { return this.answerText; }
	public boolean isAnswerCorrect() { return this.isAnswerCorrect; } 
	public Question getQuestion() { return this.question; }
	
	
	
	public void setQuestion(Question question)
	{
		if(question != null)
			this.question = question;
	}
	
	
	public void removeQuestion() { question = null; }
	
	
	public void setAnswerText(String answerText)
	{
		if(answerText == null || answerText.length() == 0)
			answerText = "Generic answer text";
		
		this.answerText = answerText;
	}
	
	
	public void setAnswerCorrect() { isAnswerCorrect = true; }
	public void setAnswerIncorrect() { isAnswerCorrect = false; }
	
	
	
	
	
	
	public Answer() 
	{
		// provided because of the need of 'JPA'
	}
	
	public Answer(String answerText, boolean isAnswerCorrect)
	{			
		setAnswerText(answerText);
		this.isAnswerCorrect = isAnswerCorrect; 
	}
	
}
