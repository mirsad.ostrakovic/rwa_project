package web.resources;

import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.quiz.*;
import service.adminpanel.*;

@Path("/admin/question")
public class AdminQuestion {

	@POST
	@Path("{quizID}")
	public Response createNewQuestion(@PathParam("quizID") String quizID)
	{
		System.out.println("createNewQuestion(): quizID: " + quizID);
		
		Question question = AdminQuestionService.createNewQuestion(Long.parseLong(quizID));
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		
		 if(question != null)
			 return Response
			  .status(Response.Status.OK)
		      .entity(gson.toJson(question))
		      .type(MediaType.APPLICATION_JSON)
		      .build();
		 else
			 return Response
				.status(Response.Status.BAD_REQUEST)
				.build();
		
	}

	
	
	@GET
	@Path("{questionID}")
	public Response getQuestion(@PathParam("questionID") String questionID)
	{
		Question question = AdminQuestionService.getQuestion(Long.parseLong(questionID));
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		
		 if(question != null)
			 return Response
			  .status(Response.Status.OK)
		      .entity(gson.toJson(question))
		      .type(MediaType.APPLICATION_JSON)
		      .build();
		 else
			 return Response
				.status(Response.Status.BAD_REQUEST)
				.build();
		
	}
	
	
	@DELETE
	@Path("{questionID}")
	public Response deleteQuestion(@PathParam("questionID") String questionID)
	{
		if(AdminQuestionService.deleteQuestion(Long.parseLong(questionID)))
			return Response
			  .status(Response.Status.OK)
		      .type(MediaType.APPLICATION_JSON)
		      .build();
		 else
			return Response
				.status(Response.Status.BAD_REQUEST)
				.build();
		
	}
	
	class SetQuestionTextMessage {	
		public String questionText;
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{questionID}/text")
	public Response setQuestionText(@PathParam("questionID") String questionID, String json) 
	{
		SetQuestionTextMessage msg = new Gson().fromJson(json, SetQuestionTextMessage.class);		
		
		if(AdminQuestionService.editQuestionText(Long.parseLong(questionID), msg.questionText))
			return Response
			  .status(Response.Status.OK)
		      .build();
		 else
			return Response
				.status(Response.Status.BAD_REQUEST)
				.build();
	}
	
	
	@PUT
	@Path("{questionID}/priority/{questionPriority}")
	public Response setQuestionPriority(@PathParam("questionID") String questionID, @PathParam("questionPriority") String questionPriority) 
	{
		if(AdminQuestionService.setQuestionPriority(Long.parseLong(questionID), Integer.parseInt(questionPriority)))
			return Response
			  .status(Response.Status.OK)
		      .type(MediaType.APPLICATION_JSON)
		      .build();
		 else
			return Response
				.status(Response.Status.BAD_REQUEST)
				.build();
	}
	
	
	@PUT
	@Path("{questionID}/time/{questionTime}")
	public Response setQuestionTime(@PathParam("questionID") String questionID, @PathParam("questionTime") String questionTime) 
	{
		if(AdminQuestionService.setQuestionTime(Long.parseLong(questionID), Integer.parseInt(questionTime)))
			return Response
			  .status(Response.Status.OK)
		      .type(MediaType.APPLICATION_JSON)
		      .build();
		 else
			return Response
				.status(Response.Status.BAD_REQUEST)
				.build();
	}
	
	
	@PUT
	@Path("{questionID}/score/{questionScore}")
	public Response setQuestionScore(@PathParam("questionID") String questionID, @PathParam("questionScore") String questionScore) 
	{
		if(AdminQuestionService.setQuestionScore(Long.parseLong(questionID), Integer.parseInt(questionScore)))
			return Response
			  .status(Response.Status.OK)
		      .type(MediaType.APPLICATION_JSON)
		      .build();
		 else
			return Response
				.status(Response.Status.BAD_REQUEST)
				.build();
	}
	
	
	
}
