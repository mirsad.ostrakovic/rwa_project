package web.resources;


import java.io.InputStream;
import java.util.Collection;
import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import csv.GenerateCSV;
import model.quiz.Quiz;
import model.quiz.QuizResult;
import service.adminpanel.*;
import view.*;


@Path("/admin/quizresult")
public class AdminQuizResult {
	
	@GET
	@Path("{quizResultID}")
	public Response getQuizResult(@PathParam("quizResultID") String quizResultID) 
	{
		System.out.println("Get single quiz result: " + quizResultID);
		
		QuizResultView quizResultView = AdminQuizResultService.getQuizResultViewByID(quizResultID);
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		
		 if(quizResultView != null)
			 return Response
			  .status(Response.Status.OK)
		      .entity(gson.toJson(quizResultView))
		      .type(MediaType.APPLICATION_JSON)
		      .build();
		 else
			 return Response
				.status(Response.Status.BAD_REQUEST)
				.build();
	}
	
	
	@GET
	@Path("quiz/{quizID}")
	public Response getQuizResults(@PathParam("quizID") String quizID,
								   @DefaultValue("0")  @QueryParam("from") String from,
								   @DefaultValue("9")  @QueryParam("to")   String to   ) 
	{
		System.out.println("Request for quiz result from quiz: " + quizID);
		
		Collection<QuizResultView> quizResultCollection = AdminQuizResultService.getQuizResults(quizID, Integer.parseInt(from), Integer.parseInt(to));
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
	
		if(quizResultCollection != null)
			return Response
			   .status(Response.Status.OK)
			   .entity(gson.toJson(quizResultCollection))
			   .type(MediaType.APPLICATION_JSON)
			   .build();
		else
			return Response
			  .status(Response.Status.BAD_REQUEST)
			  .build();
	}
	
	
	@GET
	@Path("quiz/{quizID}/csv")
	@Produces("text/csv")
	public Response getQuizResults(@PathParam("quizID") String quizID)
	{
		System.out.println("Export inbox for quiz: " + quizID);
		
		List<QuizResultView> quizResultCollection = AdminQuizResultService.getQuizResults(quizID, 0, 10000000);
	
	
		if(quizResultCollection != null)
			return Response
			   .status(Response.Status.OK)
			   .entity(GenerateCSV.generateCSVFromQuizResultViews(quizResultCollection))
			   .build();
		else
			return Response
			  .status(Response.Status.BAD_REQUEST)
			  .build();
	}

}
