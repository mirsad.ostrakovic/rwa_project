package web.resources;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;

import javax.websocket.*;
import javax.websocket.server.*;

import view.*;

@ServerEndpoint(
value="/user/stat", 
decoders = UserCountSessionMessageDecoder.class, 
encoders = UserCountSessionMessageEncoder.class)
public class UserCountSession {
  
	public static final int REGISTER_QUIZ_USER = 1;
	public static final int QUIZ_STAT_MSG = 2;
	
    private Session session;
    private static Set<UserCountSession> userCountSessions = new CopyOnWriteArraySet<>();
    private static List<String> registerUsers = new ArrayList<String>();
 
    @OnOpen
    public void onOpen(Session session) throws IOException, EncodeException {
  
        this.session = session;
        userCountSessions.add(this);
        
        UserCountSessionMessage message = new UserCountSessionMessage(QUIZ_STAT_MSG, registerUsers.size());
        session.getBasicRemote().sendObject(message);
    }
 
    @OnMessage
    public void onMessage(Session session, UserCountSessionMessage message) throws IOException, EncodeException {
  
    	switch(message.messageType)
    	{
    		case REGISTER_QUIZ_USER:
    		{
    			 if(!registerUsers.contains(session.getId()))
    			 {
    				 registerUsers.add(session.getId());
    				 message = new UserCountSessionMessage(QUIZ_STAT_MSG, registerUsers.size());
    				 broadcast(message);
    			 }
    		}
    		break;
    	}
       
    }
 
    @OnClose
    public void onClose(Session session) throws IOException, EncodeException {
  
    	userCountSessions.remove(this);
    	registerUsers.remove(session.getId());
    
    	 UserCountSessionMessage message = new UserCountSessionMessage(QUIZ_STAT_MSG, registerUsers.size());
         broadcast(message);
    }
 
    @OnError
    public void onError(Session session, Throwable throwable) {
        // Do error handling here
    }
 
    
    private static void broadcast(UserCountSessionMessage message) 
      throws IOException, EncodeException {
  
    	userCountSessions.forEach(endpoint -> {
            synchronized (endpoint) {
                try {
                    endpoint.session.getBasicRemote().
                      sendObject(message);
                } catch (IOException | EncodeException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
