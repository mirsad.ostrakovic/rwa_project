package web.resources;


import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.server.mvc.Viewable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.quiz.*;
import service.adminpanel.*;


@Path("/admin/panel")
public class AdminPanel {

	
	@GET
    @Path("quiz/{quizID}")
    @Produces(MediaType.TEXT_HTML)
    public Viewable getQuiz(@PathParam("quizID") String quizID) {
        Map<String, String> params = new HashMap<>();
        params.put("quizID", quizID);
        return new Viewable("/admin_quiz_panel.jsp", params);
    }
	
	@GET
    @Path("quiz/{quizID}/inbox")
    @Produces(MediaType.TEXT_HTML)
    public Viewable getQuizInbox(@PathParam("quizID") String quizID) {
        Map<String, String> params = new HashMap<>();
        params.put("quizID", quizID);
        return new Viewable("/quiz_inbox.jsp", params);
    }
	
	@GET
    @Path("quiz/result/{quizResultID}")
    @Produces(MediaType.TEXT_HTML)
    public Viewable getQuizResult(@PathParam("quizResultID") String quizResultID) {
        Map<String, String> params = new HashMap<>();
        params.put("quizResultID", quizResultID);
        return new Viewable("/quiz_result.jsp", params);
    }
   
	
	
	@GET
    @Produces(MediaType.TEXT_HTML)
    public Viewable getMainAdminPanelPage() {
        return new Viewable("/admin_main_panel.jsp", null);
    }
}
