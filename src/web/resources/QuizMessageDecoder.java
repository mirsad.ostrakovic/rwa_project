package web.resources;

import javax.websocket.*;
import com.google.gson.*;
import view.*;

public class QuizMessageDecoder implements Decoder.Text<ClientQuizMessage> {
 
    private static Gson gson = new Gson();
 
    @Override
    public ClientQuizMessage decode(String s) throws DecodeException 
    {
        return gson.fromJson(s, ClientQuizMessage.class);
    }
 
    @Override
    public boolean willDecode(String s) 
    {
        return (s != null);
    }
 
    @Override
    public void init(EndpointConfig endpointConfig) 
    {
        // Custom initialization logic
    }
 
    @Override
    public void destroy() 
    {
        // Close resources
    }
} 