package web.resources;


import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.*;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

import org.glassfish.jersey.server.mvc.*;

import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.quiz.Quiz;
import service.adminpanel.*;
import service.user.*;
import view.*;


@Path("/quiz/")
public class UserQuiz {
	
	@GET
    @Path("/{thisQuizID}")
    @Produces(MediaType.TEXT_HTML)
    public Viewable getQuiz(@PathParam("thisQuizID") String thisQuizID) {
        Map<String, String> params = new HashMap<>();
        params.put("thisQuizID", thisQuizID);
        params.put("otherQuizID", "null");
        return new Viewable("/quiz_session.jsp", params);
    }
	
	
	@GET
    @Path("/{thisQuizID}/{otherQuizID}")
    @Produces(MediaType.TEXT_HTML)
    public Viewable getQuiz2(@PathParam("thisQuizID") String thisQuizID, @PathParam("otherQuizID") String otherQuizID) {
        Map<String, String> params = new HashMap<>();
        params.put("thisQuizID", thisQuizID);
        params.put("otherQuizID", otherQuizID);
        return new Viewable("/quiz_session.jsp", params);
    }
	
	
    @GET
    @Path("/result/{quizResultID}")
    @Produces(MediaType.TEXT_HTML)
    public Viewable getQuizResult(@PathParam("quizResultID") String quizResultID) {
        Map<String, String> params = new HashMap<>();
        params.put("quizResultID", quizResultID);
        return new Viewable("/quiz_result.jsp", params);
    }
    
}
