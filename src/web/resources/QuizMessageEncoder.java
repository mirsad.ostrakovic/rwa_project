package web.resources;

import javax.websocket.*;
import com.google.gson.*;
import view.*;

public class QuizMessageEncoder implements Encoder.Text<ServerQuizMessage> {
 
    private static Gson gson = new Gson();
 
    @Override
    public String encode(ServerQuizMessage message) throws EncodeException 
    {
        return gson.toJson(message);
    }
 
    @Override
    public void init(EndpointConfig endpointConfig) 
    {
        // Custom initialization logic
    }
 
    @Override
    public void destroy() 
    {
        // Close resources
    }
}
