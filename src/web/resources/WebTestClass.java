package web.resources;


import javax.ws.rs.*;

@Path("/hello")
public class WebTestClass {
	
	@GET
	@Produces("text/plain")
	public String getPlainText() {
		return "PLAIN TEXT TEST";
	}
	

}
