package web.resources;

import java.io.*;
import javax.websocket.*;
import javax.websocket.server.*;

@ServerEndpoint(value = "/websocket")
public class WebSocketTest {
	 
    @OnOpen
    public void onOpen(Session session) throws IOException 
    {
    	System.out.println("Client is now connected...");
    }
 
    @OnMessage
    public String onMessage(Session session, String message) throws IOException 
    {
        System.out.println("Receive from client: " + message);
        String returnMessage = "Echo: " + message;
        return returnMessage;
    }
 
    @OnClose
    public void onClose(Session session) throws IOException 
    {
    	System.out.println("Client is now disconnected...");
    }
 
    @OnError
    public void onError(Session session, Throwable throwable) 
    {
        throwable.printStackTrace();
    }
}
