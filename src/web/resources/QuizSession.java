package web.resources;

import java.io.*;
import java.time.Duration;
import java.time.Instant;
import java.util.*;

import javax.websocket.*;
import javax.websocket.server.*;

import model.quiz.*;
import service.adminpanel.*;
import view.*;


class UnansweredQuestion {
	
	public Long remainTime;
	public Long timeStamp;
	public Question question;
	public Integer questionOrdinalNumber;
	
	public UnansweredQuestion(Question question)
	{
		remainTime = (long)(question.getQuestionTime() * 1000);
		this.question = question;
	}
}



@ServerEndpoint(value = "/quiz/{quizID}", 
				decoders = QuizMessageDecoder.class, 
				encoders = QuizMessageEncoder.class)
public class QuizSession {
	
	
	 public static final int START_QUIZ_MSG = 1;
	 public static final int CURRENT_QUESTION_MSG = 2;
	 public static final int INVALID_REQUEST_MSG = 3;
	 public static final int SEND_ASNWER_MSG = 4;
	 public static final int SKIP_ASNWER_MSG = 5;
	 public static final int WAIT_TO_USER_DATA_MSG = 6;
	 public static final int QUESTION_TIMEOUT_MSG = 7;
	 public static final int SUCCESSFULL_END_OF_QUIZ_MSG = 8;
	 public static final int SEND_USER_DATA_MSG = 9;
	 public static final int SERVER_ERROR_MSG = 10;
	 public static final int INVALID_ANSWER_MSG = 11;
	 
	 public static final int WAIT_TO_START_STATE = 0;
	 public static final int WAIT_TO_ANSWER_STATE = 1;
	 public static final int WAIT_TO_USER_DATA = 2;
	
	
	 private Session session;
	 private QuizResult quizResult;
	 private ArrayList<UnansweredQuestion> unansweredQuestions = new ArrayList<>();
	 private Integer quizSessionState = 0; 
	 private Integer unansweredQuestionIdx = 0;
	 private Instant timestamp;
	 
	 
	
	 

	 @OnOpen
	 public void onOpen(Session session, 
			 			@PathParam("quizID") String quizID) throws IOException 
	 {
		 this.session = session;
		 
		 Quiz quiz = AdminQuizService.getQuizByID(quizID);
		 
		 if(quiz == null) // quiz with id 'quizID' is not found
		 {
			 System.out.println("QuizSession::onOpen(): quiz width 'quizID' not found");
			 session.close();
			 return;
		 }
			 
		 // Try to init QuizResult model
		 try 
		 {
			 quizResult = new QuizResult(quiz);
		 }
	     catch(Exception ex)
		 {
	    	 System.out.println("QuizSession::onOpen(): QuizResult(quiz) throwed exception");
	    	 session.close();
	    	 return;
		 }
		
		 int questionNumber = 1;
		 
		 for(Question question : quiz.getQuestionCollection())
		 {
			 UnansweredQuestion newQuestion = new UnansweredQuestion(question);
			 newQuestion.questionOrdinalNumber = questionNumber++;
			 unansweredQuestions.add(newQuestion);
		 }
	    
	 }
	 
	 void sendInvalidAnswerMessage()
	 {
		 ServerQuizMessage serverQuizMessage = new ServerQuizMessage(INVALID_ANSWER_MSG, null);
		 sendMessage(serverQuizMessage);
	 }
	 
	 void sendCurrentQuestionMessage()
	 {
		 ServerQuizMessage serverQuizMessage = new ServerQuizMessage(CURRENT_QUESTION_MSG, 
				 													 new QuestionView(unansweredQuestions.get(unansweredQuestionIdx).question, 
				 																	  unansweredQuestions.get(unansweredQuestionIdx).remainTime,
				 																	  unansweredQuestions.get(unansweredQuestionIdx).questionOrdinalNumber));
		 sendMessage(serverQuizMessage);
	 }
	 
	 void sendQuestionTimeoutMessage()
	 {
		 ServerQuizMessage serverQuizMessage = new ServerQuizMessage(QUESTION_TIMEOUT_MSG, 
				 													 new QuestionView(unansweredQuestions.get(unansweredQuestionIdx).question, 
				 																	  unansweredQuestions.get(unansweredQuestionIdx).remainTime,
				 																	  unansweredQuestions.get(unansweredQuestionIdx).questionOrdinalNumber));
		 sendMessage(serverQuizMessage);
	 }
	 
	 void sendInvalidRequestMessage()
	 {
		 ServerQuizMessage serverQuizMessage = new ServerQuizMessage(INVALID_REQUEST_MSG, null);
		 sendMessage(serverQuizMessage);
	 }
	 
	 void sendServerErrorMessage()
	 {
		 ServerQuizMessage serverQuizMessage = new ServerQuizMessage(SERVER_ERROR_MSG, null);
		 sendMessage(serverQuizMessage);
	 }
	 
	 void sendSuccessfullEndOfQuizMessage(Long quizResultID)
	 {
		 ServerQuizMessage serverQuizMessage = new ServerQuizMessage(SUCCESSFULL_END_OF_QUIZ_MSG, null);
		 serverQuizMessage.quizResultID = quizResultID;
		 sendMessage(serverQuizMessage);
	 }
	 
	 void sendWaitToUserDataMessage()
	 {
		 ServerQuizMessage serverQuizMessage = new ServerQuizMessage(WAIT_TO_USER_DATA_MSG, null);
		 sendMessage(serverQuizMessage);
	 }
	 
	 
	 
	 
	 
	 int checkQuestionAnswers(ClientQuizMessage message)
	 {
		 if(message.answerIDs == null)
			 return -1;
		 
		 for(Long answerID : message.answerIDs)
			 if(!isAnswerIDCorrect(unansweredQuestions.get(unansweredQuestionIdx), answerID))
				 return -2;
		
		 return 0;
	 }
	 
	 
	 boolean addAnswersToQuizResult(ClientQuizMessage message)
	 {
		 try
		 {
			 AnsweredQuestion answeredQuestion = new AnsweredQuestion(unansweredQuestions.get(unansweredQuestionIdx).question);
			 for(Long answerID : message.answerIDs)
			 {
				 // System.out.println("Log: answerIDs: " + Long.toString(answerID));
				 answeredQuestion.addAnswer(getAnswerByID(unansweredQuestions.get(unansweredQuestionIdx), answerID));
			 }
			 quizResult.addQuestion(answeredQuestion);
			 return true;
		 }
		 catch(Exception ex)
		 {
			 ex.printStackTrace();
			 return false;
		 } 
	 }
	 
	 boolean checkForQuestionTimeout()
	 {
		 long timeElapsed = Duration.between(timestamp, Instant.now()).toMillis();
		 System.out.println("Elapsed time: " + timeElapsed);
		 long remainTime = unansweredQuestions.get(unansweredQuestionIdx).remainTime;
		 
		 return timeElapsed >= remainTime;
	 }
	 
	 void updateCurrentQuestionRemainTime()
	 {
		 long timeElapsed = Duration.between(timestamp, Instant.now()).toMillis();
		 unansweredQuestions.get(unansweredQuestionIdx).remainTime -= timeElapsed;
		 System.out.println("Remain time: " + unansweredQuestions.get(unansweredQuestionIdx).remainTime);
	 }
	 
	
	 
	 // NEED TO DO
	 void moveToNextQuestion(boolean removeCurrentQuestion)
	 {
		 if(removeCurrentQuestion)
			 unansweredQuestions.remove((int)unansweredQuestionIdx);
		 else
			 ++unansweredQuestionIdx; 
		 
		 if(unansweredQuestionIdx >= unansweredQuestions.size())
			 unansweredQuestionIdx = 0;
	 }

	 
	 
	 
	 @OnMessage
	 public void onMessage(Session session, 
	    				   ClientQuizMessage message) throws IOException 
	 {
		 System.out.println("QuizSession::Message(): received message of type " + Long.toString(message.messageType) + " from user ");
		 
		 switch(quizSessionState)
		 {
		 	case WAIT_TO_START_STATE:
		 	{
		 		if(message.messageType == START_QUIZ_MSG)
		 		{
		 			quizSessionState = WAIT_TO_ANSWER_STATE;
		 			sendCurrentQuestionMessage();
		 			timestamp = Instant.now();
		 		}
		 		else
		 			sendInvalidRequestMessage();
		 	}
		 	return;
		 
		 
		 	
		 	
		 	
		 	
		 	
		 	
		 	case WAIT_TO_ANSWER_STATE:
		 	{
		 		if(message.messageType == SEND_ASNWER_MSG)
		 		{
		 			
		 			if(checkForQuestionTimeout())
		 				message.answerIDs = new ArrayList<Long>();
		 			
		 			
		 			if(checkQuestionAnswers(message) != 0)
		 			{
		 				sendInvalidAnswerMessage();
		 				return;
		 			}
		 			
		 			
		 			if(!addAnswersToQuizResult(message))
		 			{
		 				sendServerErrorMessage();
		 				session.close();
		 				return;
		 			}
		 			
		 			moveToNextQuestion(true);
		 			
		 			if(unansweredQuestions.isEmpty())
		 			{
		 				quizSessionState = WAIT_TO_USER_DATA;
		 				sendWaitToUserDataMessage();
		 				return;
		 			}
		 			
		 			if(checkForQuestionTimeout())
		 				sendQuestionTimeoutMessage();
		 			else
		 				sendCurrentQuestionMessage();
		 			
		 			timestamp = Instant.now();
		 		}
		 		else if(message.messageType == SKIP_ASNWER_MSG)
		 		{
		 			if(!checkForQuestionTimeout())
		 			{
		 				updateCurrentQuestionRemainTime();
		 				moveToNextQuestion(false);
		 				sendCurrentQuestionMessage();
		 			}
		 			else
		 			{
		 				message.answerIDs = new ArrayList<Long>();		 			
			 			
			 			if(!addAnswersToQuizResult(message))
			 			{
			 				sendServerErrorMessage();
			 				session.close();
			 				return;
			 			}
			 			
			 			moveToNextQuestion(true);
			 			
			 			if(unansweredQuestions.isEmpty())
			 			{
			 				quizSessionState = WAIT_TO_USER_DATA;
			 				sendWaitToUserDataMessage();
			 				return;
			 			}
		 				
		 				sendQuestionTimeoutMessage();
		 			}
		 			timestamp = Instant.now();
		 		}
		 		else
		 			sendInvalidRequestMessage();
		 	}
		 	return;		 
		 
		
		 	
		 	
		 	
		 	
		 	
		 	
		 	
		 	
		 	
		 	case WAIT_TO_USER_DATA:
		 	{
		 		if(message.name != null && message.name.length() != 0)
		 			quizResult.setUserName(message.name);
		 		
		 		if(message.email != null && message.email.length() != 0)
		 			quizResult.setUserEmail(message.email);
		 		
 				if(AdminQuizResultService.saveQuizResult(quizResult))
 				{
 					System.out.println("Succesfull end of quiz");
 					if(quizResult.getQuizResultID() == null)
 						System.out.println("quizResultID: null");
 					else
 						System.out.println("quizResultID: " + Long.toString(quizResult.getQuizResultID()));
 					sendSuccessfullEndOfQuizMessage(quizResult.getQuizResultID());
 				}
 				else
 				{
 					System.out.println("AdminQuizResultService.saveQuizResult(quizResult) error");
 					sendServerErrorMessage();
 				}
 				System.out.println("Before session closed 1");
 				session.close();
		 	}
		 	return;
		 
		 
		 
		 }

	 }
	 
	 
	 
	 @OnClose
	 public void onClose(Session session) throws IOException 
	 {
		 System.out.println("Session closed");
		 /*
		 chatEndpoints.remove(this);
		 Message message = new Message();
	     message.setFrom(users.get(session.getId()));
	     message.setContent("Disconnected!");
	     broadcast(message);
	     */
	 }
	 
	 
	 
	 @OnError
	 public void onError(Session session, 
			 			 Throwable throwable)
	 {
	        // Do error handling here
	 }
	 
	 
	 
	 
	 boolean isAnswerIDCorrect(UnansweredQuestion unansweredQuestion, Long answerID)
	 {
		 boolean isAnswerIDCorrect = false;
		 
		 for(Answer answer : unansweredQuestion.question.getAnswerCollection())
		 {
			 if(answer.getAnswerID().equals(answerID))
			 {
				 isAnswerIDCorrect = true;
				 break;
			 }
		 }
		 
		 return isAnswerIDCorrect;
	 }
	 
	 
	 
	 Answer getAnswerByID(UnansweredQuestion unansweredQuestion, Long answerID)
	 {
		 Answer returnAnswer = null;
		 
		 for(Answer answer : unansweredQuestion.question.getAnswerCollection())
		 {
			 if(answer.getAnswerID().equals(answerID))
			 {
				 returnAnswer = answer;
				 break;
			 }
		 }
		 
		 return returnAnswer;
	 }
	 
	 
	 
	 private void sendMessage(ServerQuizMessage message)  
	 {
		try 
		{
		 session.getBasicRemote().sendObject(message);                
	 	} 
		catch (IOException | EncodeException e) 
		{
		  e.printStackTrace();
		}
	 }
		        
}
