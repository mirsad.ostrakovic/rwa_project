package web.resources;

import java.io.InputStream;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import filter.*;
import model.adminpanel.User;
import model.quiz.Quiz;
import service.adminpanel.*;
import web.resources.AdminQuestion.SetQuestionTextMessage;


@Path("/admin/quiz")
public class AdminQuiz {
	
	
	@Context
	HttpServletRequest webRequest;
	
	@GET
	@Path("")
	public Response getQuiz(@DefaultValue("0")  @QueryParam("from") String from,
							@DefaultValue("9")  @QueryParam("to")   String to   ) 
	{	
		User user = (User)webRequest.getSession().getAttribute("user");
		//System.out.println("User: " + user.getUsername());
		
		//setAttribute("user", user);
		
		Collection<Quiz> quizCollection = AdminQuizService.getQuizzes(user, Integer.parseInt(from), Integer.parseInt(to));
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
	
		if(quizCollection != null)
			return Response
			   .status(Response.Status.OK)
			   .entity(gson.toJson(quizCollection))
			   .type(MediaType.APPLICATION_JSON)
			   .build();
		else
			return Response
			  .status(Response.Status.BAD_REQUEST)
			  .build();
	}



	
	
	@POST
	@Path("")
	public Response createNewQuiz() 
	{
		Quiz quiz = AdminQuizService.createNewQuiz();
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		
		 if(quiz != null)
			 return Response
			  .status(Response.Status.OK)
		      .entity(gson.toJson(quiz))
		      .type(MediaType.APPLICATION_JSON)
		      .build();
		 else
			 return Response
				.status(Response.Status.BAD_REQUEST)
				.build();
	}
	
	
	
	@GET
	@Path("{quizID}")
	public Response getQuiz(@PathParam("quizID") String quizID) 
	{
		Quiz quiz = AdminQuizService.getQuizByID(quizID);
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		
		 if(quiz != null)
			 return Response
			  .status(Response.Status.OK)
		      .entity(gson.toJson(quiz))
		      .type(MediaType.APPLICATION_JSON)
		      .build();
		 else
			 return Response
				.status(Response.Status.BAD_REQUEST)
				.build();
	}
	
	
	@DELETE
	@Path("{quizID}")
	public Response deleteQuiz(@PathParam("quizID") String quizID)
	{
		if(AdminQuizService.deleteQuiz(Long.parseLong(quizID)))
			return Response
			  .status(Response.Status.OK)
		      .type(MediaType.APPLICATION_JSON)
		      .build();
		 else
			return Response
				.status(Response.Status.BAD_REQUEST)
				.build();
		
	}

	class SetQuizTitleMessage {
		public String quizTitle;
	}
	
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{quizID}/title")
	public Response editQuizTitle(@PathParam("quizID") String quizID, String json) 
	{
		SetQuizTitleMessage msg = new Gson().fromJson(json, SetQuizTitleMessage.class);	
		
		if(AdminQuizService.editQuizTitle(quizID, msg.quizTitle))
			return Response
			  .status(Response.Status.OK)
		      .type(MediaType.APPLICATION_JSON)
		      .build();
		 else
			return Response
				.status(Response.Status.BAD_REQUEST)
				.build();
	}
	
	
	

	@PUT
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("{quizID}/sharetag")
	public Response setQuizShareTag(@PathParam("quizID") String quizID, @FormParam("quizShareTag") String newQuizShareTag) 
	{
		if(AdminQuizService.setQuizShareTag(quizID, newQuizShareTag))
			return Response
			  .status(Response.Status.OK)
		      .build();
		 else
			return Response
				.status(Response.Status.BAD_REQUEST)
				.build();
	}
	
	
	@PUT
	@Path("{quizID}/active")
	public Response setQuizActive(@PathParam("quizID") String quizID) 
	{
		if(AdminQuizService.setQuizActive(quizID))
			return Response
			  .status(Response.Status.OK)
		      .build();
		 else
			return Response
				.status(Response.Status.BAD_REQUEST)
				.build();
	}
	
	
	@PUT
	@Path("{quizID}/inactive")
	public Response setQuizInactive(@PathParam("quizID") String quizID) 
	{
		if(AdminQuizService.setQuizInactive(quizID))
			return Response
			  .status(Response.Status.OK)
		      .build();
		 else
			return Response
				.status(Response.Status.BAD_REQUEST)
				.build();
	}
	
	
	
	// NEED TO DO
	@POST
	@Path("{quizID}/image")  
	@Consumes("*/*") 
	public String MyMethod(InputStream stream) 
	{
		try {
			System.out.println("Succesfull upload");
			System.out.println(IOUtils.toString(stream));
			byte[] image = IOUtils.toByteArray(stream);
			return "done";
		}
		catch(Exception ex)
		{
			return "Error happened";
		}
	}
	
	
	/*
	@POST
	@Path("{quizID}/image")  
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response uploadFile(@PathParam("quizID") String quizID,
							   @FormParam("file") InputStream uploadedInputStream,
							   @FormParam("file") FormDataContentDisposition fileInfo) 
	{
	   
		System.out.println("File type: " + fileInfo.getType());
	   
		return Response
			.status(Response.Status.OK)
			.build();
	}
	*/
	
	
	
	

}
