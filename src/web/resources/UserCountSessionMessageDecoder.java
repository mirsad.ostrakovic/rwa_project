package web.resources;

import javax.websocket.*;

import com.google.gson.Gson;

import view.*;

public class UserCountSessionMessageDecoder implements Decoder.Text<UserCountSessionMessage>{
    
	private static Gson gson = new Gson();
    
    @Override
    public UserCountSessionMessage decode(String s) throws DecodeException 
    {
        return gson.fromJson(s, UserCountSessionMessage.class);
    }
 
    @Override
    public boolean willDecode(String s) 
    {
        return (s != null);
    }
 
    @Override
    public void init(EndpointConfig endpointConfig) 
    {
        // Custom initialization logic
    }
 
    @Override
    public void destroy() 
    {
        // Close resources
    }
}
