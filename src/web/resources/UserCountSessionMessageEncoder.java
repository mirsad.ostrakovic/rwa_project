package web.resources;

import javax.websocket.*;

import com.google.gson.Gson;

import view.*;


public class UserCountSessionMessageEncoder  implements Encoder.Text<UserCountSessionMessage>{

    private static Gson gson = new Gson();
    
    @Override
    public String encode(UserCountSessionMessage message) throws EncodeException 
    {
        return gson.toJson(message);
    }
 
    @Override
    public void init(EndpointConfig endpointConfig) 
    {
        // Custom initialization logic
    }
 
    @Override
    public void destroy() 
    {
        // Close resources
    }
}
