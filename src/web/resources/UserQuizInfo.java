package web.resources;

import java.io.InputStream;
import java.util.Collection;
import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.quiz.Quiz;
import service.adminpanel.*;
import service.user.*;
import view.*;


@Path("/quiz/info/")
public class UserQuizInfo {
	
	
	@GET
	@Path("/random/1")
	public Response getRandomQuiz() 
	{	
		System.out.println("getRandomQuiz()");
		
		QuizView quiz = UserQuizService.getRandomQuiz();
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
	
		if(quiz != null)
			return Response
			   .status(Response.Status.OK)
			   .entity(gson.toJson(quiz))
			   .type(MediaType.APPLICATION_JSON)
			   .build();
		else
			return Response
			  .status(Response.Status.BAD_REQUEST)
			  .build();
	}
	
	@GET
	@Path("/random")
	public Response getRandomQuizzes() 
	{	
		List<QuizView> quizCollection = UserQuizService.getRandomQuizes(2);
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
	
		if(quizCollection != null)
			return Response
			   .status(Response.Status.OK)
			   .entity(gson.toJson(quizCollection))
			   .type(MediaType.APPLICATION_JSON)
			   .build();
		else
			return Response
			  .status(Response.Status.BAD_REQUEST)
			  .build();
	}
	
	
	@GET
	@Path("/{quizID}")
	public Response getQuiz(@PathParam("quizID") String quizID) 
	{	
		QuizView quiz = UserQuizService.getQuiz(quizID);
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
	
		if(quiz != null)
			return Response
			   .status(Response.Status.OK)
			   .entity(gson.toJson(quiz))
			   .type(MediaType.APPLICATION_JSON)
			   .build();
		else
			return Response
			  .status(Response.Status.BAD_REQUEST)
			  .build();
	}
	
	

}

