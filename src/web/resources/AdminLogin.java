package web.resources;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.mvc.Viewable;

import com.google.gson.Gson;

import model.adminpanel.*;
import service.adminpanel.*;
import web.resources.*;


@Path("/login")
public class AdminLogin {
	
	@GET
    @Produces(MediaType.TEXT_HTML)
    public Viewable getLoginPage() {
        return new Viewable("/login_page.jsp", null);
    }
	
	
	class LoginMessage {	
		public String username;
		public String password;
	}
	
	
	@Context
	HttpServletRequest webRequest;
	
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response loginRequest(String json) 
	{
		LoginMessage loginMsg = new Gson().fromJson(json, LoginMessage.class);	
	
		
		if(loginMsg.username == null || loginMsg.username.length() == 0 || loginMsg.password == null || loginMsg.password.length() == 0)
			return Response.status(Response.Status.NOT_ACCEPTABLE).build();
			
		
	
		
		User user = AdminUserService.authenticateUser(loginMsg.username, loginMsg.password);
				
		if(user == null)
			return Response.status(Response.Status.NOT_ACCEPTABLE).build();
	
		webRequest.getSession(false);
		webRequest.getSession().setAttribute("user", user);
		System.out.println("LOGIN SUCCESSFULLY");
		System.out.println("getContextPath(): " + webRequest.getContextPath());
					
		return Response.status(Response.Status.OK).build();
	}
	
}
