package web.resources;

import java.io.InputStream;
import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.quiz.*;
import service.adminpanel.*;
import web.resources.AdminQuestion.SetQuestionTextMessage;

@Path("/admin/answer")
public class AdminAnswer {
	
	@POST
	@Path("{questionID}")
	public Response createNewAnswer(@PathParam("questionID") String questionID)
	{
		System.out.println("createNewAnswer(): questionID: " + questionID);
		
		Answer answer = AdminAnswerService.createNewAnswer(Long.parseLong(questionID));
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		
		 if(answer != null)
			 return Response
			  .status(Response.Status.OK)
		      .entity(gson.toJson(answer))
		      .type(MediaType.APPLICATION_JSON)
		      .build();
		 else
			 return Response
				.status(Response.Status.BAD_REQUEST)
				.build();
		
	}
	
	
	@DELETE
	@Path("{answerID}")
	public Response deleteAnswer(@PathParam("answerID") String answerID)
	{
		if(AdminAnswerService.deleteAnswer(Long.parseLong(answerID)))
			return Response
			  .status(Response.Status.OK)
		      .type(MediaType.APPLICATION_JSON)
		      .build();
		 else
			return Response
				.status(Response.Status.BAD_REQUEST)
				.build();
		
	}
	
	
	
	
	@GET
	@Path("{answerID}")
	public Response getQuestion(@PathParam("answerID") String answerID)
	{
		Answer answer = AdminAnswerService.getAnswer(Long.parseLong(answerID));
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		
		 if(answer != null)
			 return Response
			  .status(Response.Status.OK)
		      .entity(gson.toJson(answer))
		      .type(MediaType.APPLICATION_JSON)
		      .build();
		 else
			 return Response
				.status(Response.Status.BAD_REQUEST)
				.build();
		
	}
	
	class SetAnswerTextMessage {	
		public String answerText;
	}
	
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{answerID}/text")
	public Response setAnswerText(@PathParam("answerID") String answerID,  String json) 
	{
		SetAnswerTextMessage msg = new Gson().fromJson(json, SetAnswerTextMessage.class);		
		
		if(AdminAnswerService.editAnswerText(Long.parseLong(answerID), msg.answerText))
			return Response
			  .status(Response.Status.OK)
		      .type(MediaType.APPLICATION_JSON)
		      .build();
		 else
			return Response
				.status(Response.Status.BAD_REQUEST)
				.build();
	}
	
	
	
	@PUT
	@Path("{answerID}/correct")
	public Response setAnswerCorrect(@PathParam("answerID") String answerID) 
	{
		if(AdminAnswerService.setAnswerCorrect(Long.parseLong(answerID)))
			return Response
			  .status(Response.Status.OK)
		      .type(MediaType.APPLICATION_JSON)
		      .build();
		 else
			return Response
				.status(Response.Status.BAD_REQUEST)
				.build();
	}
	
	
	
	@PUT
	@Path("{answerID}/incorrect")
	public Response setAnswerIncorrect(@PathParam("answerID") String answerID) 
	{
		if(AdminAnswerService.setAnswerIncorrect(Long.parseLong(answerID)))
			return Response
			  .status(Response.Status.OK)
		      .type(MediaType.APPLICATION_JSON)
		      .build();
		 else
			return Response
				.status(Response.Status.BAD_REQUEST)
				.build();
	}

	


}
